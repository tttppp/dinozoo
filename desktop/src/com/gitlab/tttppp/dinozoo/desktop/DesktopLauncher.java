package com.gitlab.tttppp.dinozoo.desktop;

import com.badlogic.gdx.Files;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;
import com.gitlab.tttppp.dinozoo.DinoZooGame;

public class DesktopLauncher {
	public static void main (String[] arg) {
		Lwjgl3ApplicationConfiguration config = new Lwjgl3ApplicationConfiguration();
		config.setWindowedMode(800, 600);
		config.setWindowIcon("data/logo.png");
		new Lwjgl3Application(new DinoZooGame(), config);
	}
}
