major=`grep "version = '1." build.gradle | sed "s|\W*version = '\([0-9][0-9]*\).[0-9][0-9]*'\W*|\1|g"`
minor=`grep "version = '1." build.gradle | sed "s|\W*version = '[0-9][0-9]*.\([0-9][0-9]*\)'\W*|\1|g"`

gradle desktop:jar
cp desktop/build/libs/desktop-$major.$minor.jar desktop/build/libs/desktop-$major.$minor.arm.jar

for artifact in ./artifacts/lib*
do
    zip -uj desktop/build/libs/desktop-$major.$minor.arm.jar $artifact
done

git tag $major.$minor
git push origin $major.$minor

newMinor=$((minor + 1))
sed -i "s|version = '$major.$minor'|version = '$major.$newMinor'|g" build.gradle
sed -i "s|public static final int GAME_VERSION = $minor;|public static final int GAME_VERSION = $newMinor;|g" ./core/src/com/gitlab/tttppp/dinozoo/gameworld/GameWorld.java

git add build.gradle
git add ./core/src/com/gitlab/tttppp/dinozoo/gameworld/GameWorld.java
git commit -m "Update version to $major.$newMinor."
git push origin HEAD:master
