# Welcome to the mini2dx-rpi-libs wiki!

The contents of this wiki are from https://github.com/Torbuntu/mini2dx-rpi-libs/wiki

## Follow the steps in this order:

1. Install Dev Packages
2. Compile Lwjgl [Legacy 2.9.3]
3. Compile LibGDX


At each step, it is recommended to collect the files mentioned at the bottom of the wiki and copy them into a central location for your reference. This will make it easier to use them later on in your own projects.

