# Installing Lwjgl 2.9.3 from the legacy branch [here](https://github.com/LWJGL/lwjgl)

## Set up java and env variables
    lwjgl needs to use openjdk 8 to compile. My latest raspbian image came with 9 installed by default.
    1. Update `/home/pi/.bashrc` (I use the command `nano .bashrc`)
      1.b add the following to the end of the file:
         export JAVA_HOME="/usr/lib/jvm/java-8-openjdk-armhf"
         export PATH=$PATH:$JAVA_HOME/bin
    
## Git clone and set-up build files

On the pi, I start in the home directory for easy manoeuvring. 

    1. `git clone https://github.com/LWJGL/lwjgl.git`
      1.b `cd lwjgl`
    
    2. make the update to platform_build/linux_ant/build.xml
       change line 7 from:
           `<property name="libs32" value="-L/usr/X11R6/lib -L/usr/X11/lib -lm -lX11 -lXext -lXcursor -lXrandr -lXxf86vm -lpthread -L${java.home}/lib/i386 -ljawt" />`
       to -> 
           `<property name="libs32" value="-L/usr/X11R6/lib -L/usr/X11/lib -lm -lX11 -lXext -lXcursor -lXrandr -lXxf86vm -lpthread -L${java.home}/lib/arm -ljawt" />`    
        specifically `-L${java.home}/lib/i386` to `-L${java.home}/lib/arm`
    3. `env PATH=${JAVA_HOME}/bin:${PATH} ant`

Now there should be compiled files in `/home/pi/lwjgl/libs/linux/`
```
libjinput.so
lwjgl.so
```
