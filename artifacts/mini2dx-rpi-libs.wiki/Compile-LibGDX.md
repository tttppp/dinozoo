# Setting up LibGDX

## Git clone, modify files, compile

    1. `git clone https://github.com/libgdx/libgdx.git`
      1.b `cd libgdx/gdx/jni`

    2. edit build-linux32.xml (I use `nano build-linux32.xml`)
      2.b Remove any place with the following (lns 19, 30 and 42):
          `-m32` , `-msse` , `-mfpmath=sse`

    3. `ant -f build-linux32.xml`

This will produce a file in `/home/pi/libgdx/gdx/libs/linux32` called `libgdx.so`

Copy this file to our centralized folder with the other files form previous steps,
### important
once copied to the centralized folder, rename the file to `libgdxarm.so` this way the project
will be able to detect this file. This is crucial. 


## Other
If you need other native libs from libGDX then we can follow almost the same steps as above but in the extensions directory:

### Example, gdx-freetype

    1. `cd /home/pi/libgdx/extensions/gdx-freetype/jni`
    
    2. edit build-linux32.xml (I use `nano build-linux32.xml`)
      2.b Remove any place with the following (lns 19, 72 and 82):
          `-m32` , `-msse` , `-mfpmath=sse` 

    3. `ant -f build-linux32.xml`

The file we need from this is in `/home/pi/libgdx/extensions/gdx-freetype/libs/linux32/`
Make sure when you copy it to the centralized file to rename it to `libgdx-freetypearm.so`