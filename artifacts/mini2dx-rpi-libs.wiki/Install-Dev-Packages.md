# Initial package installation to get prepared 

## update and install libs
```
sudo apt-get update
sudo apt-get dist-upgrade
sudo apt-get install 
 raspi-gpio
 git 
 ant 
 libxcursor-dev
 libxxf86vm-dev
 libxrandr-dev 
 libxt-dev
 libopenal-dev
 xcompmgr 
 libgl1-mesa-dri
 mesa-utils
 openjdk-8-*
```

## Activation of Features and Settings
To check if OpenGL is running `glxgears` . this command will show the Gears demo. If it is showing flickery and not smooth colours, continue to the following:

    1. `sudo raspi-config` and go to:
      1.b `Advanced Options` 
      1.c `GL Driver` 

    2. chose `GL (Fake KMS) Desktop Driver` 
      2.b `OK`

    3.a `Advanced Options`
      3.b `Memory Split`
      3.c Change to 256
      3.d `OK`

finish and reboot!

run `glxgears` again and it should be pretty buttery smooth now! And at glorious 60 fps.

From this page we will want to collect the file `libopenal.so` from the directory `/user/lib/arm-linux-gnueabihf/`