package com.gitlab.tttppp.dinozoo.gameobject;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.gitlab.tttppp.dinozoo.gameobject.behaviour.GuestBehaviour;
import com.gitlab.tttppp.dinozoo.gameobject.behaviour.TickBehaviour;
import com.gitlab.tttppp.dinozoo.gamerenderer.GuestLoader;
import com.gitlab.tttppp.dinozoo.gameworld.DinosaurType;
import com.gitlab.tttppp.dinozoo.gameworld.Direction;
import com.gitlab.tttppp.dinozoo.gameworld.GameWorld;
import com.gitlab.tttppp.dinozoo.gameworld.IntegerIdType;
import com.gitlab.tttppp.dinozoo.gameworld.ObjectType;
import com.gitlab.tttppp.dinozoo.gameworld.Position;
import com.gitlab.tttppp.dinozoo.utils.Log;
import com.gitlab.tttppp.dinozoo.utils.RandomUtils;

import java.util.Map;
import java.util.Set;

import static com.gitlab.tttppp.dinozoo.gameworld.Direction.UP;
import static com.google.common.collect.Maps.newHashMap;
import static com.google.common.collect.Sets.newHashSet;

public class Guest implements GameObject {
    /** The unimpeded distance a guest can see. */
    private static final int VISION = 5;
    /** The initial amount of energy a guest has before they want to head home. */
    public static final int INITIAL_ENERGY = 100;
    private static final int TICKS_PER_STEP = 10;

    private IntegerIdType guestType;
    private Position position;
    private transient TextureRegion textureRegion;
    private transient TickBehaviour tickBehaviour = new GuestBehaviour(this);
    private long tickLastUpdated;
    private Direction lastDirection = UP;
    /** Note that serialisation is limited to maps with String keys. */
    private Map<String, Set<Integer>> seenDinosaurs = newHashMap();
    private int vision = VISION;
    private double expectation;
    private int energy = INITIAL_ENERGY;
    private int injuries = 0;
    private int money;
    /** The tick (during TICKS_PER_STEP) on which this guest moves. */
    private int tickIndex;

    public Guest(IntegerIdType guestType, Position position) {
        this.guestType = guestType;
        this.position = position;
        expectation = GameWorld.getInstance().getZooStats().getRecentExperience();
        this.money = GameWorld.getInstance().getPopulation().getAverageWealth();
        tickIndex = RandomUtils.getRandomInt(TICKS_PER_STEP);
    }

    /** No arg constructor for game serialization. */
    public Guest() {
        tickIndex = RandomUtils.getRandomInt(TICKS_PER_STEP);
    }

    @Override
    public TextureRegion getTexture() {
        if (textureRegion == null) {
            textureRegion = GuestLoader.INSTANCE.getTextureRegion(guestType);
        }
        return textureRegion;
    }

    @Override
    public TextureRegion getForeground() {
        return null;
    }

    @Override
    public void tick(long tick) {
        if (tick % TICKS_PER_STEP == tickIndex && tickLastUpdated != tick && tickBehaviour != null) {
            tickLastUpdated = tick;
            tickBehaviour.tick(tick);
        }
    }

    @Override
    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    @Override
    public boolean isStackable() {
        return true;
    }

    @Override
    public ObjectType getObjectType() {
        return guestType;
    }

    public Direction getLastDirection() {
        return lastDirection;
    }

    public void setLastDirection(Direction lastDirection) {
        this.lastDirection = lastDirection;
    }

    public int getVision() {
        return vision;
    }

    public void setVision(int vision) {
        this.vision = vision;
    }

    public int getEnergy() {
        return energy;
    }

    public void setEnergy(int energy) {
        this.energy = energy;
    }

    /** Return true if the guest has seen the given dinosaur before. */
    private boolean hasSeenDinosaur(Dinosaur dinosaur) {
        String dinosaurTypeString = dinosaur.getObjectType().toString();
        if (!seenDinosaurs.containsKey(dinosaurTypeString)) {
            return false;
        }
        return seenDinosaurs.get(dinosaurTypeString).contains(dinosaur.getId());
    }

    /** Register that the guest saw a dinosaur and potentially increase their energy. */
    public void sawDinosaur(Dinosaur dinosaur) {
        if (seenDinosaurs != null && !hasSeenDinosaur(dinosaur)) {
            // Seeing a new dinosaur resets the guest's energy level.
            energy = INITIAL_ENERGY;
            Set<Integer> seenOfType = seenDinosaurs.get(dinosaur.getObjectType().toString());
            if (seenOfType == null) {
                seenOfType = newHashSet();
                seenDinosaurs.put(dinosaur.getObjectType().toString(), seenOfType);
            }
            seenOfType.add(dinosaur.getId());
        }
    }

    /** Get a score for the guest's experience. */
    private double getSeenDinosaurScore() {
        double score = 1;
        if (seenDinosaurs != null) {
            for (String dinosaurTypeString : seenDinosaurs.keySet()) {
                score *= (seenDinosaurs.get(dinosaurTypeString).size() + 1);
            }
        }
        // Score is reduced per injury.
        score /= (injuries + 1);
        return score;
    }

    public void exitZoo() {
        GameWorld instance = GameWorld.getInstance();

        double seenDinosaurScore = getSeenDinosaurScore();
        instance.getZooStats().addRecentExperience(seenDinosaurScore, expectation);

        instance.removeObject(this);
    }

    /**
     * Try to give some money to the zoo.
     *
     * @param amount The amount to try to give.
     * @return false if the guest didn't have enough money to spend.
     */
    public boolean payZoo(int amount) {
        if (amount > money) {
            return false;
        }
        money -= amount;
        GameWorld.getInstance().getZooStats().addMoney(amount);
        return true;
    }

    public void attacked(int attack) {
        if (attack > 0) {
            injuries += attack;
            energy /= attack;
        }
    }
}
