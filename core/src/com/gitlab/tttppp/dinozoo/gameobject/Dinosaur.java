package com.gitlab.tttppp.dinozoo.gameobject;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.gitlab.tttppp.dinozoo.gameobject.behaviour.DinosaurBehaviour;
import com.gitlab.tttppp.dinozoo.gameobject.behaviour.TickBehaviour;
import com.gitlab.tttppp.dinozoo.gamerenderer.DinosaurAdultLoader;
import com.gitlab.tttppp.dinozoo.gamerenderer.DinosaurLoader;
import com.gitlab.tttppp.dinozoo.gameworld.DinosaurType;
import com.gitlab.tttppp.dinozoo.gameworld.GameWorld;
import com.gitlab.tttppp.dinozoo.gameworld.ItemType;
import com.gitlab.tttppp.dinozoo.gameworld.ObjectType;
import com.gitlab.tttppp.dinozoo.gameworld.Position;
import com.gitlab.tttppp.dinozoo.utils.Log;
import com.gitlab.tttppp.dinozoo.utils.RandomUtils;

public class Dinosaur implements GameObject {
    private static final long TICKS_BETWEEN_EGGS = (long) Math.floor(GameWorld.TICKS_IN_YEAR);

    private DinosaurType dinosaurType;
    private Position position;
    private transient TextureRegion textureRegion;
    private transient TickBehaviour tickBehaviour = new DinosaurBehaviour(this);
    private long tickLastUpdated;
    private long tickBorn;
    private long ticksUntilAdult;
    private long ticksUntilDead;
    private int id = RandomUtils.getRandomInt();
    private boolean isFemale = RandomUtils.flipCoin(1, 1);
    private boolean isPregnant = false;
    private long ticksUntilNextEgg = 0;
    private int health;

    public Dinosaur(DinosaurType dinosaurType, Position position) {
        this.dinosaurType = dinosaurType;
        this.position = position;
        tickBorn = GameWorld.getInstance().getTick();
        ticksUntilAdult = dinosaurType.getTicksAsChild();
        ticksUntilDead = dinosaurType.getLifeTime();
        // Health is set based on the log of the weight.
        health = Integer.toBinaryString(dinosaurType.getWeight()).length();
    }

    /** No arg constructor for game serialization. */
    public Dinosaur() {
    }

    public void init() {
        // Handle upgrade from old versions.
        if (tickBorn == 0) {
            // Guess at the number of ticks the dinosaur has been alive
            long ticksAlive = dinosaurType.getTicksAsChild() - ticksUntilAdult;
            tickBorn = GameWorld.getInstance().getTick() - ticksAlive;
            long randomAmount = RandomUtils.getRandomInt(Integer.MAX_VALUE) % dinosaurType.getLifeTime();
            ticksUntilDead = dinosaurType.getLifeTime() + randomAmount;
        }
        if (ticksUntilAdult <= 0) {
            textureRegion = DinosaurAdultLoader.INSTANCE.getTextureRegion(dinosaurType);
        }
    }

    @Override
    public TextureRegion getTexture() {
        if (textureRegion == null) {
            textureRegion = DinosaurLoader.INSTANCE.getTextureRegion(dinosaurType);
        }
        return textureRegion;
    }

    @Override
    public TextureRegion getForeground() {
        return null;
    }

    @Override
    public void tick(long tick) {
        if (tickLastUpdated == 0) {
            tickLastUpdated = tick;
        }
        if (tickLastUpdated != tick && dinosaurType != null) {
            if (ticksUntilDead > 0) {
                ticksUntilDead -= (tick - tickLastUpdated);
                if (ticksUntilDead <= 0) {
                    GameWorld gameWorld = GameWorld.getInstance();
                    gameWorld.removeObject(this);
                    gameWorld.putObject(this.getPosition(), new Item(ItemType.SKELETON, this.getPosition()));
                    return;
                }
            }
            if (tickBehaviour != null && tick % dinosaurType.getTicksPerStep() == 0) {
                tickBehaviour.tick(tick);
            }
            if (ticksUntilAdult > 0) {
                ticksUntilAdult -= (tick - tickLastUpdated);
                if (ticksUntilAdult <= 0) {
                    textureRegion = DinosaurAdultLoader.INSTANCE.getTextureRegion(dinosaurType);
                }
            }
            if (ticksUntilNextEgg > 0) {
                ticksUntilNextEgg -= (tick - tickLastUpdated);
            }
            tickLastUpdated = tick;
        }
    }

    @Override
    public Position getPosition() {
        return position;
    }

    @Override
    public void setPosition(Position position) {
        this.position = position;
    }

    @Override
    public boolean isStackable() {
        return true;
    }

    @Override
    public ObjectType getObjectType() {
        return dinosaurType;
    }

    public boolean isAdult() {
        return (ticksUntilAdult == 0);
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return "Dinosaur " + dinosaurType + ": " + id;
    }

    public double getPressure() {
        return getWeight() / dinosaurType.getWalkingLegs();
    }

    public double getWeight() {
        if (dinosaurType == null) {
            return 0;
        }
        if (isAdult()) {
            return dinosaurType.getWeight();
        }
        return dinosaurType.getWeight() * getAgeRatio();
    }

    public double getHeight() {
        if (dinosaurType == null) {
            return 0;
        }
        if (isAdult()) {
            return dinosaurType.getHeight();
        }
        return dinosaurType.getHeight() * getAgeRatio();
    }

    /** This returns 0.5 for a newborn, 1 for an adult and a linear interpolation for anything in between. */
    private double getAgeRatio() {
        if (isAdult()) {
            return 1.0;
        }
        return 0.5 + (0.5 * ticksUntilAdult / dinosaurType.getTicksAsChild());
    }

    public boolean isFemale() {
        return isFemale;
    }

    public boolean isPregnant() {
        return isPregnant;
    }

    public void setPregnant(boolean isPregnant) {
        if (this.ticksUntilNextEgg <= 0) {
            this.isPregnant = isPregnant;
            if (isPregnant == false) {
                this.ticksUntilNextEgg = TICKS_BETWEEN_EGGS;
            }
        }
    }

    public void attacked(int attack) {
        health -= attack;
        // Kill the dinosaur next tick.
        if (health <= 0) {
            ticksUntilDead = 1; // Slight hack so that dinosaur is killed on next tick.
            Log.log("Dinosaur of type " + dinosaurType + " killed by attack of " + attack);
        }
    }

    public int getHealth() {
        return health;
    }

    public int getAttackScore() {
        return (int) (RandomUtils.getRandomInt(dinosaurType.getStrength()) * getAgeRatio());
    }

    public int getDefenceScore() {
        return (int) (RandomUtils.getRandomInt(dinosaurType.getDefence()) * getAgeRatio());
    }
}
