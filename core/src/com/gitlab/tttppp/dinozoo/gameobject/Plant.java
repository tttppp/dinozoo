package com.gitlab.tttppp.dinozoo.gameobject;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.gitlab.tttppp.dinozoo.gamerenderer.PlantLoader;
import com.gitlab.tttppp.dinozoo.gameworld.ObjectType;
import com.gitlab.tttppp.dinozoo.gameworld.PlantType;
import com.gitlab.tttppp.dinozoo.gameworld.Position;
import com.gitlab.tttppp.dinozoo.utils.RandomUtils;

import static com.gitlab.tttppp.dinozoo.gameworld.GameWorld.TICKS_IN_DAY;

public class Plant implements GameObject {
    private static final int TICKS_TO_GROW = 30 * TICKS_IN_DAY;

    private PlantType plantType;
    private Position position;
    private transient TextureRegion textureRegion;
    private int ticksBeforeGrowth;

    public Plant(PlantType plantType, Position position) {
        this.plantType = plantType;
        this.position = position;
    }

    /** No arg constructor for game serialization. */
    public Plant() {
    }

    @Override
    public TextureRegion getTexture() {
        if (textureRegion == null) {
            textureRegion = PlantLoader.INSTANCE.getTextureRegion(plantType);
        }
        return textureRegion;
    }

    @Override
    public TextureRegion getForeground() {
        return null;
    }

    @Override
    public void tick(long tick) {
        if (ticksBeforeGrowth > 0) {
            ticksBeforeGrowth--;
            if (ticksBeforeGrowth == 0 && plantType != null) {
                setPlantType(plantType.getNextStage());
            }
        }
    }

    public void setPlantType(PlantType plantType) {
        this.plantType = plantType;
        textureRegion = PlantLoader.INSTANCE.getTextureRegion(plantType);
        if (plantType.getNextStage() != null) {
            ticksBeforeGrowth = (int) Math.round(0.5 * TICKS_TO_GROW + 0.5 * RandomUtils.getRandomInt(TICKS_TO_GROW));
        }
    }

    @Override
    public Position getPosition() {
        return position;
    }

    @Override
    public void setPosition(Position position) {
        this.position = position;
    }

    @Override
    public boolean isStackable() {
        return false;
    }

    @Override
    public ObjectType getObjectType() {
        return plantType;
    }
}
