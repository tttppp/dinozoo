package com.gitlab.tttppp.dinozoo.gameobject;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.gitlab.tttppp.dinozoo.gamerenderer.EggLoader;
import com.gitlab.tttppp.dinozoo.gameworld.DinosaurType;
import com.gitlab.tttppp.dinozoo.gameworld.GameWorld;
import com.gitlab.tttppp.dinozoo.gameworld.ObjectType;
import com.gitlab.tttppp.dinozoo.gameworld.Position;

import java.util.List;

import static com.gitlab.tttppp.dinozoo.gameworld.GameWorld.TICKS_IN_DAY;
import static com.gitlab.tttppp.dinozoo.gameworld.ItemType.NEST;
import static com.gitlab.tttppp.dinozoo.gameworld.ItemType.SPLAT_EGG;

public class Egg implements GameObject {
    private DinosaurType dinosaurType;
    private Position position;
    private transient TextureRegion textureRegion;
    private int hatchTicks;

    public Egg(DinosaurType dinosaurType, Position position) {
        this.dinosaurType = dinosaurType;
        this.position = position;
        this.hatchTicks = dinosaurType.getEggTime() * TICKS_IN_DAY;
    }

    /** No arg constructor for game serialization. */
    public Egg() {
    }

    @Override
    public TextureRegion getTexture() {
        if (textureRegion == null) {
            textureRegion = EggLoader.INSTANCE.getTextureRegion(dinosaurType);
        }
        return textureRegion;
    }

    @Override
    public TextureRegion getForeground() {
        return null;
    }

    @Override
    public void tick(long tick) {
        hatchTicks--;
        GameWorld gameWorld = GameWorld.getInstance();
        List<GameObject> objectsPresent = gameWorld.getGameObjectsAt(position);
        boolean onNest = false;
        for (GameObject objectPresent : objectsPresent) {
            if (objectPresent.getObjectType() == NEST) {
                onNest = true;
                break;
            }
        }
        if (!onNest) {
            gameWorld.removeObject(this);
            gameWorld.putObject(position, new Item(SPLAT_EGG, position));
        }
        if (hatchTicks <= 0) {
            gameWorld.removeCurrentObject();
            Dinosaur dinosaur = new Dinosaur(dinosaurType, position);
            gameWorld.putObject(position, dinosaur);
        }
    }

    @Override
    public Position getPosition() {
        return position;
    }

    @Override
    public void setPosition(Position position) {
        this.position = position;
    }

    @Override
    public boolean isStackable() {
        return true;
    }

    @Override
    public ObjectType getObjectType() {
        return dinosaurType;
    }
}
