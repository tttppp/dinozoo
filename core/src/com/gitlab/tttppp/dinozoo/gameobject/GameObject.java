package com.gitlab.tttppp.dinozoo.gameobject;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.gitlab.tttppp.dinozoo.gameworld.ObjectType;
import com.gitlab.tttppp.dinozoo.gameworld.Position;

public interface GameObject {
    TextureRegion getTexture();

    TextureRegion getForeground();

    void tick(long tick);

    Position getPosition();

    void setPosition(Position position);

    boolean isStackable();

    ObjectType getObjectType();
}
