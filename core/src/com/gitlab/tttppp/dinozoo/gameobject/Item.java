package com.gitlab.tttppp.dinozoo.gameobject;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.gitlab.tttppp.dinozoo.gamerenderer.ItemLoader;
import com.gitlab.tttppp.dinozoo.gameworld.GameWorld;
import com.gitlab.tttppp.dinozoo.gameworld.ItemType;
import com.gitlab.tttppp.dinozoo.gameworld.ObjectType;
import com.gitlab.tttppp.dinozoo.gameworld.Position;

import static com.gitlab.tttppp.dinozoo.gameworld.GameWorld.TICKS_IN_DAY;

public class Item implements GameObject {
    private static final long DECORATION_DISPLAY_TICKS = 365 * TICKS_IN_DAY;
    private ItemType itemType;
    private Position position;
    private transient TextureRegion textureRegion;
    private transient TextureRegion foreground;
    private long ticksToDisappear = -1;

    public Item(ItemType itemType, Position position) {
        this.itemType = itemType;
        this.position = position;
        if (itemType == ItemType.SKELETON || itemType == ItemType.SPLAT_EGG) {
            this.ticksToDisappear = DECORATION_DISPLAY_TICKS;
        }
    }

    /** No arg constructor for game serialization. */
    public Item() {
    }

    @Override
    public TextureRegion getTexture() {
        if (textureRegion == null) {
            textureRegion = ItemLoader.INSTANCE.getTextureRegion(itemType);
        }
        return textureRegion;
    }

    @Override
    public TextureRegion getForeground() {
        if (itemType != null && itemType.getForeground() != null && foreground == null) {
            foreground = ItemLoader.INSTANCE.getTextureRegion((ItemType) itemType.getForeground());
        }
        return foreground;
    }

    @Override
    public void tick(long tick) {
        if (ticksToDisappear > 0) {
            ticksToDisappear--;
            if (ticksToDisappear <= 0) {
                GameWorld.getInstance().removeObject(this);
            }
        }
    }

    @Override
    public Position getPosition() {
        return position;
    }

    @Override
    public void setPosition(Position position) {
        this.position = position;
    }

    @Override
    public boolean isStackable() {
        return itemType.isStackable();
    }

    @Override
    public ObjectType getObjectType() {
        return itemType;
    }
}
