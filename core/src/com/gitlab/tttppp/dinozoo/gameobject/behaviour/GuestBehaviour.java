package com.gitlab.tttppp.dinozoo.gameobject.behaviour;

import com.gitlab.tttppp.dinozoo.gameobject.Building;
import com.gitlab.tttppp.dinozoo.gameobject.Dinosaur;
import com.gitlab.tttppp.dinozoo.gameobject.GameObject;
import com.gitlab.tttppp.dinozoo.gameobject.Guest;
import com.gitlab.tttppp.dinozoo.gameobject.Plant;
import com.gitlab.tttppp.dinozoo.gameobject.Wall;
import com.gitlab.tttppp.dinozoo.gameworld.BuildingType;
import com.gitlab.tttppp.dinozoo.gameworld.Direction;
import com.gitlab.tttppp.dinozoo.gameworld.GameWorld;
import com.gitlab.tttppp.dinozoo.gameworld.GroundSubType;
import com.gitlab.tttppp.dinozoo.gameworld.GroundType;
import com.gitlab.tttppp.dinozoo.gameworld.Position;
import com.gitlab.tttppp.dinozoo.utils.Log;
import com.gitlab.tttppp.dinozoo.utils.RandomUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static com.gitlab.tttppp.dinozoo.gameworld.Direction.ADJACENCIES;
import static com.gitlab.tttppp.dinozoo.gameworld.GroundSubType.PATH;
import static com.gitlab.tttppp.dinozoo.gameworld.Habitat.DOMESTIC;

public class GuestBehaviour implements TickBehaviour {
    /** A bonus for how few people there are in a destination. */
    private static final int VACANT_BONUS = 10;
    /** A bonus for there not being a building present (in effect this works out as a penalty for buildings). */
    private static final int BUILDING_PENALTY = 10000;
    private static final int BACKTRACK_PENALTY = 100;
    private static final int EXIT_SEARCH_EFFORT = 1000;

    private Guest guest;

    public GuestBehaviour(Guest guest) {
        this.guest = guest;
    }

    @Override
    public void tick(long tick) {
        GameWorld gameWorld = GameWorld.getInstance();
        // Guest looks.
        Set<GameObject> gameObjects = VisionHelper.lookIn(guest.getLastDirection(), guest.getVision(), guest.getPosition());
        for (GameObject gameObject : gameObjects) {
            if (gameObject instanceof Dinosaur) {
                guest.sawDinosaur((Dinosaur) gameObject);
            }
        }
        // Move guest.
        Position destination = null;
        Map<Position, Direction> directionMap = new HashMap<>();
        int pathOptions = 0;
        for (Direction direction : ADJACENCIES) {
            Position option = direction.applyTo(guest.getPosition());
            if (option != null) {
                directionMap.put(option, direction);
                GroundSubType groundType = gameWorld.getGroundTypeAt(option);
                if (groundType == PATH) {
                    pathOptions++;
                }
            }
        }
        // Try to find exit if has run out of energy or is at junction.
        if (guest.getEnergy() == 0 || pathOptions >= 3 && guest.getEnergy() < 0 || guest.getEnergy() <= -guest.INITIAL_ENERGY) {
            for (Building exit : gameWorld.getExits()) {
                Direction direction;
                if (guest.getEnergy() <= -2 * guest.INITIAL_ENERGY) {
                    // Guest is so fed up that they ignore the standard ground preferences.
                    direction = PathingHelper.searchForRouteIgnoringGround(guest.getPosition(), exit.getPosition(), EXIT_SEARCH_EFFORT);
                } else {
                    direction = PathingHelper.searchForRouteTo(guest.getPosition(), exit.getPosition(), EXIT_SEARCH_EFFORT);
                }
                if (direction != null) {
                    destination = direction.applyTo(guest.getPosition());
                } else {
                    Log.log("Failed to find route to exit at " + exit.getPosition() + " from " + guest.getPosition());
                }
            }
        }
        Map<Position, Integer> peopleInDirection = new HashMap<>();
        // Otherwise pick direction using preferences.
        if (destination == null) {
            Map<Position, Long> preferences = new HashMap<>();
            for (Direction direction : directionMap.values()) {
                Position option = direction.applyTo(guest.getPosition());
                if (option != null) {
                    GroundType groundType = gameWorld.getGroundTypeAt(option).getGroundType();
                    Long value = Long.valueOf(DOMESTIC.getGroundTypeBonus().get(groundType));

                    // Check for obstructions.
                    GameObject fixedObject = gameWorld.getFixedObjects().get(option);
                    if (fixedObject instanceof Building || fixedObject instanceof Wall || fixedObject instanceof Plant) {
                        boolean wantsToUseExit = false;
                        if (fixedObject instanceof Building && guest.getEnergy() <= 0) {
                            Building building = (Building) fixedObject;
                            if (building.getBuildingType() == BuildingType.ENTRY_NS || building.getBuildingType() == BuildingType.ENTRY_EW) {
                                wantsToUseExit = true;
                            }
                        }
                        if (wantsToUseExit) {
                            value *= BUILDING_PENALTY;
                        }
                    } else {
                        // No buildings or walls.
                        value *= BUILDING_PENALTY;
                    }

                    // Check for other guests.
                    int personCount = 0;
                    for (GameObject gameObject : gameWorld.getStackableObjects().get(option)) {
                        if (gameObject instanceof Guest) {
                            personCount++;
                        }
                    }
                    peopleInDirection.put(option, personCount);
                    value *= (1 + VACANT_BONUS / (personCount + 1));
                    if (direction != guest.getLastDirection().opposite()) {
                        value *= BACKTRACK_PENALTY;
                    }
                    preferences.put(option, value);
                }
            }
            destination = RandomUtils.pickWeightedOption(preferences);
        }
        // Move the guest.
        if (destination != null) {
            gameWorld.moveObject(guest, destination);
            guest.setLastDirection(directionMap.get(destination));
        }
        // Reduce remaining energy.
        int crowdingCost = 0;
        if (destination != null && peopleInDirection.containsKey(destination)) {
            crowdingCost = peopleInDirection.get(destination);
        }
        guest.setEnergy(guest.getEnergy() - 1 - crowdingCost);
    }
}
