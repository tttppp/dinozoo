package com.gitlab.tttppp.dinozoo.gameobject.behaviour;

import com.gitlab.tttppp.dinozoo.gameobject.GameObject;
import com.gitlab.tttppp.dinozoo.gameobject.Wall;
import com.gitlab.tttppp.dinozoo.gameworld.Direction;
import com.gitlab.tttppp.dinozoo.gameworld.GameWorld;
import com.gitlab.tttppp.dinozoo.gameworld.Position;

import java.util.HashMap;
import java.util.Map;

/** Behaviour for classes that change appearance based on where they are. */
public class NeighbourhoodAppearanceBehaviour implements TickBehaviour {
    private Wall wall;

    public NeighbourhoodAppearanceBehaviour(Wall wall) {
        this.wall = wall;
    }

    @Override
    public void tick(long tick) {
        Map<Direction, GameObject> neighbours = new HashMap<>();
        for (Direction direction : Direction.ADJACENCIES) {
            Position position = direction.applyTo(wall.getPosition());
            if (position != null) {
                GameObject gameObject = GameWorld.getInstance().getFixedObjects().get(position);
                if (gameObject != null) {
                    neighbours.put(direction, gameObject);
                }
            }
        }
        wall.neighboursAre(neighbours);
    }
}
