package com.gitlab.tttppp.dinozoo.gameobject.behaviour;

public interface TickBehaviour {
    void tick(long tick);
}
