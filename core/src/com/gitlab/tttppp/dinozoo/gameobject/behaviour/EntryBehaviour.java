package com.gitlab.tttppp.dinozoo.gameobject.behaviour;

import com.gitlab.tttppp.dinozoo.gameobject.GameObject;
import com.gitlab.tttppp.dinozoo.gameobject.Guest;
import com.gitlab.tttppp.dinozoo.gamerenderer.GuestLoader;
import com.gitlab.tttppp.dinozoo.gameworld.Direction;
import com.gitlab.tttppp.dinozoo.gameworld.GameWorld;
import com.gitlab.tttppp.dinozoo.gameworld.IntegerIdType;
import com.gitlab.tttppp.dinozoo.gameworld.Position;
import com.gitlab.tttppp.dinozoo.utils.Log;
import com.gitlab.tttppp.dinozoo.utils.RandomUtils;

import static com.gitlab.tttppp.dinozoo.gameworld.GameWorld.MAP_HEIGHT;
import static com.gitlab.tttppp.dinozoo.gameworld.GameWorld.MAP_WIDTH;
import static com.gitlab.tttppp.dinozoo.gameworld.GameWorld.TICKS_IN_DAY;

public class EntryBehaviour implements TickBehaviour {
    private static final int MIN_TICKS_UNTIL_GUEST = TICKS_IN_DAY / 2;
    private static final int MAX_TICKS_UNTIL_GUEST = TICKS_IN_DAY;

    private int ticksUntilGuest;
    private Position entryPosition;
    private Position spawn;

    public EntryBehaviour(Position entryPosition) {
        this.entryPosition = entryPosition;
        Direction direction;
        if (entryPosition.getX() == 0) {
            direction = Direction.RIGHT;
        } else if (entryPosition.getX() == MAP_WIDTH - 1) {
            direction = Direction.LEFT;
        } else if (entryPosition.getY() == 0) {
            direction = Direction.DOWN;
        } else if (entryPosition.getY() == MAP_HEIGHT - 1) {
            direction = Direction.UP;
        } else {
            throw new IllegalStateException("Entry must be on map border - found at " + entryPosition);
        }
        this.spawn = direction.applyTo(entryPosition);
        ticksUntilGuest = MIN_TICKS_UNTIL_GUEST + RandomUtils.getRandomInt(MAX_TICKS_UNTIL_GUEST - MIN_TICKS_UNTIL_GUEST);
    }

    /** No arg constructor for game serialization. */
    public EntryBehaviour() {
    }

    @Override
    public void tick(long tick) {
        GameWorld gameWorld = GameWorld.getInstance();
        // Wait for guests to "pass entrance".
        ticksUntilGuest--;
        if (ticksUntilGuest <= 0) {
            // Is zoo good value for money?
            if (gameWorld.getPopulation().populationWillPayForTicket()) {
                IntegerIdType integerId = new IntegerIdType((int) (tick % GuestLoader.INSTANCE.getNumberOfGuestTextures()));
                Log.log("Success! Guest id " + integerId);
                Guest guest = new Guest(integerId, spawn);
                // Can guest afford ticket?
                if (guest.payZoo(gameWorld.getZooStats().getTicketPrice())) {
                    gameWorld.putObject(spawn, guest);
                }
            }
            ticksUntilGuest += MIN_TICKS_UNTIL_GUEST + RandomUtils.getRandomInt(MAX_TICKS_UNTIL_GUEST - MIN_TICKS_UNTIL_GUEST);
        }
        // Remove any guests from world.
        for (GameObject gameObject : gameWorld.getGameObjectsAt(entryPosition)) {
            if (gameObject instanceof Guest) {
                ((Guest) gameObject).exitZoo();
            }
        }
    }
}
