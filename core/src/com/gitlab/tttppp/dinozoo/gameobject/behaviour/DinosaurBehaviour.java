package com.gitlab.tttppp.dinozoo.gameobject.behaviour;

import com.gitlab.tttppp.dinozoo.gameobject.Building;
import com.gitlab.tttppp.dinozoo.gameobject.Dinosaur;
import com.gitlab.tttppp.dinozoo.gameobject.Egg;
import com.gitlab.tttppp.dinozoo.gameobject.GameObject;
import com.gitlab.tttppp.dinozoo.gameobject.Guest;
import com.gitlab.tttppp.dinozoo.gameobject.Land;
import com.gitlab.tttppp.dinozoo.gameobject.Plant;
import com.gitlab.tttppp.dinozoo.gameobject.Wall;
import com.gitlab.tttppp.dinozoo.gameworld.DinosaurType;
import com.gitlab.tttppp.dinozoo.gameworld.Direction;
import com.gitlab.tttppp.dinozoo.gameworld.GameWorld;
import com.gitlab.tttppp.dinozoo.gameworld.GroundSubType;
import com.gitlab.tttppp.dinozoo.gameworld.GroundType;
import com.gitlab.tttppp.dinozoo.gameworld.Habitat;
import com.gitlab.tttppp.dinozoo.gameworld.ItemType;
import com.gitlab.tttppp.dinozoo.gameworld.PlantType;
import com.gitlab.tttppp.dinozoo.gameworld.Position;
import com.gitlab.tttppp.dinozoo.utils.Log;
import com.gitlab.tttppp.dinozoo.utils.RandomUtils;

import java.util.HashMap;
import java.util.Map;

import static com.gitlab.tttppp.dinozoo.gameworld.Direction.ADJACENCIES;
import static com.gitlab.tttppp.dinozoo.gameworld.GroundSubType.DIRT;
import static com.gitlab.tttppp.dinozoo.gameworld.GroundSubType.FOOTPRINTS;
import static com.gitlab.tttppp.dinozoo.gameworld.GroundType.BOG;
import static com.gitlab.tttppp.dinozoo.gameworld.GroundType.GRASS;
import static com.gitlab.tttppp.dinozoo.gameworld.GroundType.WATER;
import static com.gitlab.tttppp.dinozoo.gameworld.PlantType.BUSH_BARE;
import static com.gitlab.tttppp.dinozoo.gameworld.PlantType.TREE_BARE;

public class DinosaurBehaviour implements TickBehaviour {
    private Dinosaur dinosaur;

    public DinosaurBehaviour(Dinosaur dinosaur) {
        this.dinosaur = dinosaur;
    }

    @Override
    public void tick(long tick) {
        GameWorld gameWorld = GameWorld.getInstance();
        Map<Position, Long> preferences = new HashMap<>();
        DinosaurType dinosaurType = (DinosaurType) dinosaur.getObjectType();
        for (Direction direction : ADJACENCIES) {
            Position destination = direction.applyTo(dinosaur.getPosition());
            if (destination != null) {
                // Check the dinosaur isn't walking on the zoo perimeter.
                Position oneStepMore = direction.applyTo(destination);
                if (oneStepMore != null) {
                    GroundSubType groundSubType = gameWorld.getGroundTypeAt(destination);
                    GroundType groundType = groundSubType.getGroundType();
                    Long value = Long.valueOf(dinosaurType.getGroundPreferences().get(groundType));
                    GameObject gameObject = gameWorld.getFixedObjects().get(destination);
                    // Don't walk on buildings or walls.
                    if (gameObject instanceof Building || gameObject instanceof Wall) {
                        continue;
                    }
                    // Sea creatures have preferences about the depth of water.
                    if (groundType == WATER && dinosaurType.getHabitat() == Habitat.SEA) {
                        value += dinosaurType.getWaterDepthBonus(groundSubType);
                    }
                    preferences.put(destination, value);
                }
            }
        }
        // Change ground.
        double pressure = dinosaur.getPressure();
        GroundSubType currentGround = gameWorld.getGroundTypeAt(dinosaur.getPosition());
        GroundSubType eventualGroundType = gameWorld.getEventualGroundTypeAt(dinosaur.getPosition());
        if (pressure > 100 && currentGround.getGroundType() == GRASS || pressure > 50 && currentGround.getGroundType() == BOG) {
            // Leaves footprints.
            if (RandomUtils.flipCoin((int) Math.log(pressure), 3)) {
                gameWorld.putLand(dinosaur.getPosition(), new Land(FOOTPRINTS, dinosaur.getPosition(), DIRT, eventualGroundType));
            }
        } else if (dinosaurType.getDiet() == Diet.HERBIVORE || dinosaurType.getDiet() == Diet.OMNIVORE) {
            // Eats grass.
            if (currentGround.isEdible()) {
                if (RandomUtils.flipCoin(1, 3)) {
                    gameWorld.putLand(dinosaur.getPosition(), new Land(DIRT, dinosaur.getPosition(), eventualGroundType));
                }
            }
            // Eats neighbouring plant.
            for (Direction direction : ADJACENCIES) {
                Position destination = direction.applyTo(dinosaur.getPosition());
                if (destination != null) {
                    GameObject gameObject = gameWorld.getFixedObjects().get(destination);
                    if (gameObject instanceof Plant) {
                        if (RandomUtils.flipCoin(1, 10)) {
                            Plant plant = (Plant) gameObject;
                            PlantType plantType = (PlantType) plant.getObjectType();
                            // If dinosaur is at least a tenth of the height of the plant it can eat it.
                            if (plantType.getHeight() != null && plantType.getNextStage() != null && dinosaur.getHeight() * 10 > plantType.getHeight()) {
                                Log.log("Dinosaur eating plant: " + dinosaurType + " " + dinosaur.getHeight() + " " + dinosaur.isAdult());
                                plant.setPlantType(plantType.eatenType());
                            }
                        }
                    }
                }
            }
        }

        // Sea creatures need to be in water.
        if (dinosaurType.getHabitat() == Habitat.SEA) {
            if (currentGround.getGroundType() != GroundType.WATER && RandomUtils.flipCoin(1, GameWorld.TICKS_IN_DAY * 30)) {
                dinosaur.attacked(1);
            }
        }

        // Adult females can become pregnant.
        if (dinosaur.isAdult() && dinosaur.isFemale()) {
            if (!dinosaur.isPregnant()) {
                for (Direction direction : ADJACENCIES) {
                    Position destination = direction.applyTo(dinosaur.getPosition());
                    if (destination != null) {
                        for (GameObject gameObject : gameWorld.getStackableObjects().get(destination)) {
                            if (gameObject instanceof Dinosaur) {
                                Dinosaur otherDinosaur = (Dinosaur) gameObject;
                                if (otherDinosaur.getObjectType() == dinosaurType && otherDinosaur.isAdult() && !otherDinosaur.isFemale()) {
                                    dinosaur.setPregnant(true);
                                    break;
                                }
                            }
                        }
                    }
                }
            } else {
                for (Direction direction : ADJACENCIES) {
                    Position destination = direction.applyTo(dinosaur.getPosition());
                    if (destination != null) {
                        GameObject gameObject = gameWorld.getFixedObjects().get(destination);
                        if (gameObject != null && gameObject.getObjectType() == ItemType.NEST) {
                            boolean foundEgg = false;
                            for (GameObject stackableObject : gameWorld.getStackableObjects().get(destination)) {
                                if (stackableObject instanceof Egg) {
                                    foundEgg = true;
                                    break;
                                }
                            }
                            if (!foundEgg) {
                                Log.log("Laying egg from " + dinosaurType + dinosaur.getId() + " at " + destination);
                                gameWorld.putObject(destination, new Egg(dinosaurType, destination));
                                dinosaur.setPregnant(false);
                                break;
                            }
                        }
                    }
                }
            }
        }
        Position position = RandomUtils.pickWeightedOption(preferences);
        if (position != null) {
            // Interact with any other dinosaurs or people it's moving onto.
            for (GameObject gameObject : gameWorld.getStackableObjects().get(position)) {
                if (gameObject instanceof Dinosaur) {
                    Dinosaur other = (Dinosaur) gameObject;
                    int attackChance = (other.getObjectType() == dinosaurType ? 1 : 40);
                    if (RandomUtils.flipCoin(attackChance, 100)) {
                        // The two dinosaurs fight.
                        int attack = Math.max(0, dinosaur.getAttackScore() - other.getDefenceScore());
                        other.attacked(attack);
                        attack = Math.max(0, other.getAttackScore() - dinosaur.getDefenceScore());
                        dinosaur.attacked(attack);
                    }
                } else if (gameObject instanceof Guest) {
                    Guest guest = (Guest) gameObject;
                    if (RandomUtils.flipCoin(30, 70)) {
                        // Dinosaur attacks the guest (who has defence of 3).
                        int attack = Math.max(0, dinosaur.getAttackScore() - 3);
                        guest.attacked(attack);
                    }
                }
            }

            gameWorld.moveObject(dinosaur, position);
        }
    }
}
