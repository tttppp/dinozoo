package com.gitlab.tttppp.dinozoo.gameobject.behaviour;

import com.gitlab.tttppp.dinozoo.gameobject.GameObject;
import com.gitlab.tttppp.dinozoo.gameworld.BuildingType;
import com.gitlab.tttppp.dinozoo.gameworld.DinosaurType;
import com.gitlab.tttppp.dinozoo.gameworld.Direction;
import com.gitlab.tttppp.dinozoo.gameworld.GameWorld;
import com.gitlab.tttppp.dinozoo.gameworld.ItemType;
import com.gitlab.tttppp.dinozoo.gameworld.ObjectType;
import com.gitlab.tttppp.dinozoo.gameworld.PlantType;
import com.gitlab.tttppp.dinozoo.gameworld.Position;
import com.gitlab.tttppp.dinozoo.utils.Pair;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import static com.gitlab.tttppp.dinozoo.gameworld.Direction.ADJACENCIES;

public class VisionHelper {
    /** Private constructor for helper class. */
    private VisionHelper() {
    }

    /**
     * Look in the two diagonal directions for the given direction and return the objects that can be seen.
     *
     * @param direction The direction to look.
     * @param vision    The unobstructed distance that can be seen.
     * @param start     The position of the observer.
     * @return The list of objects that can be seen.
     */
    public static Set<GameObject> lookIn(Direction direction, int vision, Position start) {
        Set<GameObject> canSee = new HashSet<>();
        for (Direction directionB : ADJACENCIES) {
            if (directionB != direction && directionB != direction.opposite()) {
                canSee.addAll(lookIn(direction, directionB, vision, start));
            }
        }
        return canSee;
    }

    /**
     * Look in a particular (diagonal) direction and return the objects that can be seen.
     *
     * @param directionA One component of the diagonal.
     * @param directionB The other component of the diagonal.
     * @param vision     The unobstructed distance that can be seen.
     * @param start      The position of the observer.
     * @return The list of objects that can be seen.
     */
    public static Set<GameObject> lookIn(Direction directionA, Direction directionB, int vision, Position start) {
        if (directionA == directionB || directionA.opposite() == directionB) {
            throw new IllegalStateException("Directions must be perpendicular: " + directionA + ", " + directionB);
        }
        Set<GameObject> canSee = new HashSet<>();
        Deque<Pair<Position, Integer>> stack = new ArrayDeque<>();
        stack.add(new Pair<>(start, vision));
        while (!stack.isEmpty()) {
            Pair<Position, Integer> pair = stack.removeFirst();
            if (pair.getRight() > 0) {
                Position positionA = directionA.applyTo(pair.getLeft());
                checkForObjects(positionA, pair.getRight(), canSee, stack);
                Position positionB = directionB.applyTo(pair.getLeft());
                checkForObjects(positionB, pair.getRight(), canSee, stack);
            }
        }
        return canSee;
    }

    private static void checkForObjects(Position position, int vision, Set<GameObject> canSee, Deque<Pair<Position, Integer>> stack) {
        // Check for the edge of the map.
        if (position == null) {
            return;
        }
        // Update the vision left and check for objects in this cell.
        int visionLeft = vision - 1;
        for (GameObject gameObject : GameWorld.getInstance().getGameObjectsAt(position)) {
            canSee.add(gameObject);
            visionLeft = updateVisionLeft(visionLeft, gameObject);
        }
        // If this position is already in the stack then remove it if this is a cheaper way to get to it.
        Iterator<Pair<Position, Integer>> iterator = stack.iterator();
        while (iterator.hasNext()) {
            Pair<Position, Integer> pair = iterator.next();
            if (pair.getLeft() == position) {
                if (pair.getRight() < visionLeft) {
                    iterator.remove();
                    break;
                }
            }
        }
        stack.addLast(new Pair<>(position, visionLeft));
    }

    /**
     * Update the vision left based on how much different objects obscure vision.
     *
     * @param visionLeft The initial amount of vision left.
     * @param gameObject The game object.
     * @return The new amount of vision left after the obscuring effect.
     */
    private static int updateVisionLeft(int visionLeft, GameObject gameObject) {
        ObjectType objectType = gameObject.getObjectType();
        if (objectType instanceof PlantType) {
            switch ((PlantType) objectType) {
                default:
                    visionLeft -= 1;
                    break;
            }
        } else if (objectType instanceof ItemType) {
            switch ((ItemType) objectType) {
                default:
                    visionLeft -= 0;
                    break;
            }
        } else if (objectType instanceof DinosaurType) {
            switch ((DinosaurType) objectType) {
                case DIPLODOCUS:
                    visionLeft -= 1;
                    break;
                default:
                    visionLeft -= 0;
                    break;
            }
        } else if (objectType instanceof BuildingType) {
            switch ((BuildingType) objectType) {
                default:
                    // Most buildings completely obscure the view.
                    visionLeft = 0;
                    break;
            }
        }
        return visionLeft;
    }
}
