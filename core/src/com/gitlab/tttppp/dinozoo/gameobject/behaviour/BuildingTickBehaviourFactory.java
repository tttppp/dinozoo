package com.gitlab.tttppp.dinozoo.gameobject.behaviour;

import com.gitlab.tttppp.dinozoo.gameobject.Building;

public class BuildingTickBehaviourFactory {
    public static BuildingTickBehaviourFactory INSTANCE = new BuildingTickBehaviourFactory();

    public TickBehaviour get(Building building) {
        switch (building.getBuildingType()) {
            case ENTRY_NS:
            case ENTRY_EW:
                return new EntryBehaviour(building.getPosition());
        }
        return null;
    }
}
