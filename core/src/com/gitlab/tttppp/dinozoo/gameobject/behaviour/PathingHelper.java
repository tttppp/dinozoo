package com.gitlab.tttppp.dinozoo.gameobject.behaviour;

import com.gitlab.tttppp.dinozoo.gameworld.Direction;
import com.gitlab.tttppp.dinozoo.gameworld.GameWorld;
import com.gitlab.tttppp.dinozoo.gameworld.GroundSubType;
import com.gitlab.tttppp.dinozoo.gameworld.GroundType;
import com.gitlab.tttppp.dinozoo.gameworld.Habitat;
import com.gitlab.tttppp.dinozoo.gameworld.Position;
import com.gitlab.tttppp.dinozoo.utils.Log;
import com.gitlab.tttppp.dinozoo.utils.Triple;

import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;

import static com.gitlab.tttppp.dinozoo.gameworld.Habitat.DOMESTIC;

public class PathingHelper {
    /** The default initial capacity of a priority queue. See java.util.PriorityQueue#DEFAULT_INITIAL_CAPACITY. */
    private static final int DEFAULT_PRIORITY_QUEUE_SIZE = 11;
    /** The relative values of the different ground types (try to stick to the path). */
    private static final Map<GroundType, Integer> FRICTIONS = findFrictions(DOMESTIC);
    /** The friction map when a guest just wants to get to the exit no matter what. */
    private static final Map<GroundType, Integer> UNIFORM_FRICTIONS = findUniformFrictions();

    /** Private constructor for helper class. */
    private PathingHelper() {
    }

    private static Map<GroundType, Integer> findFrictions(Habitat domestic) {
        Map<GroundType, Integer> frictions = new HashMap<>();
        for (Map.Entry<GroundType, Integer> entry : domestic.getGroundTypeBonus().entrySet()) {
            if (entry.getValue() >= 20) {
                frictions.put(entry.getKey(), 100000 / entry.getValue());
            }
        }
        return frictions;
    }

    /** Create a friction map with effort 1 on any terrain. */
    private static Map<GroundType, Integer> findUniformFrictions() {
        Map<GroundType, Integer> frictions = new HashMap<>();
        for (GroundType groundType : GroundType.values()) {
            frictions.put(groundType, 1);
        }
        return frictions;
    }

    /**
     * Try to find a route using the standard domestic preferences.
     */
    public static Direction searchForRouteTo(Position start, Position end, int effort) {
        return searchForRouteTo(start, end, effort, FRICTIONS);
    }

    /**
     * Try to find a route ignoring any preferences for particular land types.
     */
    public static Direction searchForRouteIgnoringGround(Position start, Position end, int effort) {
        return searchForRouteTo(start, end, effort, UNIFORM_FRICTIONS);
    }

    /**
     * Try to find a route using a custom set of preferences.
     */
    public static Direction searchForRouteTo(Position start, Position end, int effort, Map<GroundType, Integer> frictions) {
        // Create a priority queue with the lowest "closeness" first.
        Queue<Triple<Position, Integer, Integer>> queue = new PriorityQueue<>(DEFAULT_PRIORITY_QUEUE_SIZE, new Comparator<Triple<Position, Integer, Integer>>() {
            @Override
            public int compare(Triple<Position, Integer, Integer> t1, Triple<Position, Integer, Integer> t2) {
                return t1.getRight() - t2.getRight();
            }
        });
        queue.add(makeTupleForQueue(start, 0, end));
        Map<Position, Direction> cameFrom = new HashMap<>();
        Set<Position> examined = new HashSet<>();
        examined.add(start);
        while (effort > 0 && !queue.isEmpty()) {
            Triple<Position, Integer, Integer> triple = queue.remove();
            Position position = triple.getLeft();
            if (position.equals(end)) {
                return findOutput(end, cameFrom);
            }
            for (Direction direction : Direction.ADJACENCIES) {
                Position neighbour = direction.applyTo(position);
                if (neighbour != null && !examined.contains(neighbour)) {
                    GroundSubType groundSubType = GameWorld.getInstance().getGroundTypeAt(neighbour);
                    Integer friction = frictions.get(groundSubType.getGroundType());
                    if (friction != null) {
                        queue.add(makeTupleForQueue(neighbour, triple.getMiddle() + friction, end));
                        examined.add(neighbour);
                        cameFrom.put(neighbour, direction);
                    }
                }
            }

            effort--;
        }
        // Failed to find a suitable route with the effort supplied.
        return null;
    }

    /**
     * Create a tuple suitable for the priority queue.
     *
     * @param current The position being considered.
     * @param distance The distance travelled to this position.
     * @param end The target position.
     * @return A triple of the position, the distance travelled and a score.
     */
    private static Triple<Position, Integer, Integer> makeTupleForQueue(Position current, int distance, Position end) {
        return new Triple<>(current, distance, distance + manhattanDistance(current, end));
    }

    private static Direction findOutput(Position end, Map<Position, Direction> cameFrom) {
        String s = "End: " + end;
        Position position = end;
        Direction direction = Direction.DOWN;
        while (cameFrom.containsKey(position)) {
            direction = cameFrom.get(position);
            position = direction.opposite().applyTo(position);
            s = direction + ", " + s;
        }
        return direction;
    }

    private static int manhattanDistance(Position start, Position end) {
        return Math.abs(start.getX() - end.getX()) + Math.abs(start.getY() - end.getY());
    }
}
