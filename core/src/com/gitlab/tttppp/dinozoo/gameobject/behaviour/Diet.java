package com.gitlab.tttppp.dinozoo.gameobject.behaviour;

public enum Diet {
    CARNIVORE, OMNIVORE, HERBIVORE, INSECTIVORE, PESCATARIAN
}
