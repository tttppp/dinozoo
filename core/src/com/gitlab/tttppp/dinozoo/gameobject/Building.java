package com.gitlab.tttppp.dinozoo.gameobject;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.gitlab.tttppp.dinozoo.gameobject.behaviour.BuildingTickBehaviourFactory;
import com.gitlab.tttppp.dinozoo.gameobject.behaviour.TickBehaviour;
import com.gitlab.tttppp.dinozoo.gamerenderer.BuildingLoader;
import com.gitlab.tttppp.dinozoo.gameworld.BuildingType;
import com.gitlab.tttppp.dinozoo.gameworld.ObjectType;
import com.gitlab.tttppp.dinozoo.gameworld.Position;

public class Building implements GameObject {
    private BuildingType buildingType;
    private Position position;
    private transient TextureRegion textureRegion;
    private TickBehaviour tickBehaviour;

    public Building(BuildingType buildingType, Position position) {
        this.buildingType = buildingType;
        this.position = position;
        updateBehaviour();
    }

    /** No arg constructor for game serialization. */
    public Building() {
    }

    @Override
    public TextureRegion getTexture() {
        if (textureRegion == null) {
            textureRegion = BuildingLoader.INSTANCE.getTextureRegion(buildingType);
        }
        return textureRegion;
    }

    @Override
    public TextureRegion getForeground() {
        return null;
    }

    @Override
    public void tick(long tick) {
        // No-op for most buildings.
        if (tickBehaviour != null) {
            tickBehaviour.tick(tick);
        }
    }

    @Override
    public Position getPosition() {
        return position;
    }

    @Override
    public void setPosition(Position position) {
        this.position = position;
        updateBehaviour();
    }

    @Override
    public boolean isStackable() {
        return false;
    }

    @Override
    public ObjectType getObjectType() {
        return buildingType;
    }

    public BuildingType getBuildingType() {
        return buildingType;
    }

    public void setBuildingType(BuildingType buildingType) {
        this.buildingType = buildingType;
        updateBehaviour();
    }

    private void updateBehaviour() {
        if (buildingType != null && position != null) {
            tickBehaviour = BuildingTickBehaviourFactory.INSTANCE.get(this);
        }
    }
}
