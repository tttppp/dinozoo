package com.gitlab.tttppp.dinozoo.gameobject;

import com.gitlab.tttppp.dinozoo.gameworld.Direction;
import com.gitlab.tttppp.dinozoo.gameworld.GameWorld;
import com.gitlab.tttppp.dinozoo.gameworld.GroundSubType;
import com.gitlab.tttppp.dinozoo.gameworld.GroundType;
import com.gitlab.tttppp.dinozoo.gameworld.Position;
import com.gitlab.tttppp.dinozoo.utils.RandomUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import static com.gitlab.tttppp.dinozoo.gameworld.GameWorld.TICK_LENGTH;
import static com.gitlab.tttppp.dinozoo.gameworld.GroundSubType.WATER_DEPTHS;
import static com.gitlab.tttppp.dinozoo.utils.CollectionUtils.isEmpty;
import static java.util.Arrays.asList;

public class Land {
    private static final int MIN_TICKS_TO_CHANGE = (int) Math.round(5 / TICK_LENGTH);
    private static final int MAX_TICKS_TO_CHANGE = (int) Math.round(60 / TICK_LENGTH);

    private List<GroundSubType> futureGroundTypes;
    private GroundSubType groundType;
    private Position position;
    private int ticksToTransition;

    public Land(GroundSubType groundType, Position position, GroundSubType... futureGroundTypes) {
        this.groundType = groundType;
        this.position = position;
        this.ticksToTransition = makeRandomTicksToChange();
        // The collection needs to be converted to a normal ArrayList so that it can be deserialized correctly.
        this.futureGroundTypes = new ArrayList<>(asList(futureGroundTypes));
    }

    /** No arg constructor for game serialization. */
    public Land() {
    }

    public GroundSubType getGroundType() {
        return groundType;
    }

    public GroundSubType getEventualGroundType() {
        if (isEmpty(futureGroundTypes)) {
            return groundType;
        }
        return futureGroundTypes.get(futureGroundTypes.size() - 1);
    }

    public void setGroundType(GroundSubType groundType) {
        this.groundType = groundType;
    }

    /**
     * Carry out one tick.
     *
     * @return true if the land is now empty.
     */
    public boolean tick() {
        if (ticksToTransition > 0 && !isEmpty(futureGroundTypes)) {
            ticksToTransition--;
            if (ticksToTransition <= 0) {
                groundType = futureGroundTypes.get(0);
                if (futureGroundTypes.size() > 1) {
                    // Create a new ArrayList so that it can be (de)serialized correctly.
                    futureGroundTypes = new ArrayList<>(futureGroundTypes.subList(1, futureGroundTypes.size()));
                    ticksToTransition = makeRandomTicksToChange();
                }
            }
        }
        // Every so often we'll update the water depth.
        if (RandomUtils.flipCoin(1, 999)) {
            // Check if depth is correct.
            if (groundType.getGroundType() == GroundType.WATER) {
                // Initialise to max water depth.
                int potentialDepth = WATER_DEPTHS.size() - 1;
                for (Direction direction : Direction.ADJACENCIES) {
                    Position neighbourPosition = direction.applyTo(position);
                    if (neighbourPosition != null) {
                        Land neighbour = GameWorld.getInstance().getGround().get(neighbourPosition);
                        if (neighbour == null) {
                            potentialDepth = 0;
                            break;
                        }
                        GroundSubType neighbourGroundSubType = neighbour.getGroundType();
                        if (neighbourGroundSubType.getGroundType() != GroundType.WATER) {
                            potentialDepth = 0;
                            break;
                        }
                        potentialDepth = Math.min(neighbourGroundSubType.getDepth() + 1, potentialDepth);
                    }
                }
                groundType = WATER_DEPTHS.get(potentialDepth);
            }
        }
        return false;
    }

    private int makeRandomTicksToChange() {
        return MIN_TICKS_TO_CHANGE + RandomUtils.getRandomInt(MAX_TICKS_TO_CHANGE - MIN_TICKS_TO_CHANGE);
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }
}
