package com.gitlab.tttppp.dinozoo.gameobject;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.gitlab.tttppp.dinozoo.gameobject.behaviour.NeighbourhoodAppearanceBehaviour;
import com.gitlab.tttppp.dinozoo.gameobject.behaviour.TickBehaviour;
import com.gitlab.tttppp.dinozoo.gamerenderer.WallLoader;
import com.gitlab.tttppp.dinozoo.gameworld.Direction;
import com.gitlab.tttppp.dinozoo.gameworld.GameWorld;
import com.gitlab.tttppp.dinozoo.gameworld.ObjectType;
import com.gitlab.tttppp.dinozoo.gameworld.Position;
import com.gitlab.tttppp.dinozoo.gameworld.WallType;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Wall implements GameObject {
    private WallType wallType;
    private Position position;
    private transient TextureRegion textureRegion;
    private transient TickBehaviour tickBehaviour = new NeighbourhoodAppearanceBehaviour(this);

    public Wall(WallType wallType, Position position) {
        this.wallType = wallType;
        this.position = position;
        // Tick immediately to set the texture.
        tickBehaviour.tick(GameWorld.getInstance().getTick());
    }

    /** No arg constructor for game serialization. */
    public Wall() {
    }

    @Override
    public TextureRegion getTexture() {
        if (textureRegion == null) {
            textureRegion = WallLoader.INSTANCE.getTextureRegion(wallType);
        }
        return textureRegion;
    }

    @Override
    public TextureRegion getForeground() {
        return null;
    }

    @Override
    public void tick(long tick) {
        tickBehaviour.tick(tick);
    }

    @Override
    public Position getPosition() {
        return position;
    }

    @Override
    public void setPosition(Position position) {
        this.position = position;
    }

    @Override
    public boolean isStackable() {
        return false;
    }

    @Override
    public ObjectType getObjectType() {
        return wallType;
    }

    public WallType getWallType() {
        return wallType;
    }

    public void neighboursAre(Map<Direction, GameObject> neighbours) {
        if (wallType != null) {
            Set<Direction> solidDirections = new HashSet<>();
            for (Map.Entry<Direction, GameObject> entry : neighbours.entrySet()) {
                ObjectType objectType = entry.getValue().getObjectType();
                if (objectType instanceof WallType) {
                    solidDirections.add(entry.getKey());
                }
            }
            wallType = WallType.getWallType(wallType.getWallStyle(), solidDirections);
            textureRegion = WallLoader.INSTANCE.getTextureRegion(wallType);
        }
    }
}
