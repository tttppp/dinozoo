package com.gitlab.tttppp.dinozoo.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.gitlab.tttppp.dinozoo.display.DisplayElement;
import com.gitlab.tttppp.dinozoo.display.MainMenuButton;
import com.gitlab.tttppp.dinozoo.display.MenuButton;
import com.gitlab.tttppp.dinozoo.display.TextDisplay;
import com.gitlab.tttppp.dinozoo.gamerenderer.ButtonLoader;
import com.gitlab.tttppp.dinozoo.gamerenderer.FontLoader;
import com.gitlab.tttppp.dinozoo.gamerenderer.GameRenderer;
import com.gitlab.tttppp.dinozoo.gamerenderer.MenuRenderer;
import com.gitlab.tttppp.dinozoo.input.Action;
import com.gitlab.tttppp.dinozoo.input.GameMode;
import com.gitlab.tttppp.dinozoo.input.MenuInputHandler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.gitlab.tttppp.dinozoo.gamerenderer.GameRenderer.SCALE;

public class AboutScreen implements MenuScreen {
    private Game game;
    private MenuRenderer menuRenderer;
    private MenuInputHandler menuInputHandler;
    private List<MenuButton> buttons;
    private List<DisplayElement> displayElements = new ArrayList<DisplayElement>();

    public AboutScreen(Game game) {
        this.game = game;

        Color titleColor1 = new Color(0.3f, 0.9f, 0.5f, 1);
        Color titleColor2 = new Color(0.1f, 0.7f, 0.3f, 1);
        Color textColor = new Color(0.3f, 0.1f, 0.5f, 1);

        TextDisplay text1 = new TextDisplay(0, SCALE * 2, GameRenderer.VIEWPORT_WIDTH, 36,
                "Dino Zoo is an open source project. To report issues or contribute,   \nvisit https://gitlab.com/tttppp/dinozoo",
                FontLoader.latrQuiteSmall, titleColor1);
        displayElements.add(text1);
        TextDisplay text2 = new TextDisplay(0, SCALE * 4, GameRenderer.VIEWPORT_WIDTH, 36,
                "Dino Zoo has been written using LibGDX which is an Apache 2.0 project.\nFind out more at https://libgdx.badlogicgames.com",
                FontLoader.latrQuiteSmall, titleColor2);
        displayElements.add(text2);

        MenuButton backButton = new MainMenuButton(GameMode.MAIN_MENU, SCALE * 21 / 2, SCALE * 13, ButtonLoader.mediumButtonUp, ButtonLoader.mediumButtonDown, "Back",
                FontLoader.latrMedium, textColor, game);
        buttons = Arrays.asList(backButton);

        displayElements.addAll(buttons);
        menuRenderer = new MenuRenderer(displayElements);

        menuInputHandler = new MenuInputHandler(this, buttons);
        Gdx.input.setInputProcessor(menuInputHandler);
    }

    @Override
    public void handleCallback(Action action) {

    }

    @Override
    public void back() {

    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        menuRenderer.render();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
