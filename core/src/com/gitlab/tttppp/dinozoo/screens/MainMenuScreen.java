package com.gitlab.tttppp.dinozoo.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.gitlab.tttppp.dinozoo.display.DisplayElement;
import com.gitlab.tttppp.dinozoo.display.MainMenuButton;
import com.gitlab.tttppp.dinozoo.display.MenuButton;
import com.gitlab.tttppp.dinozoo.display.TextDisplay;
import com.gitlab.tttppp.dinozoo.gamerenderer.ButtonLoader;
import com.gitlab.tttppp.dinozoo.gamerenderer.FontLoader;
import com.gitlab.tttppp.dinozoo.gamerenderer.GameRenderer;
import com.gitlab.tttppp.dinozoo.gamerenderer.MenuRenderer;
import com.gitlab.tttppp.dinozoo.input.Action;
import com.gitlab.tttppp.dinozoo.input.GameMode;
import com.gitlab.tttppp.dinozoo.input.MenuInputHandler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.gitlab.tttppp.dinozoo.gamerenderer.GameRenderer.SCALE;

public class MainMenuScreen implements MenuScreen {
    private Game game;
    private MenuRenderer menuRenderer;
    private MenuInputHandler menuInputHandler;
    private List<MenuButton> buttons;
    private List<DisplayElement> displayElements = new ArrayList<DisplayElement>();

    public MainMenuScreen(Game game) {
        this.game = game;

        TextureRegion buttonUp = ButtonLoader.wideButtonUp;
        TextureRegion buttonDown = ButtonLoader.wideButtonDown;
        Color titleColor1 = new Color(0.3f, 0.9f, 0.5f, 1);
        Color titleColor2 = new Color(0.1f, 0.7f, 0.3f, 1);
        Color textColor = new Color(0.3f, 0.1f, 0.5f, 1);

        TextDisplay dinoText = new TextDisplay(0, SCALE * 2, GameRenderer.VIEWPORT_WIDTH, 36, "Dinosaur", FontLoader.latrBig, titleColor1);
        displayElements.add(dinoText);
        TextDisplay zooText = new TextDisplay(0, SCALE * 6, GameRenderer.VIEWPORT_WIDTH, 36, "Zoo", FontLoader.latrBig, titleColor2);
        displayElements.add(zooText);

        MenuButton playButton = new MainMenuButton(GameMode.PLAY, SCALE * 7, SCALE * 10, buttonUp, buttonDown, "Play",
                FontLoader.latr, textColor, game);
        MenuButton loadButton1 = new MainMenuButton(GameMode.LOAD_1, SCALE * 11 / 2, SCALE * 13, ButtonLoader.mediumButtonUp, ButtonLoader.mediumButtonDown, "Load 1",
                FontLoader.latrMedium, textColor, game);
        MenuButton loadButton2 = new MainMenuButton(GameMode.LOAD_2, SCALE * 21 / 2, SCALE * 13, ButtonLoader.mediumButtonUp, ButtonLoader.mediumButtonDown, "Load 2",
                FontLoader.latrMedium, textColor, game);
        MenuButton loadButton3 = new MainMenuButton(GameMode.LOAD_3, SCALE * 31 / 2, SCALE * 13, ButtonLoader.mediumButtonUp, ButtonLoader.mediumButtonDown, "Load 3",
                FontLoader.latrMedium, textColor, game);

        MenuButton aboutButton = new MainMenuButton(GameMode.ABOUT, SCALE * 23, SCALE * 17, ButtonLoader.tinyButtonUp, ButtonLoader.tinyButtonDown, "?",
                FontLoader.latrMedium, textColor, game);
        buttons = Arrays.asList(playButton, loadButton1, loadButton2, loadButton3, aboutButton);

        displayElements.addAll(buttons);
        menuRenderer = new MenuRenderer(displayElements);

        menuInputHandler = new MenuInputHandler(this, buttons);
        Gdx.input.setInputProcessor(menuInputHandler);
    }

    @Override
    public void render(float delta) {
        menuRenderer.render();
    }

    @Override
    public void show() {
        // TODO Auto-generated method stub

    }

    /**
     * TODO This doesn't work for some reason.
     */
    @Override
    public void resize(int width, int height) {
        menuInputHandler.resize();
        menuRenderer.resize();
    }

    @Override
    public void pause() {
        // TODO Auto-generated method stub

    }

    @Override
    public void resume() {
        // TODO Auto-generated method stub

    }

    @Override
    public void hide() {
        // TODO Auto-generated method stub

    }

    @Override
    public void dispose() {
        // TODO Auto-generated method stub

    }

    @Override
    public void handleCallback(Action action) {
    }

    @Override
    public void back() {
        Gdx.app.exit();
    }
}

