package com.gitlab.tttppp.dinozoo.screens;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.gitlab.tttppp.dinozoo.display.AbstractMenuButton;
import com.gitlab.tttppp.dinozoo.display.DisplayElement;
import com.gitlab.tttppp.dinozoo.display.GraphicDisplay;
import com.gitlab.tttppp.dinozoo.display.MenuButton;
import com.gitlab.tttppp.dinozoo.display.MenuGraphicButton;
import com.gitlab.tttppp.dinozoo.display.TextButton;
import com.gitlab.tttppp.dinozoo.display.TextDisplay;
import com.gitlab.tttppp.dinozoo.gamerenderer.BuildingLoader;
import com.gitlab.tttppp.dinozoo.gamerenderer.DinosaurLoader;
import com.gitlab.tttppp.dinozoo.gamerenderer.FontLoader;
import com.gitlab.tttppp.dinozoo.gamerenderer.GroundLoader;
import com.gitlab.tttppp.dinozoo.gamerenderer.IconLoader;
import com.gitlab.tttppp.dinozoo.gamerenderer.ItemLoader;
import com.gitlab.tttppp.dinozoo.gamerenderer.PlantLoader;
import com.gitlab.tttppp.dinozoo.gamerenderer.StaffLoader;
import com.gitlab.tttppp.dinozoo.gamerenderer.TextureLoader;
import com.gitlab.tttppp.dinozoo.gamerenderer.WallLoader;
import com.gitlab.tttppp.dinozoo.gameworld.BuildingType;
import com.gitlab.tttppp.dinozoo.gameworld.DinosaurType;
import com.gitlab.tttppp.dinozoo.gameworld.GameWorld;
import com.gitlab.tttppp.dinozoo.gameworld.GroundSubType;
import com.gitlab.tttppp.dinozoo.gameworld.IconType;
import com.gitlab.tttppp.dinozoo.gameworld.ObjectType;
import com.gitlab.tttppp.dinozoo.gameworld.PlantType;
import com.gitlab.tttppp.dinozoo.gameworld.StaffType;
import com.gitlab.tttppp.dinozoo.gameworld.WallType;
import com.gitlab.tttppp.dinozoo.input.Action;
import com.gitlab.tttppp.dinozoo.input.MapInputMode;
import com.gitlab.tttppp.dinozoo.input.PickSubMenuAction;
import com.gitlab.tttppp.dinozoo.input.PlayPauseAction;
import com.gitlab.tttppp.dinozoo.input.SaveAction;
import com.gitlab.tttppp.dinozoo.input.SetMapClickAction;
import com.gitlab.tttppp.dinozoo.input.UpdateResearchAction;
import com.gitlab.tttppp.dinozoo.input.UpdateTicketPriceAction;
import com.google.common.collect.Lists;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import static com.gitlab.tttppp.dinozoo.gamerenderer.GameRenderer.BORDER;
import static com.gitlab.tttppp.dinozoo.gamerenderer.GameRenderer.SCALE;
import static com.gitlab.tttppp.dinozoo.gamerenderer.GameRenderer.VIEWPORT_WIDTH;
import static com.gitlab.tttppp.dinozoo.gameworld.BuildingType.ENTRY_NS;
import static com.gitlab.tttppp.dinozoo.gameworld.IconType.ECSTATIC_FACE;
import static com.gitlab.tttppp.dinozoo.gameworld.IconType.ENTRY_MINUS;
import static com.gitlab.tttppp.dinozoo.gameworld.IconType.ENTRY_PLUS;
import static com.gitlab.tttppp.dinozoo.gameworld.IconType.HAPPY_FACE;
import static com.gitlab.tttppp.dinozoo.gameworld.IconType.NEUTRAL_FACE;
import static com.gitlab.tttppp.dinozoo.gameworld.IconType.PLAY_PAUSE;
import static com.gitlab.tttppp.dinozoo.gameworld.IconType.POPULARITY;
import static com.gitlab.tttppp.dinozoo.gameworld.IconType.RESEARCH;
import static com.gitlab.tttppp.dinozoo.gameworld.IconType.RESEARCH_MINUS;
import static com.gitlab.tttppp.dinozoo.gameworld.IconType.RESEARCH_PLUS;
import static com.gitlab.tttppp.dinozoo.gameworld.IconType.SAD_FACE;
import static com.gitlab.tttppp.dinozoo.gameworld.IconType.SATISFIED_FACE;
import static com.gitlab.tttppp.dinozoo.gameworld.IconType.SAVE_1;
import static com.gitlab.tttppp.dinozoo.gameworld.IconType.SAVE_2;
import static com.gitlab.tttppp.dinozoo.gameworld.IconType.SAVE_3;
import static com.gitlab.tttppp.dinozoo.gameworld.ItemType.BULLDOZER;
import static com.gitlab.tttppp.dinozoo.gameworld.ItemType.NEST;
import static com.gitlab.tttppp.dinozoo.input.MapInputMode.CLICK_PLACEMENT_MODE;
import static com.gitlab.tttppp.dinozoo.input.MapInputMode.DRAG_PLACEMENT_MODE;
import static com.gitlab.tttppp.dinozoo.screens.SubMenuType.BUILDING;
import static com.gitlab.tttppp.dinozoo.screens.SubMenuType.DINOSAUR;
import static com.gitlab.tttppp.dinozoo.screens.SubMenuType.FINANCE;
import static com.gitlab.tttppp.dinozoo.screens.SubMenuType.GROUND;
import static com.gitlab.tttppp.dinozoo.screens.SubMenuType.ITEM;
import static com.gitlab.tttppp.dinozoo.screens.SubMenuType.PLANT;
import static com.gitlab.tttppp.dinozoo.screens.SubMenuType.PREFERENCES;
import static com.gitlab.tttppp.dinozoo.screens.SubMenuType.STAFF;
import static com.gitlab.tttppp.dinozoo.screens.SubMenuType.WALL;
import static java.util.Arrays.asList;

public class SubMenu {
    public static final int LEFT = 0;
    public static final int TOP = SCALE * 16;
    public static final int WIDTH = VIEWPORT_WIDTH;
    public static final int HEIGHT = SCALE + BORDER * 2;
    private static final Color SUB_MENU_BACKGROUND = new Color(0x002700ff);
    /** The maximum number of buttons that will fit in a submenu leaving room for paging buttons. */
    private static final int MAX_SUBMENU_BUTTONS = 19;

    private GameWorld gameWorld;
    private Map<SubMenuType, List<Map<ObjectType, AbstractMenuButton>>> buttons = new HashMap<>();
    private Map<SubMenuType, List<Map<ObjectType, DisplayElement>>> prices = new HashMap<>();
    private Map<SubMenuType, List<List<DisplayElement>>> otherDisplayElements = new HashMap<>();
    private SubMenuType selected = null;
    private int subMenuPage = 0;
    private TextDisplay ticketPrice;
    private TextDisplay researchAmount;
    private List<GraphicDisplay> popularityIcons = new ArrayList<>();

    public SubMenu(GameWorld gameWorld) {
        this.gameWorld = gameWorld;

        for (SubMenuType subMenuType : SubMenuType.values()) {
            createSubMenu(subMenuType);
        }
    }

    /**
     * Create (or recreate) the specified submenu.
     *
     * @param subMenuType The submenu to create.
     */
    public void createSubMenu(SubMenuType subMenuType) {
        switch (subMenuType) {
            case ITEM:
                createSubMenu(ITEM, ItemLoader.INSTANCE, CLICK_PLACEMENT_MODE, BULLDOZER, NEST);
                break;
            case GROUND:
                createSubMenu(GROUND, GroundLoader.INSTANCE, DRAG_PLACEMENT_MODE, GroundSubType.allPurchasable());
                break;
            case WALL:
                createSubMenu(WALL, WallLoader.INSTANCE, DRAG_PLACEMENT_MODE, WallType.allPurchasable());
                break;
            case PLANT:
                createSubMenu(PLANT, PlantLoader.INSTANCE, CLICK_PLACEMENT_MODE, PlantType.allPurchasable());
                break;
            case DINOSAUR:
                createSubMenu(DINOSAUR, DinosaurLoader.INSTANCE, CLICK_PLACEMENT_MODE, DinosaurType.values());
                break;
            case BUILDING:
                createSubMenu(BUILDING, BuildingLoader.INSTANCE, CLICK_PLACEMENT_MODE, BuildingType.allPurchasable());
                break;
            case STAFF:
                createSubMenu(STAFF, StaffLoader.INSTANCE, CLICK_PLACEMENT_MODE, StaffType.values());
                break;
            case FINANCE:
                createFinanceSubMenu();
                break;
            case PREFERENCES:
                createSettingsSubMenu();
                break;
        }
    }

    private <T extends ObjectType> void createSubMenu(SubMenuType subMenuType, TextureLoader<T> textureLoader, MapInputMode mapInputMode, T... objectTypes) {
        List<Map<ObjectType, AbstractMenuButton>> buttonSets = new ArrayList<>();
        List<Map<ObjectType, DisplayElement>> priceSets = new ArrayList<>();
        ListIterator<T> iterator = Lists.newArrayList(objectTypes).listIterator();
        int pages = 1 + (objectTypes.length - 1) / MAX_SUBMENU_BUTTONS;
        while (iterator.hasNext()) {
            Map<ObjectType, AbstractMenuButton> buttonSet = new HashMap<>();
            Map<ObjectType, DisplayElement> priceSet = new HashMap<>();
            while (iterator.hasNext() && buttonSet.size() < MAX_SUBMENU_BUTTONS) {
                int nextIndex = iterator.nextIndex();
                T next = iterator.next();
                // Icon and button
                TextureRegion textureRegion = textureLoader.getIconTextureRegion(next);
                SetMapClickAction action = new SetMapClickAction(this, subMenuType, mapInputMode, next);
                int subMenuItemX = getSubMenuItemX((nextIndex % MAX_SUBMENU_BUTTONS) + 1);
                MenuGraphicButton button = new MenuGraphicButton(action, subMenuItemX, TOP + BORDER, textureRegion);
                button.setActive(false);
                buttonSet.put(next, button);
                // Price
                TextDisplay price = new TextDisplay(subMenuItemX, TOP + BORDER + SCALE / 3, SCALE, SCALE, String.valueOf(next.getPrice()), FontLoader.latrSmall, Color.WHITE);
                priceSet.put(next, price);
            }
            if (pages > 1) {
                // Add paging buttons.
                TextureRegion leftTexture = IconLoader.INSTANCE.getIconTextureRegion(IconType.SCROLL_LEFT);
                Action leftAction = new PickSubMenuAction(this, subMenuType, (buttonSets.size() + pages - 1) % pages);
                MenuGraphicButton leftButton = new MenuGraphicButton(leftAction, getSubMenuItemX(0), TOP + BORDER, leftTexture);
                leftButton.setActive(false);
                buttonSet.put(IconType.SCROLL_LEFT, leftButton);
                TextureRegion rightTexture = IconLoader.INSTANCE.getIconTextureRegion(IconType.SCROLL_RIGHT);
                Action rightAction = new PickSubMenuAction(this, subMenuType, (buttonSets.size() + 1) % pages);
                MenuGraphicButton rightButton = new MenuGraphicButton(rightAction, getSubMenuItemX(MAX_SUBMENU_BUTTONS + 1), TOP + BORDER, rightTexture);
                rightButton.setActive(false);
                buttonSet.put(IconType.SCROLL_RIGHT, rightButton);
            }
            buttonSets.add(buttonSet);
            priceSets.add(priceSet);
        }
        buttons.put(subMenuType, buttonSets);
        prices.put(subMenuType, priceSets);
    }

    private void createFinanceSubMenu() {
        Map<ObjectType, AbstractMenuButton> buttonSet = new HashMap<>();
        List<DisplayElement> displayElements = new ArrayList<>();

        // Ticket price.
        GraphicDisplay entrySymbol = new GraphicDisplay(getSubMenuItemX(0), TOP + BORDER, BuildingLoader.INSTANCE.getIconTextureRegion(ENTRY_NS));
        displayElements.add(entrySymbol);
        Action minusAction = new UpdateTicketPriceAction(this, -1);
        TextButton minusButton = new TextButton(getSubMenuItemX(1), TOP + BORDER, SCALE, SCALE, "-", FontLoader.latrMedium, Color.WHITE, minusAction);
        buttonSet.put(ENTRY_MINUS, minusButton);
        ticketPrice = new TextDisplay(getSubMenuItemX(2), TOP, SCALE * 2, SCALE, "", FontLoader.latrMedium, Color.WHITE);
        displayElements.add(ticketPrice);
        Action addAction = new UpdateTicketPriceAction(this, 1);
        TextButton addButton = new TextButton(getSubMenuItemX(4), TOP + BORDER, SCALE, SCALE, "+", FontLoader.latrMedium, Color.WHITE, addAction);
        buttonSet.put(ENTRY_PLUS, addButton);

        // Research.
        GraphicDisplay researchSymbol = new GraphicDisplay(getSubMenuItemX(5), TOP + BORDER, IconLoader.INSTANCE.getIconTextureRegion(RESEARCH));
        displayElements.add(researchSymbol);
        Action minusActionResearch = new UpdateResearchAction(this, -1);
        TextButton minusButtonResearch = new TextButton(getSubMenuItemX(6), TOP + BORDER, SCALE, SCALE, "-", FontLoader.latrMedium, Color.WHITE, minusActionResearch);
        buttonSet.put(RESEARCH_MINUS, minusButtonResearch);
        researchAmount = new TextDisplay(getSubMenuItemX(7), TOP, SCALE * 2, SCALE, "", FontLoader.latrMedium, Color.WHITE);
        displayElements.add(researchAmount);
        Action addActionResearch = new UpdateResearchAction(this, 1);
        TextButton addButtonResearch = new TextButton(getSubMenuItemX(9), TOP + BORDER, SCALE, SCALE, "+", FontLoader.latrMedium, Color.WHITE, addActionResearch);
        buttonSet.put(RESEARCH_PLUS, addButtonResearch);

        // Popularity
        GraphicDisplay popularitySymbol = new GraphicDisplay(getSubMenuItemX(10), TOP + BORDER, IconLoader.INSTANCE.getIconTextureRegion(POPULARITY));
        displayElements.add(popularitySymbol);
        popularityIcons.add(new GraphicDisplay(getSubMenuItemX(11), TOP + BORDER, IconLoader.INSTANCE.getIconTextureRegion(SAD_FACE)));
        popularityIcons.add(new GraphicDisplay(getSubMenuItemX(11), TOP + BORDER, IconLoader.INSTANCE.getIconTextureRegion(NEUTRAL_FACE)));
        popularityIcons.add(new GraphicDisplay(getSubMenuItemX(11), TOP + BORDER, IconLoader.INSTANCE.getIconTextureRegion(SATISFIED_FACE)));
        popularityIcons.add(new GraphicDisplay(getSubMenuItemX(11), TOP + BORDER, IconLoader.INSTANCE.getIconTextureRegion(HAPPY_FACE)));
        popularityIcons.add(new GraphicDisplay(getSubMenuItemX(11), TOP + BORDER, IconLoader.INSTANCE.getIconTextureRegion(ECSTATIC_FACE)));

        buttons.put(FINANCE, asList(buttonSet));
        otherDisplayElements.put(FINANCE, asList(displayElements));
    }

    private void createSettingsSubMenu() {
        Map<ObjectType, AbstractMenuButton> buttonSet = new HashMap<>();
        List<DisplayElement> displayElements = new ArrayList<>();

        // Save game buttons.
        TextureRegion textureRegion = IconLoader.INSTANCE.getIconTextureRegion(SAVE_1);
        Action saveAction1 = new SaveAction(this, "1");
        MenuGraphicButton saveButton1 = new MenuGraphicButton(saveAction1, getSubMenuItemX(0), TOP + BORDER, textureRegion);
        buttonSet.put(SAVE_1, saveButton1);
        TextDisplay text1 = new TextDisplay(getSubMenuItemX(0), TOP + BORDER + SCALE / 4, SCALE, SCALE, "1", FontLoader.latrQuiteSmall, Color.WHITE);
        displayElements.add(text1);

        Action saveAction2 = new SaveAction(this, "2");
        MenuGraphicButton saveButton2 = new MenuGraphicButton(saveAction2, getSubMenuItemX(1), TOP + BORDER, textureRegion);
        buttonSet.put(SAVE_2, saveButton2);
        TextDisplay text2 = new TextDisplay(getSubMenuItemX(1), TOP + BORDER + SCALE / 4, SCALE, SCALE, "2", FontLoader.latrQuiteSmall, Color.WHITE);
        displayElements.add(text2);

        Action saveAction3 = new SaveAction(this, "3");
        MenuGraphicButton saveButton3 = new MenuGraphicButton(saveAction3, getSubMenuItemX(2), TOP + BORDER, textureRegion);
        buttonSet.put(SAVE_3, saveButton3);
        TextDisplay text3 = new TextDisplay(getSubMenuItemX(2), TOP + BORDER + SCALE / 4, SCALE, SCALE, "3", FontLoader.latrQuiteSmall, Color.WHITE);
        displayElements.add(text3);

        TextureRegion playPauseTexture = IconLoader.INSTANCE.getIconTextureRegion(PLAY_PAUSE);
        Action playPauseAction = new PlayPauseAction(this);
        MenuGraphicButton playPauseButton = new MenuGraphicButton(playPauseAction, getSubMenuItemX(5), TOP + BORDER, playPauseTexture);
        buttonSet.put(PLAY_PAUSE, playPauseButton);
        // This needs to default to "Pause" if loaded when not on the game screen.
        String playPauseLabel = "Pause";
        if (gameWorld.isPaused()) {
            playPauseLabel = "Play";
        }
        TextDisplay playPauseText = new TextDisplay(getSubMenuItemX(5), TOP + BORDER + SCALE / 4, SCALE, SCALE, playPauseLabel, FontLoader.latrQuiteSmall, Color.WHITE);
        displayElements.add(playPauseText);

        buttons.put(PREFERENCES, asList(buttonSet));
        otherDisplayElements.put(PREFERENCES, asList(displayElements));
    }

    /** Get the x coordinate of a menu item. */
    private int getSubMenuItemX(int i) {
        return LEFT + BORDER + (SCALE + BORDER) * i;
    }

    public void drawButtons(SpriteBatch spriteBatch) {
        if (selected != null) {
            for (Map.Entry<ObjectType, AbstractMenuButton> entry : buttons.get(selected).get(subMenuPage).entrySet()) {
                if (gameWorld.getResearch().isResearched(entry.getKey())) {
                    entry.getValue().draw(spriteBatch);
                    List<Map<ObjectType, DisplayElement>> priceSets = prices.get(selected);
                    if (priceSets != null && priceSets.get(subMenuPage) != null) {
                        Map<ObjectType, DisplayElement> priceSet = priceSets.get(subMenuPage);
                        DisplayElement priceLabel = priceSet.get(entry.getKey());
                        if (priceLabel != null) {
                            priceLabel.draw(spriteBatch);
                        }
                    }
                    // New research
                    if (gameWorld.getResearch().isNewResearch(entry.getKey())) {
                        TextureRegion starTexture = IconLoader.INSTANCE.getIconTextureRegion(IconType.NEW_RESEARCH);
                        DisplayElement star = new GraphicDisplay(entry.getValue().getX(), entry.getValue().getY(), starTexture);
                        star.draw(spriteBatch);
                    }
                }
            }
            if (selected == FINANCE) {
                ticketPrice.setText("£" + gameWorld.getZooStats().getTicketPrice());
                researchAmount.setText("£" + gameWorld.getResearch().getResearchAmount());
                // Get an index between 0 and 4.
                int popularityIndex = (int) (gameWorld.getZooStats().getPopularity() * 5);
                if (popularityIndex >= popularityIcons.size()) {
                    popularityIndex = popularityIcons.size() - 1;
                }
                popularityIcons.get(popularityIndex).draw(spriteBatch);
            }
            List<List<DisplayElement>> displayElementSets = otherDisplayElements.get(selected);
            if (displayElementSets != null && displayElementSets.get(subMenuPage) != null) {
                List<DisplayElement> displayElements = displayElementSets.get(subMenuPage);
                for (DisplayElement displayElement : displayElements) {
                    displayElement.draw(spriteBatch);
                }
            }
        }
    }

    public void drawBackground(ShapeRenderer shapeRenderer) {
        shapeRenderer.setColor(SUB_MENU_BACKGROUND);
        shapeRenderer.rect(LEFT, TOP, WIDTH, HEIGHT);
    }

    public SubMenuType getSelected() {
        return selected;
    }

    public void setSelected(SubMenuType selected, int subMenuPage) {
        // Don't allow returning to initial state of no submenu displayed.
        if (selected == null)
        {
            return;
        }
        // Deactivate old submenu.
        if (this.selected != null) {
            for (MenuButton menuButton : buttons.get(this.selected).get(this.subMenuPage).values()) {
                menuButton.setActive(false);
            }
        }
        this.selected = selected;
        this.subMenuPage = subMenuPage;
        // Activate the new submenu.
        for (Map.Entry<ObjectType, AbstractMenuButton> entry : buttons.get(selected).get(subMenuPage).entrySet()) {
            if (gameWorld.getResearch().isResearched(entry.getKey())) {
                entry.getValue().setActive(true);
            }
        }
    }

    public void refresh() {
        setSelected(selected, subMenuPage);
    }

    public Collection<? extends MenuButton> getAllButtons() {
        List<MenuButton> allButtons = new ArrayList<>();
        for (List<Map<ObjectType, AbstractMenuButton>> subMenu : buttons.values()) {
            for (Map<ObjectType, AbstractMenuButton> subMenuPage : subMenu) {
                allButtons.addAll(subMenuPage.values());
            }
        }
        return allButtons;
    }

    public boolean containsNewResearch(SubMenuType subMenuType) {
        List<Map<ObjectType, AbstractMenuButton>> buttonSets = buttons.get(subMenuType);
        for (Map<ObjectType, AbstractMenuButton> buttonSet : buttonSets) {
            for (ObjectType objectType : buttonSet.keySet()) {
                if (gameWorld.getResearch().isNewResearch(objectType)) {
                    return true;
                }
            }
        }
        return false;
    }
}
