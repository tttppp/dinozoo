package com.gitlab.tttppp.dinozoo.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.gitlab.tttppp.dinozoo.display.MenuButton;
import com.gitlab.tttppp.dinozoo.display.MenuGraphicButton;
import com.gitlab.tttppp.dinozoo.gamerenderer.ButtonLoader;
import com.gitlab.tttppp.dinozoo.gamerenderer.GameRenderer;
import com.gitlab.tttppp.dinozoo.gameworld.GameWorld;
import com.gitlab.tttppp.dinozoo.input.GameInputHandler;
import com.gitlab.tttppp.dinozoo.input.ScrollAction;
import com.gitlab.tttppp.dinozoo.save.CrashHandler;
import com.gitlab.tttppp.dinozoo.utils.Log;
import com.google.common.collect.Lists;

import java.util.List;

import static com.gitlab.tttppp.dinozoo.gamerenderer.GameRenderer.SCALE;

public class GameScreen implements Screen {
    public static final int SCROLL_AMOUNT = 5;
    private GameWorld gameWorld;
    private GameRenderer gameRenderer;
    private boolean paused = false;

    public GameScreen(Game game) {
        gameWorld = GameWorld.getInstance();
        gameWorld.setGame(game);
        SubMenu subMenu = new SubMenu(gameWorld);
        BottomMenu bottomMenu = new BottomMenu(gameWorld, subMenu);
        List<MenuButton> buttons = createScrollButtons();
        buttons.addAll(bottomMenu.getAllButtons());
        buttons.addAll(subMenu.getAllButtons());
        gameRenderer = new GameRenderer(gameWorld, buttons, bottomMenu, subMenu);

        GameInputHandler inputHandler = GameInputHandler.INSTANCE;
        inputHandler.init(game, buttons, subMenu);
        Gdx.input.setInputProcessor(inputHandler);
    }

    /**
     * Create the scroll buttons for the viewport.
     */
    private List<MenuButton> createScrollButtons() {
        ScrollAction upAction = new ScrollAction(gameWorld, 0, -SCROLL_AMOUNT);
        MenuButton upButton = new MenuGraphicButton(upAction, SCALE * 23 / 2, 0, ButtonLoader.scrollUpUp, ButtonLoader.scrollUpDown);
        ScrollAction downAction = new ScrollAction(gameWorld, 0, SCROLL_AMOUNT);
        MenuButton downButton = new MenuGraphicButton(downAction, SCALE * 23 / 2, SCALE * 15, ButtonLoader.scrollDownUp, ButtonLoader.scrollDownDown);
        ScrollAction leftAction = new ScrollAction(gameWorld, -SCROLL_AMOUNT, 0);
        MenuButton leftButton = new MenuGraphicButton(leftAction, 0, SCALE * 14 / 2, ButtonLoader.scrollLeftUp, ButtonLoader.scrollLeftDown);
        ScrollAction rightAction = new ScrollAction(gameWorld, SCROLL_AMOUNT, 0);
        MenuButton rightButton = new MenuGraphicButton(rightAction, SCALE * 24, SCALE * 14 / 2, ButtonLoader.scrollRightUp, ButtonLoader.scrollRightDown);
        ScrollAction ulAction = new ScrollAction(gameWorld, -SCROLL_AMOUNT, -SCROLL_AMOUNT);
        MenuButton ulButton = new MenuGraphicButton(ulAction, 0, 0, ButtonLoader.scrollULUp, ButtonLoader.scrollULDown);
        ScrollAction urAction = new ScrollAction(gameWorld, SCROLL_AMOUNT, -SCROLL_AMOUNT);
        MenuButton urButton = new MenuGraphicButton(urAction, SCALE * 24, 0, ButtonLoader.scrollURUp, ButtonLoader.scrollURDown);
        ScrollAction dlAction = new ScrollAction(gameWorld, -SCROLL_AMOUNT, SCROLL_AMOUNT);
        MenuButton dlButton = new MenuGraphicButton(dlAction, 0, SCALE * 15, ButtonLoader.scrollDLUp, ButtonLoader.scrollDLDown);
        ScrollAction drAction = new ScrollAction(gameWorld, SCROLL_AMOUNT, SCROLL_AMOUNT);
        MenuButton drButton = new MenuGraphicButton(drAction, SCALE * 24, SCALE * 15, ButtonLoader.scrollDRUp, ButtonLoader.scrollDRDown);
        return Lists.newArrayList(upButton, downButton, leftButton, rightButton, ulButton, urButton, dlButton, drButton);
    }

    @Override
    public void render(float delta) {
        try {
            if (!paused) {
                gameWorld.update(delta);
            }
            gameRenderer.render();
        } catch (RuntimeException e) {
            CrashHandler crashHandler = new CrashHandler();
            throw crashHandler.handleCrashAndReturnException(e);
        }
    }

    @Override
    public void show() {
        Log.log("Showing");
    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void pause() {
        Log.log("Pausing");
        paused = true;
        GameInputHandler.INSTANCE.setMapPlacementObjectType(null);
    }

    @Override
    public void resume() {
        Log.log("Resuming");
        paused = false;
    }

    @Override
    public void hide() {
        Log.log("Hiding");
    }

    @Override
    public void dispose() {
        // TODO Auto-generated method stub

    }

    public GameRenderer getGameRenderer() {
        return gameRenderer;
    }

    public boolean isPaused() {
        return paused;
    }
}
