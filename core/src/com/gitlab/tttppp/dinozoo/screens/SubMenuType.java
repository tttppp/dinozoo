package com.gitlab.tttppp.dinozoo.screens;

public enum SubMenuType {
    GROUND, WALL, PLANT, ITEM, DINOSAUR, STAFF, BUILDING, PREFERENCES, FINANCE
}
