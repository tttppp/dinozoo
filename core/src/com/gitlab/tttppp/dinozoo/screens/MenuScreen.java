package com.gitlab.tttppp.dinozoo.screens;

import com.badlogic.gdx.Screen;
import com.gitlab.tttppp.dinozoo.input.Action;

public interface MenuScreen extends Screen {
	void handleCallback(Action action);

	void back();
}
