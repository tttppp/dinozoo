package com.gitlab.tttppp.dinozoo.screens;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.gitlab.tttppp.dinozoo.display.DisplayElement;
import com.gitlab.tttppp.dinozoo.display.GraphicDisplay;
import com.gitlab.tttppp.dinozoo.display.MenuButton;
import com.gitlab.tttppp.dinozoo.display.MenuGraphicButton;
import com.gitlab.tttppp.dinozoo.display.TextDisplay;
import com.gitlab.tttppp.dinozoo.gamerenderer.DinosaurLoader;
import com.gitlab.tttppp.dinozoo.gamerenderer.FontLoader;
import com.gitlab.tttppp.dinozoo.gamerenderer.GroundLoader;
import com.gitlab.tttppp.dinozoo.gamerenderer.IconLoader;
import com.gitlab.tttppp.dinozoo.gamerenderer.ItemLoader;
import com.gitlab.tttppp.dinozoo.gamerenderer.PlantLoader;
import com.gitlab.tttppp.dinozoo.gamerenderer.WallLoader;
import com.gitlab.tttppp.dinozoo.gameworld.GameWorld;
import com.gitlab.tttppp.dinozoo.gameworld.IconType;
import com.gitlab.tttppp.dinozoo.input.PickSubMenuAction;
import com.gitlab.tttppp.dinozoo.utils.Pair;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static com.gitlab.tttppp.dinozoo.gamerenderer.GameRenderer.BORDER;
import static com.gitlab.tttppp.dinozoo.gamerenderer.GameRenderer.SCALE;
import static com.gitlab.tttppp.dinozoo.gamerenderer.GameRenderer.VIEWPORT_WIDTH;
import static com.gitlab.tttppp.dinozoo.gameworld.DinosaurType.PROTOCERATOPS;
import static com.gitlab.tttppp.dinozoo.gameworld.GroundSubType.PATH;
import static com.gitlab.tttppp.dinozoo.gameworld.IconType.COIN;
import static com.gitlab.tttppp.dinozoo.gameworld.IconType.SETTINGS;
import static com.gitlab.tttppp.dinozoo.gameworld.ItemType.NEST;
import static com.gitlab.tttppp.dinozoo.gameworld.PlantType.TREE;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.BRICK_EW;
import static com.gitlab.tttppp.dinozoo.screens.SubMenuType.DINOSAUR;
import static com.gitlab.tttppp.dinozoo.screens.SubMenuType.FINANCE;
import static com.gitlab.tttppp.dinozoo.screens.SubMenuType.GROUND;
import static com.gitlab.tttppp.dinozoo.screens.SubMenuType.ITEM;
import static com.gitlab.tttppp.dinozoo.screens.SubMenuType.PLANT;
import static com.gitlab.tttppp.dinozoo.screens.SubMenuType.PREFERENCES;
import static com.gitlab.tttppp.dinozoo.screens.SubMenuType.WALL;

public class BottomMenu {
    public static final int LEFT = 0;
    public static final int TOP = SCALE * 16 + SCALE + BORDER * 2;
    public static final int WIDTH = VIEWPORT_WIDTH;
    public static final int HEIGHT = SCALE + BORDER * 2;
    private static final Color BOTTOM_MENU_BACKGROUND = new Color(0x004300ff);

    private GameWorld gameWorld;
    private SubMenu subMenu;
    private List<Pair<SubMenuType, MenuGraphicButton>> buttons = new ArrayList<>();
    private TextDisplay money;

    public BottomMenu(GameWorld gameWorld, SubMenu subMenu) {
        this.gameWorld = gameWorld;
        this.subMenu = subMenu;

        makeBottomButton(subMenu, ITEM, ItemLoader.INSTANCE.getIconTextureRegion(NEST), 0);
        makeBottomButton(subMenu, GROUND, GroundLoader.INSTANCE.getIconTextureRegion(PATH), 1);
        makeBottomButton(subMenu, WALL, WallLoader.INSTANCE.getIconTextureRegion(BRICK_EW), 2);
        makeBottomButton(subMenu, PLANT, PlantLoader.INSTANCE.getIconTextureRegion(TREE), 3);
        makeBottomButton(subMenu, DINOSAUR, DinosaurLoader.INSTANCE.getIconTextureRegion(PROTOCERATOPS), 4);
        // TODO Implement buildings and staff.
        //makeBottomButton(subMenu, BUILDING, BuildingLoader.INSTANCE.getIconTextureRegion(BURGER_BAR), 5);
        //makeBottomButton(subMenu, STAFF, StaffLoader.INSTANCE.getIconTextureRegion(CLEANER), 6);

        makeBottomButton(subMenu, FINANCE, IconLoader.INSTANCE.getIconTextureRegion(COIN), 15);
        makeBottomButton(subMenu, PREFERENCES, IconLoader.INSTANCE.getIconTextureRegion(SETTINGS), 16);

        addBankBalance();
    }

    private void addBankBalance() {
        int textWidth = SCALE * 5;
        money = new TextDisplay(LEFT + WIDTH - textWidth, TOP, textWidth, SCALE, "", FontLoader.latrMedium, Color.WHITE);
    }

    private void makeBottomButton(SubMenu subMenu, SubMenuType subMenuType, TextureRegion iconTextureRegion, int i) {
        PickSubMenuAction action = new PickSubMenuAction(subMenu, subMenuType);
        TextureRegion texture = iconTextureRegion;
        MenuGraphicButton menuGraphicButton = new MenuGraphicButton(action, LEFT + BORDER + (SCALE + BORDER) * i, TOP + BORDER, texture);
        buttons.add(new Pair<>(subMenuType, menuGraphicButton));
    }

    public void drawButtons(SpriteBatch spriteBatch) {
        for (Pair<SubMenuType, MenuGraphicButton> buttonPair : buttons) {
            SubMenuType subMenuType = buttonPair.getLeft();
            MenuGraphicButton button = buttonPair.getRight();
            button.draw(spriteBatch);
            // New research
            if (subMenu.containsNewResearch(subMenuType)) {
                TextureRegion starTexture = IconLoader.INSTANCE.getIconTextureRegion(IconType.NEW_RESEARCH);
                DisplayElement star = new GraphicDisplay(button.getX(), button.getY(), starTexture);
                star.draw(spriteBatch);
            }
        }
        money.setText("£" + gameWorld.getZooStats().getMoney());
        money.draw(spriteBatch);
    }

    public void drawBackground(ShapeRenderer shapeRenderer) {
        shapeRenderer.setColor(BOTTOM_MENU_BACKGROUND);
        shapeRenderer.rect(LEFT, TOP, WIDTH, HEIGHT);
    }

    public Collection<? extends MenuButton> getAllButtons() {
        Collection<MenuGraphicButton> buttonList = new ArrayList<>();
        for (Pair<SubMenuType, MenuGraphicButton> buttonPair : buttons) {
            buttonList.add(buttonPair.getRight());
        }
        return buttonList;
    }
}
