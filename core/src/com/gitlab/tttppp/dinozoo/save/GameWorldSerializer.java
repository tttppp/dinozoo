package com.gitlab.tttppp.dinozoo.save;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.Json.Serializer;
import com.badlogic.gdx.utils.JsonValue;
import com.gitlab.tttppp.dinozoo.gameobject.Dinosaur;
import com.gitlab.tttppp.dinozoo.gameobject.GameObject;
import com.gitlab.tttppp.dinozoo.gameobject.Guest;
import com.gitlab.tttppp.dinozoo.gameobject.Land;
import com.gitlab.tttppp.dinozoo.gameworld.GameWorld;
import com.gitlab.tttppp.dinozoo.gameworld.Population;
import com.gitlab.tttppp.dinozoo.gameworld.Position;
import com.gitlab.tttppp.dinozoo.gameworld.Research;
import com.gitlab.tttppp.dinozoo.gameworld.ZooStats;
import com.gitlab.tttppp.dinozoo.utils.Log;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

import java.util.HashMap;
import java.util.Map;

public class GameWorldSerializer implements Serializer<GameWorld> {
    private GameWorld gameWorld;

    public GameWorldSerializer() {
        this.gameWorld = GameWorld.getInstance();
    }

    @Override
    public void write(Json json, GameWorld gameWorld, Class knownType) {
        json.writeObjectStart();
        json.writeValue("gameVersion", gameWorld.getGameVersion());
        json.writeValue("zooStats", gameWorld.getZooStats());
        json.writeValue("research", gameWorld.getResearch());
        json.writeValue("population", gameWorld.getPopulation());
        json.writeValue("ground", gameWorld.getGround());
        json.writeValue("fixedObjects", gameWorld.getFixedObjects());
        json.writeObjectStart("stackableObjects");
        for (Position position : gameWorld.getStackableObjects().keySet()) {
            json.writeArrayStart(position.toString());
            for (GameObject gameObject : gameWorld.getStackableObjects().get(position)) {
                json.writeValue(gameObject, GameObject.class);
            }
            json.writeArrayEnd();
        }
        json.writeObjectEnd();
        json.writeObjectEnd();
    }

    @Override
    public GameWorld read(Json json, JsonValue jsonData, Class type) {
        // Load the old game version in case it's needed for data model migration. Don't set it in the gameWorld though.
        Integer oldGameVersion = json.readValue(Integer.class, jsonData.get("gameVersion"));

        if (oldGameVersion < 6) {
            Log.log("Remove all references to Guest.seenDinosaurs since they used to use Multimaps, but these didn't serialise.");
            for (JsonValue stackablePosition : jsonData.get("stackableObjects")) {
                JsonValue.JsonIterator iterator = stackablePosition.iterator();
                while (iterator.hasNext()) {
                    JsonValue stackable = iterator.next();
                    if (stackable.getString("class").equals("com.gitlab.tttppp.dinozoo.gameobject.Guest")) {
                        stackable.remove("seenDinosaurs");
                    }
                }
            }
        }

        ZooStats zooStats = json.readValue(ZooStats.class, jsonData.get("zooStats"));
        gameWorld.setZooStats(zooStats);
        Research research = json.readValue(Research.class, jsonData.get("research"));
        research.refreshToResearch();
        gameWorld.setResearch(research);
        Population population = json.readValue(Population.class, jsonData.get("population"));
        gameWorld.setPopulation(population);
        Map<String, Land> groundJson = json.readValue(HashMap.class, jsonData.get("ground"));
        // Load ground.
        Map<Position, Land> ground = new HashMap<>();
        for (Map.Entry<String, Land> fromJson : groundJson.entrySet()) {
            Position position = Position.valueOf(fromJson.getKey());
            Land land = fromJson.getValue();
            if (oldGameVersion <= 9) {
                land.setPosition(position);
            }
            ground.put(position, land);
        }
        gameWorld.setGround(ground);
        // Load fixed objects.
        Map<String, GameObject> fixedObjectJson = json.readValue(HashMap.class, jsonData.get("fixedObjects"));
        Map<Position, GameObject> fixedObjects = new HashMap<>();
        for (Map.Entry<String, GameObject> fromJson : fixedObjectJson.entrySet()) {
            Position position = Position.valueOf(fromJson.getKey());
            fixedObjects.put(position, fromJson.getValue());
        }
        gameWorld.setFixedObjects(fixedObjects);
        // Load stackable objects.
        Map<String, Array<GameObject>> stackableObjectJson = json.readValue(HashMap.class, jsonData.get("stackableObjects"));
        Multimap<Position, GameObject> stackableObjects = HashMultimap.create();
        for (Map.Entry<String, Array<GameObject>> fromJson : stackableObjectJson.entrySet()) {
            Position position = Position.valueOf(fromJson.getKey());
            for (GameObject gameObject : fromJson.getValue()) {
                if (gameObject instanceof Dinosaur) {
                    Dinosaur dinosaur = (Dinosaur) gameObject;
                    dinosaur.init();
                }
                stackableObjects.put(position, gameObject);
            }
        }
        gameWorld.setStackableObjects(stackableObjects);

        Log.log("Game version was: " + oldGameVersion + " and is now: " + gameWorld.getGameVersion());

        return gameWorld;
    }
}
