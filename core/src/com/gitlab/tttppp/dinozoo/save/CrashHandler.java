package com.gitlab.tttppp.dinozoo.save;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.google.common.base.Throwables;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class CrashHandler {
    public RuntimeException handleCrashAndReturnException(RuntimeException e) {
        // Create a string containing the current timestamp.
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        String gameKey = simpleDateFormat.format(Calendar.getInstance().getTime());

        // Write log with stack trace.
        FileHandle errorFile = Gdx.files.local("dinozoo/error" + gameKey + ".txt");
        errorFile.writeString(Throwables.getStackTraceAsString(e), false);

        // Try to write game state to save file.
        GameSaver gameSaver = new GameSaver();
        gameSaver.saveGame(gameKey);

        // Return the to be thrown.
        return e;
    }
}
