package com.gitlab.tttppp.dinozoo.save;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.utils.Json;
import com.gitlab.tttppp.dinozoo.gameworld.GameWorld;
import com.gitlab.tttppp.dinozoo.utils.Log;

public class GameSaver {
    private Json json;

    public GameSaver() {
        json = new Json();
        json.setSerializer(GameWorld.class, new GameWorldSerializer());
    }

    public void saveGame(String key) {
        GameWorld gameWorld = GameWorld.getInstance();

        FileHandle file = getSaveGameFile(key);
        String jsonString = json.toJson(gameWorld);
        file.writeString(jsonString, false);
    }

    public GameWorld loadGame(String key) {
        FileHandle file = getSaveGameFile(key);
        String jsonString;
        try {
            jsonString = file.readString();
        } catch (GdxRuntimeException e) {
            Log.log("No such save game");
            return null;
        }
        GameWorld gameWorld = json.fromJson(GameWorld.class, jsonString);
        return gameWorld;
    }

    private FileHandle getSaveGameFile(String key) {
        return Gdx.files.local("dinozoo/savegame" + key + ".json");
    }
}
