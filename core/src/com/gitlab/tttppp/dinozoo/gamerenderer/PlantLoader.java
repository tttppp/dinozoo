package com.gitlab.tttppp.dinozoo.gamerenderer;

import com.gitlab.tttppp.dinozoo.gameworld.PlantType;
import com.google.common.collect.Lists;

import java.util.ArrayList;

import static com.gitlab.tttppp.dinozoo.gamerenderer.GameRenderer.SCALE;
import static com.gitlab.tttppp.dinozoo.gameworld.PlantType.ACACIA;
import static com.gitlab.tttppp.dinozoo.gameworld.PlantType.ACACIA_BARE;
import static com.gitlab.tttppp.dinozoo.gameworld.PlantType.BEECH_BARE;
import static com.gitlab.tttppp.dinozoo.gameworld.PlantType.BEECH;
import static com.gitlab.tttppp.dinozoo.gameworld.PlantType.BULL_RUSH_BARE;
import static com.gitlab.tttppp.dinozoo.gameworld.PlantType.BULL_RUSH;
import static com.gitlab.tttppp.dinozoo.gameworld.PlantType.BULL_RUSH_SHORT;
import static com.gitlab.tttppp.dinozoo.gameworld.PlantType.BUSH_BARE;
import static com.gitlab.tttppp.dinozoo.gameworld.PlantType.BUSH;
import static com.gitlab.tttppp.dinozoo.gameworld.PlantType.BUSH_LEAVES;
import static com.gitlab.tttppp.dinozoo.gameworld.PlantType.CACTUS;
import static com.gitlab.tttppp.dinozoo.gameworld.PlantType.CACTUS_EATEN;
import static com.gitlab.tttppp.dinozoo.gameworld.PlantType.FAN_PALM;
import static com.gitlab.tttppp.dinozoo.gameworld.PlantType.FAN_PALM_EATEN;
import static com.gitlab.tttppp.dinozoo.gameworld.PlantType.GIANT_DRACAENA;
import static com.gitlab.tttppp.dinozoo.gameworld.PlantType.GIANT_DRACAENA_BARE;
import static com.gitlab.tttppp.dinozoo.gameworld.PlantType.GIANT_DRACAENA_GROWING;
import static com.gitlab.tttppp.dinozoo.gameworld.PlantType.HORSES_TAIL_BARE;
import static com.gitlab.tttppp.dinozoo.gameworld.PlantType.HORSES_TAIL;
import static com.gitlab.tttppp.dinozoo.gameworld.PlantType.OAK_BARE;
import static com.gitlab.tttppp.dinozoo.gameworld.PlantType.OAK_GROWING;
import static com.gitlab.tttppp.dinozoo.gameworld.PlantType.OAK;
import static com.gitlab.tttppp.dinozoo.gameworld.PlantType.PALM_BARE;
import static com.gitlab.tttppp.dinozoo.gameworld.PlantType.PALM;
import static com.gitlab.tttppp.dinozoo.gameworld.PlantType.SILVER_BIRCH_BARE;
import static com.gitlab.tttppp.dinozoo.gameworld.PlantType.SILVER_BIRCH;
import static com.gitlab.tttppp.dinozoo.gameworld.PlantType.TREE_BARE;
import static com.gitlab.tttppp.dinozoo.gameworld.PlantType.TREE;

public class PlantLoader extends GridLoader<PlantType> {
    public static final PlantLoader INSTANCE = new PlantLoader();

    /**
     * Private constructor for singleton.
     */
    private PlantLoader() {
    }

    @Override
    public void load() {
        ArrayList<PlantType> plantTypes = Lists.newArrayList(TREE, TREE_BARE, BUSH, BUSH_LEAVES,
                BUSH_BARE, BEECH, BEECH_BARE, OAK, OAK_GROWING, OAK_BARE, HORSES_TAIL,
                HORSES_TAIL_BARE, BULL_RUSH, BULL_RUSH_BARE, BULL_RUSH_SHORT, SILVER_BIRCH,
                SILVER_BIRCH_BARE, PALM, PALM_BARE, CACTUS, CACTUS_EATEN, ACACIA, ACACIA_BARE, FAN_PALM, FAN_PALM_EATEN,
                GIANT_DRACAENA, GIANT_DRACAENA_GROWING, GIANT_DRACAENA_BARE);
        loadTexturesFromGrid("data/plants.png", plantTypes, SCALE, SCALE);
    }
}
