package com.gitlab.tttppp.dinozoo.gamerenderer;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;

import java.util.Arrays;
import java.util.List;

public class FontLoader {
	public static BitmapFont font, miniFont, latrBig, latr, latrMedium, latrSmall, latrQuiteSmall;
	/**
	 * The order of this list of fonts is used to determine the encoding as a
	 * byte. New fonts should always be added at the end, and fonts should not
	 * be removed, only replaced.
	 */
	private List<BitmapFont> fontList = Arrays.asList(font, miniFont, latrBig, latr, latrMedium, latrQuiteSmall, latrSmall);

	public static void load() {
		font = new BitmapFont(Gdx.files.internal("data/jottr.fnt"));
		font.getData().setScale(0.24f, -0.24f);
		miniFont = new BitmapFont(Gdx.files.internal("data/jottr.fnt"));
		miniFont.getData().setScale(0.15f, -0.15f);
		latrBig = new BitmapFont(Gdx.files.internal("data/latr.fnt"));
		latrBig.getData().setScale(1.5f, -1.5f);
		latrBig.setUseIntegerPositions(false);
		latr = new BitmapFont(Gdx.files.internal("data/latr.fnt"));
		latr.getData().setScale(0.8f, -0.8f);
		latr.setUseIntegerPositions(false);
		latrMedium = new BitmapFont(Gdx.files.internal("data/latr.fnt"));
		latrMedium.getData().setScale(0.5f, -0.5f);
		latrMedium.setUseIntegerPositions(false);
		latrQuiteSmall = new BitmapFont(Gdx.files.internal("data/latr.fnt"));
		latrQuiteSmall.getData().setScale(0.3f, -0.3f);
		latrQuiteSmall.setUseIntegerPositions(false);
		latrSmall = new BitmapFont(Gdx.files.internal("data/latr.fnt"));
		latrSmall.getData().setScale(0.18f, -0.18f);
		latrSmall.setUseIntegerPositions(false);
	}

	public static void dispose() {
		font.dispose();
	}

	public BitmapFont getNextFont(BitmapFont font) {
		int i = fontList.indexOf(font);
		i = (i + 1) % fontList.size();
		return fontList.get(i);
	}

	public BitmapFont getFont(byte encoding) {
		return fontList.get(encoding);
	}

	public byte getEncoding(BitmapFont font) {
		return (byte) fontList.indexOf(font);
	}
}
