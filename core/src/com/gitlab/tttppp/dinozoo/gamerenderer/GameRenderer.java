package com.gitlab.tttppp.dinozoo.gamerenderer;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.gitlab.tttppp.dinozoo.display.DisplayElement;
import com.gitlab.tttppp.dinozoo.display.MenuButton;
import com.gitlab.tttppp.dinozoo.display.TextDisplay;
import com.gitlab.tttppp.dinozoo.gameobject.GameObject;
import com.gitlab.tttppp.dinozoo.gameworld.GameWorld;
import com.gitlab.tttppp.dinozoo.gameworld.GroundSubType;
import com.gitlab.tttppp.dinozoo.gameworld.Position;
import com.gitlab.tttppp.dinozoo.input.GameInputHandler;
import com.gitlab.tttppp.dinozoo.screens.BottomMenu;
import com.gitlab.tttppp.dinozoo.screens.SubMenu;

import java.util.List;

public class GameRenderer extends Renderer {
    public static final int SCALE = 32;
    public static final int BORDER = 6;
    public static final int VISIBLE_TILES_WIDTH = 25;
    public static final int VISIBLE_TILES_HEIGHT = 16;
    public static final int VIEWPORT_WIDTH = VISIBLE_TILES_WIDTH * SCALE;
    public static final int VIEWPORT_HEIGHT = VISIBLE_TILES_HEIGHT * SCALE;
    public static final int MENU_HEIGHT = SCALE + 2 * BORDER;
    public static final int SUBMENU_HEIGHT = SCALE + 2 * BORDER;
    public static final int MENU_WIDTH = VIEWPORT_WIDTH;
    public static final int SUBMENU_WIDTH = VIEWPORT_WIDTH;
    public static final int SCREEN_WIDTH = VIEWPORT_WIDTH;
    public static final int SCREEN_HEIGHT = VIEWPORT_HEIGHT + MENU_HEIGHT + SUBMENU_HEIGHT;
    private static final Color TRANSLUCENT_BORDER = new Color(0x4040407f);
    private static final GroundLoader groundLoader = GroundLoader.INSTANCE;

    private GameWorld gameWorld;
    private List<MenuButton> buttons;
    private BottomMenu bottomMenu;
    private SubMenu subMenu;

    public GameRenderer(GameWorld gameWorld, List<MenuButton> buttons, BottomMenu bottomMenu, SubMenu subMenu) {
        super();
        this.gameWorld = gameWorld;
        this.buttons = buttons;
        this.bottomMenu = bottomMenu;
        this.subMenu = subMenu;
    }

    @Override
    public void render() {
        Gdx.gl.glClearColor(0, 0, 0, 1f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        spriteBatch.begin();
        for (Position position : gameWorld.getAllVisiblePositions()) {
            // Render ground.
            GroundSubType groundType = GroundSubType.EMPTY;
            if (gameWorld.getGround().containsKey(position)) {
                groundType = gameWorld.getGround().get(position).getGroundType();
            }
            TextureRegion groundTexture = groundLoader.getTextureRegion(groundType);
            spriteBatch.draw(groundTexture, (position.getX() - gameWorld.getViewX()) * SCALE,
                    (position.getY() - gameWorld.getViewY()) * SCALE, SCALE, SCALE);
        }
        for (Position position : gameWorld.getAllVisiblePositions()) {
            // Render any background.
            for (GameObject gameObject : gameWorld.getGameObjectsAt(position)) {
                if (gameObject.getForeground() != null) {
                    renderGameObject(position, gameObject.getTexture());
                }
            }
            // Render game objects that don't have separate background/foreground.
            for (GameObject gameObject : gameWorld.getGameObjectsAt(position)) {
                if (gameObject.getForeground() == null) {
                    renderGameObject(position, gameObject.getTexture());
                }
            }
            // Render any foreground.
            for (GameObject gameObject : gameWorld.getGameObjectsAt(position)) {
                if (gameObject.getForeground() != null) {
                    renderGameObject(position, gameObject.getForeground());
                }
            }
        }

        for (DisplayElement button : buttons) {
            button.draw(spriteBatch);
        }
        spriteBatch.end();

        // Draw border of map.
        Gdx.gl.glEnable(GL20.GL_BLEND);
        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(TRANSLUCENT_BORDER);
        shapeRenderer.rect(0, 0, VIEWPORT_WIDTH, SCALE);
        shapeRenderer.rect(0, (VISIBLE_TILES_HEIGHT - 1) * SCALE, VIEWPORT_WIDTH, SCALE);
        shapeRenderer.rect(0, SCALE, SCALE, VIEWPORT_HEIGHT - 2 * SCALE);
        shapeRenderer.rect(VIEWPORT_WIDTH - SCALE, SCALE, SCALE, VIEWPORT_HEIGHT - 2 * SCALE);
        shapeRenderer.end();

        renderMenus(bottomMenu, subMenu);
    }

    private void renderGameObject(Position position, TextureRegion texture) {
        TextureRegion objectTexture = texture;
        spriteBatch.draw(objectTexture,
                (position.getX() - gameWorld.getViewX()) * SCALE - (objectTexture.getRegionWidth() + SCALE) / 2 + SCALE,
                (position.getY() - gameWorld.getViewY()) * SCALE - objectTexture.getRegionHeight() + SCALE);
    }

    private void renderMenus(BottomMenu bottomMenu, SubMenu subMenu) {
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        subMenu.drawBackground(shapeRenderer);
        bottomMenu.drawBackground(shapeRenderer);
        shapeRenderer.end();

        spriteBatch.begin();

        String inputMode = "";
        if (gameWorld.isPaused()) {
            inputMode = "Paused";
        } else if (GameInputHandler.INSTANCE != null && GameInputHandler.INSTANCE.getMapPlacementObjectType() != null) {
            inputMode = GameInputHandler.INSTANCE.getMapPlacementObjectType().getDisplayString();
        }
        TextDisplay textDisplay = new TextDisplay(0, 0, VIEWPORT_WIDTH / 2, SCALE, inputMode, FontLoader.latrMedium, Color.WHITE);
        textDisplay.draw(spriteBatch);

        bottomMenu.drawButtons(spriteBatch);
        subMenu.drawButtons(spriteBatch);
        spriteBatch.end();
    }

    public BottomMenu getBottomMenu() {
        return bottomMenu;
    }

    public SubMenu getSubMenu() {
        return subMenu;
    }
}
