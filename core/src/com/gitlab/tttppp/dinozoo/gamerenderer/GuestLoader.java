package com.gitlab.tttppp.dinozoo.gamerenderer;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.gitlab.tttppp.dinozoo.gameworld.IntegerIdType;

import java.util.ArrayList;
import java.util.List;

import static com.gitlab.tttppp.dinozoo.gamerenderer.GameRenderer.SCALE;

public class GuestLoader implements TextureLoader<IntegerIdType> {
    public static GuestLoader INSTANCE = new GuestLoader();
    private Texture guestTexture;
    private List<TextureRegion> guestTextures = new ArrayList<>();

    /**
     * Private constructor for singleton.
     */
    private GuestLoader() {
    }

    @Override
    public void load() {
        guestTexture = new Texture(Gdx.files.internal("data/guests.png"));
        guestTexture.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);

        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                TextureRegion guestTextureRegion = new TextureRegion(guestTexture, SCALE * i, SCALE * j, SCALE, SCALE);
                guestTextureRegion.flip(false, true);
                guestTextures.add(guestTextureRegion);
            }
        }
    }

    @Override
    public void dispose() {
        guestTexture.dispose();
    }

    @Override
    public TextureRegion getTextureRegion(IntegerIdType guestType) {
        return guestTextures.get(guestType.getId());
    }

    @Override
    public TextureRegion getIconTextureRegion(IntegerIdType objectType) {
        return getTextureRegion(objectType);
    }

    public int getNumberOfGuestTextures() {
        return guestTextures.size();
    }
}
