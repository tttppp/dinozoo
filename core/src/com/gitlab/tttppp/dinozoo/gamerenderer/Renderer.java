package com.gitlab.tttppp.dinozoo.gamerenderer;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.gitlab.tttppp.dinozoo.utils.Log;

import static com.gitlab.tttppp.dinozoo.gamerenderer.GameRenderer.SCREEN_HEIGHT;
import static com.gitlab.tttppp.dinozoo.gamerenderer.GameRenderer.SCREEN_WIDTH;

public abstract class Renderer {
    protected float gameScreenHeight;
    protected OrthographicCamera orthographicCamera = new OrthographicCamera();
    protected ShapeRenderer shapeRenderer = new ShapeRenderer();
    protected SpriteBatch spriteBatch = new SpriteBatch();

    public Renderer() {
        resize();

        if (gameScreenHeight < SCREEN_HEIGHT) {
            Log.log("Error - screen height not big enough");
        }

        orthographicCamera.setToOrtho(true, SCREEN_WIDTH, gameScreenHeight);
        shapeRenderer.setProjectionMatrix(orthographicCamera.combined);
        spriteBatch.setProjectionMatrix(orthographicCamera.combined);
    }

    public abstract void render();

    public void resize() {
        float screenWidth = Gdx.graphics.getWidth();
        float screenHeight = Gdx.graphics.getHeight();
        float gameScreenWidth = SCREEN_WIDTH;
        gameScreenHeight = gameScreenWidth * screenHeight / screenWidth;
    }
}
