package com.gitlab.tttppp.dinozoo.gamerenderer;

import com.gitlab.tttppp.dinozoo.gameworld.ItemType;
import com.google.common.collect.Lists;

import java.util.ArrayList;

import static com.gitlab.tttppp.dinozoo.gamerenderer.GameRenderer.SCALE;
import static com.gitlab.tttppp.dinozoo.gameworld.ItemType.BULLDOZER;
import static com.gitlab.tttppp.dinozoo.gameworld.ItemType.NEST;
import static com.gitlab.tttppp.dinozoo.gameworld.ItemType.NEST_FOREGROUND;
import static com.gitlab.tttppp.dinozoo.gameworld.ItemType.SKELETON;
import static com.gitlab.tttppp.dinozoo.gameworld.ItemType.SPLAT_EGG;

public class ItemLoader extends GridLoader<ItemType> {
    public static final ItemLoader INSTANCE = new ItemLoader();

    /**
     * Private constructor for singleton.
     */
    private ItemLoader() {
    }

    @Override
    public void load() {
        ArrayList<ItemType> itemTypes = Lists.newArrayList(NEST, NEST_FOREGROUND, BULLDOZER, SKELETON, SPLAT_EGG);
        loadTexturesFromGrid("data/items.png", itemTypes, SCALE, SCALE);
    }
}

