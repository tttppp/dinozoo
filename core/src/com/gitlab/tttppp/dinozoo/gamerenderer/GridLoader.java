package com.gitlab.tttppp.dinozoo.gamerenderer;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.gitlab.tttppp.dinozoo.utils.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.gitlab.tttppp.dinozoo.gamerenderer.GameRenderer.SCALE;

public abstract class GridLoader<T> implements TextureLoader<T> {
    protected Texture texture;
    protected Map<T, TextureRegion> textures = new HashMap<>();
    protected Map<T, TextureRegion> textureIcons = new HashMap<>();

    public void loadTexturesFromGrid(String textureFile, ArrayList<T> objectTypes, int xScale, int yScale) {
        texture = new Texture(Gdx.files.internal(textureFile));
        texture.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);

        int i = 0;
        int j = 0;
        for (T objectType : objectTypes) {
            TextureRegion textureRegion = new TextureRegion(texture, xScale * i, yScale * j, xScale, yScale);
            textureRegion.flip(false, true);
            textures.put(objectType, textureRegion);
            // Create an icon that fits in a grid cell.
            TextureRegion textureIcon = new TextureRegion(texture, xScale * i, yScale * (j + 1) - SCALE, SCALE, SCALE);
            textureIcon.flip(false, true);
            textureIcons.put(objectType, textureIcon);
            i++;
            if (i * xScale > texture.getWidth() - 1) {
                i = 0;
                j++;
            }
        }
    }

    @Override
    public void dispose() {
        texture.dispose();
    }

    @Override
    public TextureRegion getTextureRegion(T objectType) {
        return textures.get(objectType);
    }

    @Override
    public TextureRegion getIconTextureRegion(T objectType) {
        return textureIcons.get(objectType);
    }
}
