package com.gitlab.tttppp.dinozoo.gamerenderer;

import com.gitlab.tttppp.dinozoo.gameworld.IconType;
import com.google.common.collect.Lists;

import java.util.ArrayList;

import static com.gitlab.tttppp.dinozoo.gamerenderer.GameRenderer.SCALE;
import static com.gitlab.tttppp.dinozoo.gameworld.IconType.COIN;
import static com.gitlab.tttppp.dinozoo.gameworld.IconType.ECSTATIC_FACE;
import static com.gitlab.tttppp.dinozoo.gameworld.IconType.HAPPY_FACE;
import static com.gitlab.tttppp.dinozoo.gameworld.IconType.LOAD;
import static com.gitlab.tttppp.dinozoo.gameworld.IconType.NEUTRAL_FACE;
import static com.gitlab.tttppp.dinozoo.gameworld.IconType.NEW_RESEARCH;
import static com.gitlab.tttppp.dinozoo.gameworld.IconType.PLAY_PAUSE;
import static com.gitlab.tttppp.dinozoo.gameworld.IconType.POPULARITY;
import static com.gitlab.tttppp.dinozoo.gameworld.IconType.RESEARCH;
import static com.gitlab.tttppp.dinozoo.gameworld.IconType.SAD_FACE;
import static com.gitlab.tttppp.dinozoo.gameworld.IconType.SATISFIED_FACE;
import static com.gitlab.tttppp.dinozoo.gameworld.IconType.SAVE_1;
import static com.gitlab.tttppp.dinozoo.gameworld.IconType.SCROLL_LEFT;
import static com.gitlab.tttppp.dinozoo.gameworld.IconType.SCROLL_RIGHT;
import static com.gitlab.tttppp.dinozoo.gameworld.IconType.SETTINGS;

public class IconLoader extends GridLoader<IconType> {
    public static final IconLoader INSTANCE = new IconLoader();

    /**
     * Private constructor for singleton.
     */
    private IconLoader() {
    }

    @Override
    public void load() {
        ArrayList<IconType> iconTypes = Lists.newArrayList(SAVE_1, LOAD, COIN, RESEARCH, SETTINGS, NEW_RESEARCH,
                SAD_FACE, NEUTRAL_FACE, SATISFIED_FACE, HAPPY_FACE, ECSTATIC_FACE, POPULARITY, PLAY_PAUSE, SCROLL_LEFT,
                SCROLL_RIGHT);
        loadTexturesFromGrid("data/icons.png", iconTypes, SCALE, SCALE);
    }
}

