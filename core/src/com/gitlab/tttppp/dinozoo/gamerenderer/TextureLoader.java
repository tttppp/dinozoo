package com.gitlab.tttppp.dinozoo.gamerenderer;

import com.badlogic.gdx.graphics.g2d.TextureRegion;

public interface TextureLoader<ObjectType> {
    void load();

    void dispose();

    TextureRegion getTextureRegion(ObjectType objectType);

    /** Get a texture that will fit in a single cell. */
    TextureRegion getIconTextureRegion(ObjectType objectType);
}
