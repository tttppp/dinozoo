package com.gitlab.tttppp.dinozoo.gamerenderer;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.gitlab.tttppp.dinozoo.display.DisplayElement;

import java.util.List;

public class MenuRenderer extends Renderer {
	private List<DisplayElement> displayElements;

	public MenuRenderer(List<DisplayElement> displayElements) {
		super();
		this.displayElements = displayElements;
	}

	@Override
	public void render() {
		Gdx.gl.glClearColor(0.3f, 0.1f, 0.5f, 1f);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		spriteBatch.begin();
		for (DisplayElement displayElements : displayElements) {
			displayElements.draw(spriteBatch);
		}

		spriteBatch.end();
	}

	public void setDisplayElements(List<DisplayElement> displayElements) {
		this.displayElements = displayElements;
	}
}
