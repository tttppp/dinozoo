package com.gitlab.tttppp.dinozoo.gamerenderer;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import static com.gitlab.tttppp.dinozoo.gamerenderer.GameRenderer.SCALE;

public class ButtonLoader {
	private static Texture buttonTexture;
    public static TextureRegion wideButtonUp, wideButtonDown, mediumButtonUp, mediumButtonDown, squareButtonUp,
			squareButtonDown, backgroundTop, backgroundBottom, tinyButtonUp, tinyButtonDown, scrollUpUp, scrollUpDown,
			scrollDownUp, scrollDownDown, scrollLeftUp, scrollLeftDown, scrollRightUp, scrollRightDown, scrollULUp,
			scrollULDown, scrollURUp, scrollURDown, scrollDLUp, scrollDLDown, scrollDRUp, scrollDRDown;

	public static void load() {
		buttonTexture = new Texture(Gdx.files.internal("data/buttons.png"));
		buttonTexture.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);

		wideButtonUp = new TextureRegion(buttonTexture, 0, 0, SCALE * 11, SCALE * 2);
		wideButtonUp.flip(false, true);
		wideButtonDown = new TextureRegion(buttonTexture, 0, SCALE * 2, SCALE * 11, SCALE * 2);
		wideButtonDown.flip(false, true);
		squareButtonUp = new TextureRegion(buttonTexture, 0, SCALE * 4, SCALE * 2, SCALE * 2);
		squareButtonUp.flip(false, true);
		squareButtonDown = new TextureRegion(buttonTexture, SCALE * 2, SCALE * 4, SCALE * 2, SCALE * 2);
		squareButtonDown.flip(false, true);

		backgroundTop = new TextureRegion(buttonTexture, SCALE * 8, SCALE * 4, SCALE, SCALE);
		backgroundTop.flip(false, true);
		backgroundBottom = new TextureRegion(buttonTexture, SCALE * 8, 45, SCALE, SCALE);
		backgroundBottom.flip(false, true);

		mediumButtonUp = new TextureRegion(buttonTexture, 0, SCALE * 6, SCALE * 4, SCALE * 2);
		mediumButtonUp.flip(false, true);
		mediumButtonDown = new TextureRegion(buttonTexture, SCALE * 4, SCALE * 6, SCALE * 4, SCALE * 2);
		mediumButtonDown.flip(false, true);
		tinyButtonUp = new TextureRegion(buttonTexture, SCALE * 8, SCALE * 6, SCALE, SCALE);
		tinyButtonUp.flip(false, true);
		tinyButtonDown = new TextureRegion(buttonTexture, SCALE * 9, SCALE * 6, SCALE, SCALE);
		tinyButtonDown.flip(false, true);

		scrollUpUp = new TextureRegion(buttonTexture, 0, SCALE * 8, SCALE * 2, SCALE);
		scrollUpUp.flip(false, true);
		scrollUpDown = new TextureRegion(buttonTexture, 0, SCALE * 9, SCALE * 2, SCALE);
		scrollUpDown.flip(false, true);
		scrollDownUp = new TextureRegion(buttonTexture, 0, SCALE * 8, SCALE * 2, SCALE);
		scrollDownUp.flip(false, false);
		scrollDownDown = new TextureRegion(buttonTexture, 0, SCALE * 9, SCALE * 2, SCALE);
		scrollDownDown.flip(false, false);
		scrollLeftUp = new TextureRegion(buttonTexture, SCALE * 2, SCALE * 8, SCALE, SCALE * 2);
		scrollLeftUp.flip(false, true);
		scrollLeftDown = new TextureRegion(buttonTexture, SCALE * 3, SCALE * 8, SCALE, SCALE * 2);
		scrollLeftDown.flip(false, true);
		scrollRightUp = new TextureRegion(buttonTexture, SCALE * 2, SCALE * 8, SCALE, SCALE * 2);
		scrollRightUp.flip(true, true);
		scrollRightDown = new TextureRegion(buttonTexture, SCALE * 3, SCALE * 8, SCALE, SCALE * 2);
		scrollRightDown.flip(true, true);
		scrollULUp = new TextureRegion(buttonTexture, SCALE * 4, SCALE * 8, SCALE, SCALE);
		scrollULUp.flip(false, true);
		scrollULDown = new TextureRegion(buttonTexture, SCALE * 4, SCALE * 9, SCALE, SCALE);
		scrollULDown.flip(false, true);
		scrollURUp = new TextureRegion(buttonTexture, SCALE * 4, SCALE * 8, SCALE, SCALE);
		scrollURUp.flip(true, true);
		scrollURDown = new TextureRegion(buttonTexture, SCALE * 4, SCALE * 9, SCALE, SCALE);
		scrollURDown.flip(true, true);
		scrollDLUp = new TextureRegion(buttonTexture, SCALE * 4, SCALE * 8, SCALE, SCALE);
		scrollDLUp.flip(false, false);
		scrollDLDown = new TextureRegion(buttonTexture, SCALE * 4, SCALE * 9, SCALE, SCALE);
		scrollDLDown.flip(false, false);
		scrollDRUp = new TextureRegion(buttonTexture, SCALE * 4, SCALE * 8, SCALE, SCALE);
		scrollDRUp.flip(true, false);
		scrollDRDown = new TextureRegion(buttonTexture, SCALE * 4, SCALE * 9, SCALE, SCALE);
		scrollDRDown.flip(true, false);
	}

	public static void dispose() {
		buttonTexture.dispose();
	}
}
