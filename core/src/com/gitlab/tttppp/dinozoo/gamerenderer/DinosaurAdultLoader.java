package com.gitlab.tttppp.dinozoo.gamerenderer;

import com.gitlab.tttppp.dinozoo.gameworld.DinosaurType;
import com.google.common.collect.Lists;

import java.util.ArrayList;

import static com.gitlab.tttppp.dinozoo.gamerenderer.GameRenderer.SCALE;
import static com.gitlab.tttppp.dinozoo.gameworld.DinosaurType.ANKYLOSAURUS;
import static com.gitlab.tttppp.dinozoo.gameworld.DinosaurType.ASTRASPIS;
import static com.gitlab.tttppp.dinozoo.gameworld.DinosaurType.BASILOSAURUS;
import static com.gitlab.tttppp.dinozoo.gameworld.DinosaurType.COMPSOGNATHUS;
import static com.gitlab.tttppp.dinozoo.gameworld.DinosaurType.DIPLODOCUS;
import static com.gitlab.tttppp.dinozoo.gameworld.DinosaurType.HERRARASAURUS;
import static com.gitlab.tttppp.dinozoo.gameworld.DinosaurType.HUAYANGOSAURUS;
import static com.gitlab.tttppp.dinozoo.gameworld.DinosaurType.LEAELLYNASAURA;
import static com.gitlab.tttppp.dinozoo.gameworld.DinosaurType.LONGISQUAMA;
import static com.gitlab.tttppp.dinozoo.gameworld.DinosaurType.LYSTROSAURUS;
import static com.gitlab.tttppp.dinozoo.gameworld.DinosaurType.MAIASAURA;
import static com.gitlab.tttppp.dinozoo.gameworld.DinosaurType.PARASAUROLOPHUS;
import static com.gitlab.tttppp.dinozoo.gameworld.DinosaurType.PLESIOSAURUS;
import static com.gitlab.tttppp.dinozoo.gameworld.DinosaurType.PROTEROSUCHUS;
import static com.gitlab.tttppp.dinozoo.gameworld.DinosaurType.PROTOCERATOPS;
import static com.gitlab.tttppp.dinozoo.gameworld.DinosaurType.SPINOSAURUS;
import static com.gitlab.tttppp.dinozoo.gameworld.DinosaurType.STEGACERAS;
import static com.gitlab.tttppp.dinozoo.gameworld.DinosaurType.STEGOSAURUS;
import static com.gitlab.tttppp.dinozoo.gameworld.DinosaurType.TANYSTROPHEUS;
import static com.gitlab.tttppp.dinozoo.gameworld.DinosaurType.THECONTASAURUS;
import static com.gitlab.tttppp.dinozoo.gameworld.DinosaurType.TYRANOSAURUS_REX;
import static com.gitlab.tttppp.dinozoo.gameworld.DinosaurType.TRICERATOPS;

public class DinosaurAdultLoader extends GridLoader<DinosaurType> {
    public static DinosaurAdultLoader INSTANCE = new DinosaurAdultLoader();

    /**
     * Private constructor for singleton.
     */
    private DinosaurAdultLoader() {
    }

    @Override
    public void load() {
        ArrayList<DinosaurType> dinosaurTypes = Lists.newArrayList(PROTOCERATOPS, TRICERATOPS, DIPLODOCUS, TYRANOSAURUS_REX,
                STEGOSAURUS, LEAELLYNASAURA, SPINOSAURUS, PARASAUROLOPHUS, ANKYLOSAURUS, COMPSOGNATHUS,
                PROTEROSUCHUS, TANYSTROPHEUS, LYSTROSAURUS, MAIASAURA, LONGISQUAMA, HUAYANGOSAURUS, THECONTASAURUS,
                HERRARASAURUS, ASTRASPIS, BASILOSAURUS, PLESIOSAURUS, STEGACERAS);
        loadTexturesFromGrid("data/dinoAdults.png", dinosaurTypes, 3 * SCALE, 3 * SCALE);
    }
}

