package com.gitlab.tttppp.dinozoo.gamerenderer;

import com.gitlab.tttppp.dinozoo.gameworld.StaffType;
import com.google.common.collect.Lists;

import java.util.ArrayList;

import static com.gitlab.tttppp.dinozoo.gamerenderer.GameRenderer.SCALE;
import static com.gitlab.tttppp.dinozoo.gameworld.StaffType.CLEANER;

public class StaffLoader extends GridLoader<StaffType> {
    public static StaffLoader INSTANCE = new StaffLoader();

    /**
     * Private constructor for singleton.
     */
    private StaffLoader() {
    }

    @Override
    public void load() {
        ArrayList<StaffType> staffTypes = Lists.newArrayList(CLEANER);
        loadTexturesFromGrid("data/staff.png", staffTypes, SCALE, SCALE);
    }
}
