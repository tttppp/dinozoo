package com.gitlab.tttppp.dinozoo.gamerenderer;

import com.gitlab.tttppp.dinozoo.gameworld.GroundSubType;
import com.google.common.collect.Lists;

import java.util.ArrayList;

import static com.gitlab.tttppp.dinozoo.gamerenderer.GameRenderer.SCALE;
import static com.gitlab.tttppp.dinozoo.gameworld.GroundSubType.BLOCKS;
import static com.gitlab.tttppp.dinozoo.gameworld.GroundSubType.BOG;
import static com.gitlab.tttppp.dinozoo.gameworld.GroundSubType.CLAY;
import static com.gitlab.tttppp.dinozoo.gameworld.GroundSubType.DIRT;
import static com.gitlab.tttppp.dinozoo.gameworld.GroundSubType.EMPTY;
import static com.gitlab.tttppp.dinozoo.gameworld.GroundSubType.FOOTPRINTS;
import static com.gitlab.tttppp.dinozoo.gameworld.GroundSubType.LIGHT_GRASS;
import static com.gitlab.tttppp.dinozoo.gameworld.GroundSubType.MARBLE;
import static com.gitlab.tttppp.dinozoo.gameworld.GroundSubType.PATH;
import static com.gitlab.tttppp.dinozoo.gameworld.GroundSubType.PLANKS;
import static com.gitlab.tttppp.dinozoo.gameworld.GroundSubType.RYEGRASS;
import static com.gitlab.tttppp.dinozoo.gameworld.GroundSubType.SAND;
import static com.gitlab.tttppp.dinozoo.gameworld.GroundSubType.SNOW;
import static com.gitlab.tttppp.dinozoo.gameworld.GroundSubType.TROPICAL_SAND;
import static com.gitlab.tttppp.dinozoo.gameworld.GroundSubType.WATER;
import static com.gitlab.tttppp.dinozoo.gameworld.GroundSubType.WATER_1;
import static com.gitlab.tttppp.dinozoo.gameworld.GroundSubType.WATER_2;
import static com.gitlab.tttppp.dinozoo.gameworld.GroundSubType.WATER_3;
import static com.gitlab.tttppp.dinozoo.gameworld.GroundSubType.WATER_4;
import static com.gitlab.tttppp.dinozoo.gameworld.GroundSubType.WET_SAND;

public class GroundLoader extends GridLoader<GroundSubType> {
    public static final GroundLoader INSTANCE = new GroundLoader();

    /**
     * Private constructor for singleton.
     */
    private GroundLoader() {
    }

    @Override
    public void load() {
        ArrayList<GroundSubType> groundTypes = Lists.newArrayList(EMPTY, DIRT, FOOTPRINTS, PATH, WATER, SAND,
                LIGHT_GRASS, BOG, PLANKS, MARBLE, SNOW, WET_SAND, RYEGRASS, BLOCKS, CLAY, TROPICAL_SAND, WATER_1, WATER_2, WATER_3, WATER_4);
        loadTexturesFromGrid("data/ground.png", groundTypes, SCALE, SCALE);
    }
}

