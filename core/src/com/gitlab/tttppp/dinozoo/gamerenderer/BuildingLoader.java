package com.gitlab.tttppp.dinozoo.gamerenderer;

import com.gitlab.tttppp.dinozoo.gameworld.BuildingType;
import com.google.common.collect.Lists;

import java.util.ArrayList;

import static com.gitlab.tttppp.dinozoo.gamerenderer.GameRenderer.SCALE;
import static com.gitlab.tttppp.dinozoo.gameworld.BuildingType.BURGER_BAR;
import static com.gitlab.tttppp.dinozoo.gameworld.BuildingType.ENTRY_EW;
import static com.gitlab.tttppp.dinozoo.gameworld.BuildingType.ENTRY_NS;
import static com.gitlab.tttppp.dinozoo.gameworld.BuildingType.SOFT_DRINKS;
import static com.gitlab.tttppp.dinozoo.gameworld.BuildingType.SOLID_WALL;
import static com.gitlab.tttppp.dinozoo.gameworld.BuildingType.STAFF_ROOM;
import static com.gitlab.tttppp.dinozoo.gameworld.BuildingType.SURVEY_POST;

public class BuildingLoader extends GridLoader<BuildingType> {
    public static final BuildingLoader INSTANCE = new BuildingLoader();

    /**
     * Private constructor for singleton.
     */
    private BuildingLoader() {
    }

    @Override
    public void load() {
        ArrayList<BuildingType> buildingTypes = Lists.newArrayList(BURGER_BAR, SOLID_WALL, ENTRY_NS, ENTRY_EW,
                SOFT_DRINKS, SURVEY_POST, STAFF_ROOM);
        loadTexturesFromGrid("data/buildings.png", buildingTypes, SCALE, 2 * SCALE);
    }
}

