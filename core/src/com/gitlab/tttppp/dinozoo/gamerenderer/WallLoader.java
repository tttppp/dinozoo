package com.gitlab.tttppp.dinozoo.gamerenderer;

import com.gitlab.tttppp.dinozoo.gameworld.WallType;
import com.google.common.collect.Lists;

import java.util.ArrayList;

import static com.gitlab.tttppp.dinozoo.gamerenderer.GameRenderer.SCALE;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.BRICK_E;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.BRICK_EN;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.BRICK_ENS;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.BRICK_ENSW;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.BRICK_ENW;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.BRICK_ES;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.BRICK_ESW;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.BRICK_EW;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.BRICK_N;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.BRICK_NS;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.BRICK_NSW;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.BRICK_NW;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.BRICK_POST;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.BRICK_S;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.BRICK_SW;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.BRICK_W;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.CHAINLINK_E;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.CHAINLINK_EN;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.CHAINLINK_ENS;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.CHAINLINK_ENSW;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.CHAINLINK_ENW;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.CHAINLINK_ES;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.CHAINLINK_ESW;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.CHAINLINK_EW;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.CHAINLINK_N;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.CHAINLINK_NS;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.CHAINLINK_NSW;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.CHAINLINK_NW;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.CHAINLINK_POST;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.CHAINLINK_S;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.CHAINLINK_SW;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.CHAINLINK_W;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.CONCRETE_E;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.CONCRETE_EN;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.CONCRETE_ENS;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.CONCRETE_ENSW;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.CONCRETE_ENW;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.CONCRETE_ES;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.CONCRETE_ESW;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.CONCRETE_EW;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.CONCRETE_N;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.CONCRETE_NS;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.CONCRETE_NSW;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.CONCRETE_NW;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.CONCRETE_POST;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.CONCRETE_S;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.CONCRETE_SW;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.CONCRETE_W;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.ELECTRIC_E;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.ELECTRIC_EN;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.ELECTRIC_ENS;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.ELECTRIC_ENSW;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.ELECTRIC_ENW;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.ELECTRIC_ES;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.ELECTRIC_ESW;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.ELECTRIC_EW;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.ELECTRIC_N;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.ELECTRIC_NS;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.ELECTRIC_NSW;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.ELECTRIC_NW;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.ELECTRIC_POST;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.ELECTRIC_S;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.ELECTRIC_SW;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.ELECTRIC_W;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.GLASS_E;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.GLASS_EN;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.GLASS_ENS;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.GLASS_ENSW;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.GLASS_ENW;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.GLASS_ES;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.GLASS_ESW;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.GLASS_EW;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.GLASS_N;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.GLASS_NS;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.GLASS_NSW;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.GLASS_NW;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.GLASS_POST;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.GLASS_S;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.GLASS_SW;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.GLASS_W;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.WOODEN_E;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.WOODEN_EN;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.WOODEN_ENS;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.WOODEN_ENSW;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.WOODEN_ENW;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.WOODEN_ES;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.WOODEN_ESW;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.WOODEN_EW;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.WOODEN_N;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.WOODEN_NS;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.WOODEN_NSW;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.WOODEN_NW;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.WOODEN_POST;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.WOODEN_S;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.WOODEN_SW;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.WOODEN_W;

public class WallLoader extends GridLoader<WallType> {
    public static final WallLoader INSTANCE = new WallLoader();

    /**
     * Private constructor for singleton.
     */
    private WallLoader() {
    }

    @Override
    public void load() {
        ArrayList<WallType> wallTypes = Lists.newArrayList(
                BRICK_POST, BRICK_W, BRICK_E, BRICK_N, BRICK_S, BRICK_SW, BRICK_NW, BRICK_EN,
                BRICK_ES, BRICK_EW, BRICK_NS, BRICK_ESW, BRICK_NSW, BRICK_ENW, BRICK_ENS, BRICK_ENSW,
                CHAINLINK_POST, CHAINLINK_W, CHAINLINK_E, CHAINLINK_N, CHAINLINK_S, CHAINLINK_SW, CHAINLINK_NW, CHAINLINK_EN,
                CHAINLINK_ES, CHAINLINK_EW, CHAINLINK_NS, CHAINLINK_ESW, CHAINLINK_NSW, CHAINLINK_ENW, CHAINLINK_ENS, CHAINLINK_ENSW,
                GLASS_POST, GLASS_W, GLASS_E, GLASS_N, GLASS_S, GLASS_SW, GLASS_NW, GLASS_EN,
                GLASS_ES, GLASS_EW, GLASS_NS, GLASS_ESW, GLASS_NSW, GLASS_ENW, GLASS_ENS, GLASS_ENSW,
                CONCRETE_POST, CONCRETE_W, CONCRETE_E, CONCRETE_N, CONCRETE_S, CONCRETE_SW, CONCRETE_NW, CONCRETE_EN,
                CONCRETE_ES, CONCRETE_EW, CONCRETE_NS, CONCRETE_ESW, CONCRETE_NSW, CONCRETE_ENW, CONCRETE_ENS, CONCRETE_ENSW,
                ELECTRIC_POST, ELECTRIC_W, ELECTRIC_E, ELECTRIC_N, ELECTRIC_S, ELECTRIC_SW, ELECTRIC_NW, ELECTRIC_EN,
                ELECTRIC_ES, ELECTRIC_EW, ELECTRIC_NS, ELECTRIC_ESW, ELECTRIC_NSW, ELECTRIC_ENW, ELECTRIC_ENS, ELECTRIC_ENSW,
                WOODEN_POST, WOODEN_W, WOODEN_E, WOODEN_N, WOODEN_S, WOODEN_SW, WOODEN_NW, WOODEN_EN,
                WOODEN_ES, WOODEN_EW, WOODEN_NS, WOODEN_ESW, WOODEN_NSW, WOODEN_ENW, WOODEN_ENS, WOODEN_ENSW);
        loadTexturesFromGrid("data/walls.png", wallTypes, SCALE, 2 * SCALE);
    }
}

