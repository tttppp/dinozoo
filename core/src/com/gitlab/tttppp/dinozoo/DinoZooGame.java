package com.gitlab.tttppp.dinozoo;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.gitlab.tttppp.dinozoo.gamerenderer.BuildingLoader;
import com.gitlab.tttppp.dinozoo.gamerenderer.ButtonLoader;
import com.gitlab.tttppp.dinozoo.gamerenderer.DinosaurAdultLoader;
import com.gitlab.tttppp.dinozoo.gamerenderer.DinosaurLoader;
import com.gitlab.tttppp.dinozoo.gamerenderer.EggLoader;
import com.gitlab.tttppp.dinozoo.gamerenderer.FontLoader;
import com.gitlab.tttppp.dinozoo.gamerenderer.GroundLoader;
import com.gitlab.tttppp.dinozoo.gamerenderer.GuestLoader;
import com.gitlab.tttppp.dinozoo.gamerenderer.IconLoader;
import com.gitlab.tttppp.dinozoo.gamerenderer.ItemLoader;
import com.gitlab.tttppp.dinozoo.gamerenderer.PlantLoader;
import com.gitlab.tttppp.dinozoo.gamerenderer.StaffLoader;
import com.gitlab.tttppp.dinozoo.gamerenderer.TextureLoader;
import com.gitlab.tttppp.dinozoo.gamerenderer.WallLoader;
import com.gitlab.tttppp.dinozoo.screens.MainMenuScreen;
import com.gitlab.tttppp.dinozoo.utils.Log;
import com.google.common.collect.Lists;

import java.util.List;

public class DinoZooGame extends Game {
    private List<? extends TextureLoader> textureLoaders = Lists.newArrayList(GroundLoader.INSTANCE,
            WallLoader.INSTANCE, PlantLoader.INSTANCE, ItemLoader.INSTANCE, EggLoader.INSTANCE, DinosaurLoader.INSTANCE,
            DinosaurAdultLoader.INSTANCE, StaffLoader.INSTANCE, GuestLoader.INSTANCE, BuildingLoader.INSTANCE,
            IconLoader.INSTANCE);

    @Override
    public void create() {
        Log.log("Created");

        for (TextureLoader textureLoader : textureLoaders) {
            textureLoader.load();
        }
        FontLoader.load();
        ButtonLoader.load();

        Gdx.input.setCatchKey(Input.Keys.BACK, true);

        setScreen(new MainMenuScreen(this));
    }

    @Override
    public void dispose() {
        super.dispose();

        for (TextureLoader textureLoader : textureLoaders) {
            textureLoader.dispose();
        }
        FontLoader.dispose();
        ButtonLoader.dispose();
    }
}
