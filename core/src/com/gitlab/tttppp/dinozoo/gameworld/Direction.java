package com.gitlab.tttppp.dinozoo.gameworld;

import com.google.common.collect.Lists;

import java.util.List;

import static com.gitlab.tttppp.dinozoo.gameworld.GameWorld.MAP_HEIGHT;
import static com.gitlab.tttppp.dinozoo.gameworld.GameWorld.MAP_WIDTH;

public enum Direction {
    UP(0, -1), DOWN(0, 1), LEFT(-1, 0), RIGHT(1, 0), HERE(0, 0);

    public static final List<Direction> ADJACENCIES = Lists.newArrayList(UP, DOWN, LEFT, RIGHT);

    private final int deltaX;
    private final int deltaY;

    Direction(int deltaX, int deltaY) {
        this.deltaX = deltaX;
        this.deltaY = deltaY;
    }

    /**
     * Apply the direction to a position.
     *
     * @param start The position to start from.
     * @return The destination, or null if it's outside the map.
     */
    public Position applyTo(Position start) {
        Position destination = Position.of(start.getX() + deltaX, start.getY() + deltaY);
        if (destination.getX() < 0 || destination.getX() >= MAP_WIDTH || destination.getY() < 0 || destination.getY() >= MAP_HEIGHT) {
            destination = null;
        }
        return destination;
    }

    public Direction opposite() {
        switch (this) {
            case UP:
                return DOWN;
            case DOWN:
                return UP;
            case LEFT:
                return RIGHT;
            case RIGHT:
                return LEFT;
            case HERE:
                throw new IllegalStateException("No opposite of HERE");
        }
        return null;
    }
}
