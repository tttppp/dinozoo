package com.gitlab.tttppp.dinozoo.gameworld;

public interface ObjectType {
    /** Get the foreground object type or null if the object has no foreground. */
    ObjectType getForeground();

    /** Get the price or null if the object cannot be bought. */
    Integer getPrice();

    /** A string to show to the user. */
    String getDisplayString();
}
