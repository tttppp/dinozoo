package com.gitlab.tttppp.dinozoo.gameworld;

public enum GroundType {
    GRASS(true), DIRT(false), FOOTPRINTS(false), PATH(false), WATER(false),
    SAND(false), BOG(true), SNOW(false);

    private boolean edible;

    GroundType(boolean edible) {
        this.edible = edible;
    }

    public boolean isEdible() {
        return edible;
    }
}

