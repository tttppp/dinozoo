package com.gitlab.tttppp.dinozoo.gameworld;

import com.gitlab.tttppp.dinozoo.utils.RandomUtils;

public class Population {
    private static final int INITIAL_AVERAGE_WEALTH = 200;
    private static final double DAILY_INFLATION_RATE = 1.00000863;

    private double averageWealth = INITIAL_AVERAGE_WEALTH;
    private long ticksUntilInflation = GameWorld.TICKS_IN_DAY;

    public void tick() {
        ticksUntilInflation--;
        if (ticksUntilInflation <= 0) {
            averageWealth *= DAILY_INFLATION_RATE;
            ticksUntilInflation += GameWorld.TICKS_IN_DAY;
        }
    }

    public int getAverageWealth() {
        return (int) (Math.round(averageWealth));
    }

    public boolean populationWillPayForTicket() {
        ZooStats zooStats = GameWorld.getInstance().getZooStats();
        int successes = (int) (71000 * zooStats.getPopularity() + 2000);
        int failures = (int) ((730 - 730 * zooStats.getPopularity()) * (zooStats.getTicketPrice() * zooStats.getTicketPrice() / 20));
        return RandomUtils.flipCoin(successes, failures);
    }
}
