package com.gitlab.tttppp.dinozoo.gameworld;

import com.gitlab.tttppp.dinozoo.utils.StringUtils;

import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;

public enum GroundSubType implements ObjectType {
    EMPTY(GroundType.GRASS, 1), DIRT(GroundType.DIRT), FOOTPRINTS(GroundType.FOOTPRINTS), PATH(GroundType.PATH, 1),
    WATER(GroundType.WATER, 5), SAND(GroundType.SAND, 2), LIGHT_GRASS(GroundType.GRASS, 2),
    BOG(GroundType.BOG, 2), PLANKS(GroundType.PATH, 5), MARBLE(GroundType.PATH, 12),
    SNOW(GroundType.SNOW, 40), WET_SAND(GroundType.SAND, 6), RYEGRASS(GroundType.GRASS, 9),
    BLOCKS(GroundType.PATH, 7), CLAY(GroundType.DIRT, 5), TROPICAL_SAND(GroundType.SAND, 21),
    WATER_1(GroundType.WATER), WATER_2(GroundType.WATER), WATER_3(GroundType.WATER), WATER_4(GroundType.WATER);

    public static final List<GroundSubType> WATER_DEPTHS = asList(GroundSubType.WATER, GroundSubType.WATER_1, GroundSubType.WATER_2, GroundSubType.WATER_3, GroundSubType.WATER_4);

    private GroundType groundType;
    private Integer price = null;
    private String displayString;
    /** Used for water to store the depth of the water, but could be used for e.g. depth of jungle as well. */
    private int depth = 0;

    GroundSubType(GroundType groundType) {
        this(groundType, null);
    }

    GroundSubType(GroundType groundType, Integer price) {
        this.groundType = groundType;
        this.price = price;
        String name = this.name();
        this.displayString = StringUtils.makeReadableStringFromEnum(name);
        if (name.endsWith("1")) {
            depth = 1;
        } else if (name.endsWith("2")) {
            depth = 2;
        } else if (name.endsWith("3")) {
            depth = 3;
        } else if (name.endsWith("4")) {
            depth = 4;
        }
    }

    public static GroundSubType[] allPurchasable() {
        List<GroundSubType> purchasable = new ArrayList<>();
        for (GroundSubType groundSubType : values()) {
            if (groundSubType.price != null) {
                purchasable.add(groundSubType);
            }
        }
        return purchasable.toArray(new GroundSubType[purchasable.size()]);
    }

    @Override
    public ObjectType getForeground() {
        return null;
    }

    @Override
    public Integer getPrice() {
        return price;
    }

    public boolean isEdible() {
        return groundType.isEdible();
    }

    public GroundType getGroundType() {
        return groundType;
    }

    @Override
    public String getDisplayString() {
        return displayString;
    }

    public int getDepth() {
        return depth;
    }
}
