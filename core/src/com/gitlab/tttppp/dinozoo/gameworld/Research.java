package com.gitlab.tttppp.dinozoo.gameworld;

import com.gitlab.tttppp.dinozoo.screens.GameScreen;
import com.gitlab.tttppp.dinozoo.screens.SubMenu;
import com.gitlab.tttppp.dinozoo.utils.Log;
import com.google.common.collect.Sets;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.gitlab.tttppp.dinozoo.gameworld.BuildingType.BURGER_BAR;
import static com.gitlab.tttppp.dinozoo.gameworld.BuildingType.SOFT_DRINKS;
import static com.gitlab.tttppp.dinozoo.gameworld.DinosaurType.ANKYLOSAURUS;
import static com.gitlab.tttppp.dinozoo.gameworld.DinosaurType.COMPSOGNATHUS;
import static com.gitlab.tttppp.dinozoo.gameworld.DinosaurType.LEAELLYNASAURA;
import static com.gitlab.tttppp.dinozoo.gameworld.DinosaurType.PROTOCERATOPS;
import static com.gitlab.tttppp.dinozoo.gameworld.GameWorld.TICKS_IN_DAY;
import static com.gitlab.tttppp.dinozoo.gameworld.GroundSubType.EMPTY;
import static com.gitlab.tttppp.dinozoo.gameworld.GroundSubType.PATH;
import static com.gitlab.tttppp.dinozoo.gameworld.IconType.ENTRY_MINUS;
import static com.gitlab.tttppp.dinozoo.gameworld.IconType.ENTRY_PLUS;
import static com.gitlab.tttppp.dinozoo.gameworld.IconType.PLAY_PAUSE;
import static com.gitlab.tttppp.dinozoo.gameworld.IconType.RESEARCH_MINUS;
import static com.gitlab.tttppp.dinozoo.gameworld.IconType.RESEARCH_PLUS;
import static com.gitlab.tttppp.dinozoo.gameworld.IconType.SAVE_1;
import static com.gitlab.tttppp.dinozoo.gameworld.IconType.SAVE_2;
import static com.gitlab.tttppp.dinozoo.gameworld.IconType.SAVE_3;
import static com.gitlab.tttppp.dinozoo.gameworld.IconType.SCROLL_LEFT;
import static com.gitlab.tttppp.dinozoo.gameworld.IconType.SCROLL_RIGHT;
import static com.gitlab.tttppp.dinozoo.gameworld.ItemType.BULLDOZER;
import static com.gitlab.tttppp.dinozoo.gameworld.ItemType.NEST;
import static com.gitlab.tttppp.dinozoo.gameworld.PlantType.TREE;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.BRICK_EW;
import static com.gitlab.tttppp.dinozoo.gameworld.WallType.CHAINLINK_EW;
import static java.util.Arrays.asList;

public class Research {
    private static final int INITIAL_RESEARCH_AMOUNT = 5;
    private static final int INITIAL_RESEARCH_NEEDED = 10 * TICKS_IN_DAY * INITIAL_RESEARCH_AMOUNT;
    private static final int RESEARCH_NEEDED_INCREMENT = TICKS_IN_DAY * INITIAL_RESEARCH_AMOUNT;
    public static final List<? extends ObjectType> ALL_RESEARCH = orderAllResearch();

    private List<? extends ObjectType> toResearch;
    private Set<ObjectType> researched;
    private int ticksUntilResearch = TICKS_IN_DAY;
    private long totalResearch = 0;
    private long researchNeeded = INITIAL_RESEARCH_NEEDED;
    private int researchAmount = INITIAL_RESEARCH_AMOUNT;
    private Set<ObjectType> newResearch = new HashSet<>();

    /**
     * Create an ordered list of all research items. Items from each submenu are added at a rate proportional to the
     * number of items in the submenu. The aim is to mix items from each submenu as much as possible. Note that some
     * items from this will be removed from toResearch as they are given to the player at the start of the game.
     */
    private static List<? extends ObjectType> orderAllResearch() {
        List<String> objectTypes = asList(GroundType.class.getSimpleName(), WallType.class.getSimpleName(), PlantType.class.getSimpleName(),
                DinosaurType.class.getSimpleName(), BuildingType.class.getSimpleName(), StaffType.class.getSimpleName());
        Map<String, ObjectType[]> researchItems = new HashMap<>();
        researchItems.put(GroundType.class.getSimpleName(), GroundSubType.allPurchasable());
        researchItems.put(WallType.class.getSimpleName(), WallType.allPurchasable());
        researchItems.put(PlantType.class.getSimpleName(), PlantType.allPurchasable());
        researchItems.put(DinosaurType.class.getSimpleName(), DinosaurType.values());
        researchItems.put(BuildingType.class.getSimpleName(), BuildingType.allPurchasable());
        researchItems.put(StaffType.class.getSimpleName(), StaffType.values());
        Map<String, Integer> itemNumber = new HashMap<>();
        for (String objectType : objectTypes) {
            itemNumber.put(objectType, 0);
        }
        List<ObjectType> orderedResearch = new ArrayList<>();
        while (!itemNumber.isEmpty()) {
            // Find priority item
            ObjectType lowestOrderItem = null;
            double lowestScore = 1.0;
            String lowestObjectType = null;
            for (String objectType : objectTypes) {
                if (!itemNumber.containsKey(objectType)) {
                    continue;
                }
                Integer i = itemNumber.get(objectType);
                double score = (i + 1.0) / (researchItems.get(objectType).length + 1);
                if (score < lowestScore) {
                    lowestScore = score;
                    lowestOrderItem = researchItems.get(objectType)[i];
                    lowestObjectType = objectType;
                }
            }
            // Add to orderedResearch
            orderedResearch.add(lowestOrderItem);
            // Update associated itemNumber (remove if all items added).
            int nextItemNumber = itemNumber.get(lowestObjectType) + 1;
            if (nextItemNumber < researchItems.get(lowestObjectType).length) {
                itemNumber.put(lowestObjectType, nextItemNumber);
            } else {
                itemNumber.remove(lowestObjectType);
            }
        }
        return orderedResearch;
    }

    public Research() {
        researched = Sets.<ObjectType>newHashSet(
                NEST, BULLDOZER,
                EMPTY, PATH,
                BRICK_EW, CHAINLINK_EW,
                TREE,
                PROTOCERATOPS, LEAELLYNASAURA, ANKYLOSAURUS, COMPSOGNATHUS,
                BURGER_BAR, SOFT_DRINKS,
                ENTRY_MINUS, ENTRY_PLUS, RESEARCH_MINUS, RESEARCH_PLUS,
                SAVE_1, SAVE_2, SAVE_3, PLAY_PAUSE, SCROLL_LEFT, SCROLL_RIGHT);
        refreshToResearch();
    }

    public void tick() {
        ticksUntilResearch--;
        if (ticksUntilResearch <= 0) {
            if (GameWorld.getInstance().getZooStats().spendMoney(researchAmount)) {
                // If nothing left to research then might spend money for nothing.
                if (!toResearch.isEmpty()) {
                    totalResearch += researchAmount;
                    if (totalResearch > researchNeeded) {
                        ObjectType justResearched = toResearch.remove(0);
                        Log.log("Research has unlocked " + justResearched);
                        researched.add(justResearched);
                        newResearch.add(justResearched);
                        // Recreate the submenu to allow clicking the button immediately.
                        SubMenu subMenu = ((GameScreen) GameWorld.getInstance().getGame().getScreen()).getGameRenderer().getSubMenu();
                        subMenu.refresh();
                        // Set the next research target.
                        totalResearch = 0;
                        researchNeeded += RESEARCH_NEEDED_INCREMENT;
                    }
                }
            }
            ticksUntilResearch += TICKS_IN_DAY;
        }
    }

    public int getResearchAmount() {
        return researchAmount;
    }

    public void setResearchAmount(int researchAmount) {
        this.researchAmount = researchAmount;
    }

    public boolean isResearched(ObjectType objectType) {
        return researched.contains(objectType);
    }

    public boolean isNewResearch(ObjectType objectType) {
        return newResearch.contains(objectType);
    }

    public void seenResearch(ObjectType objectType) {
        newResearch.remove(objectType);
    }

    @Override
    public String toString() {
        return toResearch.toString();
    }

    /** Rebuild the list of things to research by using the list of things that have already been researched. */
    public void refreshToResearch() {
        toResearch = new ArrayList<>(ALL_RESEARCH);
        for (ObjectType objectType : researched) {
            toResearch.remove(objectType);
        }
    }
}
