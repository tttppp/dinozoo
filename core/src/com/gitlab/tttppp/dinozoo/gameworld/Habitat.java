package com.gitlab.tttppp.dinozoo.gameworld;

import com.google.common.collect.ImmutableMap;

import java.util.Map;

import static com.gitlab.tttppp.dinozoo.gameworld.GroundType.BOG;
import static com.gitlab.tttppp.dinozoo.gameworld.GroundType.DIRT;
import static com.gitlab.tttppp.dinozoo.gameworld.GroundType.GRASS;
import static com.gitlab.tttppp.dinozoo.gameworld.GroundType.FOOTPRINTS;
import static com.gitlab.tttppp.dinozoo.gameworld.GroundType.PATH;
import static com.gitlab.tttppp.dinozoo.gameworld.GroundType.SAND;
import static com.gitlab.tttppp.dinozoo.gameworld.GroundType.SNOW;
import static com.gitlab.tttppp.dinozoo.gameworld.GroundType.WATER;

public enum Habitat {
    /** This is primarily for guests. */
    DOMESTIC(ImmutableMap.<GroundType, Integer>builder().put(PATH, 100000).put(GRASS, 100).put(DIRT, 50)
            .put(FOOTPRINTS, 20).put(WATER, 1).put(SAND, 90).put(BOG, 2).put(SNOW, 2).build()),

    MOUNTAIN(ImmutableMap.<GroundType, Integer>builder().put(GRASS, 1000).put(DIRT, 1000).put(FOOTPRINTS, 1000)
            .put(PATH, 2000).put(WATER, 1).put(SAND, 90).put(BOG, 2).put(SNOW, 500).build()),
    LOWLAND(ImmutableMap.<GroundType, Integer>builder().put(GRASS, 2000).put(DIRT, 1000).put(FOOTPRINTS, 1000)
            .put(PATH, 1000).put(WATER, 1).put(SAND, 1000).put(BOG, 1000).put(SNOW, 50).build()),
    FOREST(ImmutableMap.<GroundType, Integer>builder().put(GRASS, 1000).put(DIRT, 1000).put(FOOTPRINTS, 1000)
            .put(PATH, 1000).put(WATER, 1).put(SAND, 500).put(BOG, 500).put(SNOW, 20).build()),
    SWAMP(ImmutableMap.<GroundType, Integer>builder().put(GRASS, 100).put(DIRT, 100).put(FOOTPRINTS, 100)
            .put(PATH, 1).put(WATER, 10).put(SAND, 90).put(BOG, 2000).put(SNOW, 100).build()),
    SEA(ImmutableMap.<GroundType, Integer>builder().put(GRASS, 1).put(DIRT, 1).put(FOOTPRINTS, 1)
            .put(PATH, 1).put(WATER, 10000).put(SAND, 2).put(BOG, 10).put(SNOW, 1).build()),
    AIR(ImmutableMap.<GroundType, Integer>builder().put(GRASS, 1000).put(DIRT, 1000).put(FOOTPRINTS, 100)
            .put(PATH, 100).put(WATER, 1).put(SAND, 1000).put(BOG, 100).put(SNOW, 100).build()),
    ICE(ImmutableMap.<GroundType, Integer>builder().put(GRASS, 1000).put(DIRT, 1500).put(FOOTPRINTS, 1000)
            .put(PATH, 1500).put(WATER, 1).put(SAND, 100).put(BOG, 500).put(SNOW, 2000).build()),
    JUNGLE(ImmutableMap.<GroundType, Integer>builder().put(GRASS, 2000).put(DIRT, 1000).put(FOOTPRINTS, 2000)
            .put(PATH, 100).put(WATER, 2).put(SAND, 50).put(BOG, 1000).put(SNOW, 1).build()),
    DESERT(ImmutableMap.<GroundType, Integer>builder().put(GRASS, 1000).put(DIRT, 2000).put(FOOTPRINTS, 1000)
            .put(PATH, 2000).put(WATER, 1).put(SAND, 10000).put(BOG, 500).put(SNOW, 1).build());

    private Map<GroundType, Integer> groundTypeBonus;

    Habitat(Map<GroundType, Integer> groundTypeBonus) {
        this.groundTypeBonus = groundTypeBonus;
    }

    public Map<GroundType, Integer> getGroundTypeBonus() {
        return groundTypeBonus;
    }
}
