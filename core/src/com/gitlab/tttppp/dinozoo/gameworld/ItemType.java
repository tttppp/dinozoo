package com.gitlab.tttppp.dinozoo.gameworld;

import com.gitlab.tttppp.dinozoo.gameobject.GameObject;
import com.gitlab.tttppp.dinozoo.utils.StringUtils;

public enum ItemType implements ObjectType {
    NEST_FOREGROUND, NEST(1000, NEST_FOREGROUND), BULLDOZER(1), SKELETON(true), SPLAT_EGG;

    private ItemType foreground;
    private Integer price = null;
    private String displayString;
    private boolean stackable = false;

    ItemType() {
        this.displayString = StringUtils.makeReadableStringFromEnum(this.name());
    }

    ItemType(boolean stackable) {
        this.stackable = stackable;
    }

    ItemType(int price) {
        this.price = price;
        this.displayString = StringUtils.makeReadableStringFromEnum(this.name());
    }

    ItemType(int price, ItemType foreground) {
        this.price = price;
        this.foreground = foreground;
        this.displayString = StringUtils.makeReadableStringFromEnum(this.name());
    }

    @Override
    public ObjectType getForeground() {
        return foreground;
    }

    @Override
    public Integer getPrice() {
        int premium = 0;
        // Each nest costs 1000 more than the previous one (mainly to try to avoid exponential explosion of the dinosaur population).
        if (this == NEST) {
            for (GameObject gameObject : GameWorld.getInstance().getFixedObjects().values()) {
                if (gameObject.getObjectType() == NEST) {
                    premium += 1000;
                }
            }
        }
        return price + premium;
    }

    @Override
    public String getDisplayString() {
        return displayString;
    }

    public boolean isStackable() {
        return stackable;
    }
}
