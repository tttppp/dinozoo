package com.gitlab.tttppp.dinozoo.gameworld;

public enum IconType implements ObjectType {
    COIN, ENTRY_MINUS, ENTRY_PLUS, RESEARCH, RESEARCH_MINUS, RESEARCH_PLUS, SAVE_1, SAVE_2, SAVE_3, LOAD, SETTINGS,
    NEW_RESEARCH, SAD_FACE, NEUTRAL_FACE, SATISFIED_FACE, HAPPY_FACE, ECSTATIC_FACE, POPULARITY, PLAY_PAUSE,
    SCROLL_LEFT, SCROLL_RIGHT;

    @Override
    public ObjectType getForeground() {
        return null;
    }

    @Override
    public Integer getPrice() {
        return null;
    }

    @Override
    public String getDisplayString() {
        // Never displayed
        return "";
    }
}
