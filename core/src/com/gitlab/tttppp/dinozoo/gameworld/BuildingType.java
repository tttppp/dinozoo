package com.gitlab.tttppp.dinozoo.gameworld;

import com.gitlab.tttppp.dinozoo.utils.StringUtils;

import java.util.ArrayList;
import java.util.List;

public enum BuildingType implements ObjectType {
    BURGER_BAR(1500), SOLID_WALL, ENTRY_NS(5000, "Entry (N/S)"), ENTRY_EW(5000, "Entry (E/W)"), SOFT_DRINKS(1000),
    SURVEY_POST(1000), STAFF_ROOM(2000);

    private Integer price = null;
    private String displayString;

    BuildingType() {
        this.displayString = StringUtils.makeReadableStringFromEnum(this.name());
    }

    BuildingType(int price) {
        this.price = price;
        this.displayString = StringUtils.makeReadableStringFromEnum(this.name());
    }

    BuildingType(int price, String displayString) {
        this.price = price;
        this.displayString = displayString;
    }

    public static BuildingType[] allPurchasable() {
        List<BuildingType> purchasable = new ArrayList<>();
        for (BuildingType buildingType : values()) {
            if (buildingType.price != null) {
                purchasable.add(buildingType);
            }
        }
        return purchasable.toArray(new BuildingType[purchasable.size()]);
    }

    @Override
    public ObjectType getForeground() {
        return null;
    }

    @Override
    public Integer getPrice() {
        return price;
    }

    @Override
    public String getDisplayString() {
        return displayString;
    }
}
