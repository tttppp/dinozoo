package com.gitlab.tttppp.dinozoo.gameworld;

import com.gitlab.tttppp.dinozoo.gameobject.behaviour.Diet;
import com.gitlab.tttppp.dinozoo.utils.Log;
import com.gitlab.tttppp.dinozoo.utils.StringUtils;

import java.util.HashMap;
import java.util.Map;

import static com.gitlab.tttppp.dinozoo.gameobject.behaviour.Diet.CARNIVORE;
import static com.gitlab.tttppp.dinozoo.gameobject.behaviour.Diet.HERBIVORE;
import static com.gitlab.tttppp.dinozoo.gameobject.behaviour.Diet.INSECTIVORE;
import static com.gitlab.tttppp.dinozoo.gameobject.behaviour.Diet.OMNIVORE;
import static com.gitlab.tttppp.dinozoo.gameobject.behaviour.Diet.PESCATARIAN;
import static com.gitlab.tttppp.dinozoo.gameworld.GameWorld.TICKS_IN_DAY;
import static com.gitlab.tttppp.dinozoo.gameworld.Habitat.DESERT;
import static com.gitlab.tttppp.dinozoo.gameworld.Habitat.FOREST;
import static com.gitlab.tttppp.dinozoo.gameworld.Habitat.ICE;
import static com.gitlab.tttppp.dinozoo.gameworld.Habitat.JUNGLE;
import static com.gitlab.tttppp.dinozoo.gameworld.Habitat.LOWLAND;
import static com.gitlab.tttppp.dinozoo.gameworld.Habitat.MOUNTAIN;
import static com.gitlab.tttppp.dinozoo.gameworld.Habitat.SEA;
import static com.gitlab.tttppp.dinozoo.gameworld.Habitat.SWAMP;

public enum DinosaurType implements ObjectType {
    /** Similar to a sheep. */
    PROTOCERATOPS(600, 83, 60, 20, HERBIVORE, 3, 3, LOWLAND, 5 * 30, 6 * 30, 11 * 365, 4),
    /** Similar to a king penguin. */
    LEAELLYNASAURA(650, 17, 90, 14, HERBIVORE, 1, 1, ICE, 55, 3 * 365, 41 * 365, 2),
    /** Similar to a ten times bigger giant armadillo. */
    ANKYLOSAURUS(1400, 54000, 300, 9, OMNIVORE, 7, 10, JUNGLE, 122, 3 * 365, 15 * 365, 4),
    /** Similar to a turkey. */
    COMPSOGNATHUS(700, 3, 25, 7, CARNIVORE, 1, 1, MOUNTAIN, 28, 6 * 30, 5 * 365, 2),
    /** Similar to a blue whale. */
    DIPLODOCUS(8900, 113000, 1100, 16, HERBIVORE, 6, 2, FOREST, 12 * 30, 8 * 365, 90 * 365, 3),
    /** Similar to a kangaroo. */
    PARASAUROLOPHUS(1200, 3300, 490, 8, HERBIVORE, 2, 3, DESERT, 34, 32 * 30, 22 * 365, 2),
    /** Similar to a lion. */
    TYRANOSAURUS_REX(7600, 8800, 400, 10, CARNIVORE, 10, 3, SWAMP, 110, 3 * 365, 12 * 365, 2),
    /** Similar to a three times bigger saltwater crocodile. */
    SPINOSAURUS(3400, 11000, 450, 9, CARNIVORE, 10, 3, SWAMP, 80, 11 * 365, 70 * 365, 2),
    /** Similar to a flathead grey mullet. Nb Real fish lay loads of eggs far more frequently than would make sense for the game. */
    ASTRASPIS(2600, 8, 75, 5, PESCATARIAN, 1, 4, SEA, 37, 2 * 365, 25 * 365, 1),
    /** Similar to a hippo. */
    STEGOSAURUS(4000, 6000, 370, 21, HERBIVORE, 4, 4, FOREST, 243, 5 * 365, 45 * 365, 4),
    /** Similar to a rhino. */
    TRICERATOPS(14500, 6000, 300, 5, HERBIVORE, 7, 5, LOWLAND, 16 * 30, 5 * 365, 50 * 365, 4),
    /** Similar to a double otter. */
    PROTEROSUCHUS(5400, 24, 30, 23, CARNIVORE, 4, 2, LOWLAND, 64, 3 * 365, 20 * 365, 4),
    /** Similar to a heron. */
    TANYSTROPHEUS(8400, 140, 300, 32, CARNIVORE, 3, 1, SWAMP, 35, 22 * 30, 15 * 365, 4),
    /** Similar to a warthog. */
    LYSTROSAURUS(7500, 90, 100, 41, HERBIVORE, 4, 6, DESERT, 183, 4 * 365, 11 * 365, 4),
    /** Similar to a humpback whale. */
    BASILOSAURUS(8600, 60000, 500, 74, PESCATARIAN, 6, 9, SEA, 11 * 30, 8 * 365, 48 * 365, 1),
    /** Similar to a donkey. */
    MAIASAURA(6511, 10000, 250, 8, HERBIVORE, 2, 4, FOREST, 14 * 30, 2 * 365, 28 * 365, 4),
    /** Similar to a ferret. */
    LONGISQUAMA(2300, 1, 20, 6, INSECTIVORE, 2, 1, FOREST, 42, 4 * 30, 6 * 365, 4),
    /** Similar to an ox. */
    HUAYANGOSAURUS(4900, 3000, 450, 18, HERBIVORE, 7, 9, LOWLAND, 283, 2 * 365, 22 * 365, 4),
    /** Similar to a gharial. */
    PLESIOSAURUS(8100, 450, 350, 7, PESCATARIAN, 1, 3, SEA, 70, 18 * 365, 55 * 365, 1),
    /** Similar to an ostrich. */
    THECONTASAURUS(8300, 11, 200, 9, OMNIVORE, 3, 2, MOUNTAIN, 42, 3 * 365, 45 * 265, 2),
    /** Similar to a gray fox. */
    HERRARASAURUS(4300, 350, 110, 7, CARNIVORE, 3, 1, LOWLAND, 61, 10 * 30, 7 * 365, 2),
    /** Similar to a goat. */
    STEGACERAS(16200, 40, 250, 5, HERBIVORE, 5, 6, MOUNTAIN, 150, 5 * 30, 18 * 365, 2);

    /** Price of the dinosaur egg. */
    private int price;
    /** Weight in kg. */
    private int weight;
    /** Height in cm. */
    private int height;
    /** Inverse speed (human = 10). */
    private int ticksPerStep;
    /** What it ate. */
    private Diet diet;
    /** On a scale of 0-10. */
    private int strength;
    /** On a scale of 0-10. */
    private int defence;
    /** Where it lived. */
    private Habitat habitat;
    /** Time for egg to hatch. */
    private int eggTime;
    /** The ticks this spends as a child. */
    private long ticksAsChild;
    /** Ticks from hatching to death. */
    private long lifeTime;
    /** How many legs the dinosaur usually walks on (e.g. could be 3 as an average of 2 and 4). For fish this should be set to 1. */
    private int walkingLegs;
    /** How much the dinosaur likes different surfaces to walk on. */
    private Map<GroundType, Integer> groundPreferences;
    /** How much pressure the dinosaur exerts through a foot. */
    private double pressure;
    /** Human readable name. */
    private String displayString;
    /** A map containing bonuses for certain water depths. */
    private Map<GroundSubType, Integer> waterDepthBonus = new HashMap<>();

    DinosaurType(int price, int weight, int height, int ticksPerStep, Diet diet, int strength, int defence, Habitat habitat, int eggTime, int childTime, int lifeTime, int walkingLegs) {
        this.price = price;
        this.weight = weight;
        this.height = height;
        this.ticksPerStep = ticksPerStep;
        this.diet = diet;
        this.strength = strength;
        this.defence = defence;
        this.habitat = habitat;
        this.eggTime = eggTime;
        this.ticksAsChild = childTime * TICKS_IN_DAY;
        this.lifeTime = lifeTime * TICKS_IN_DAY;
        this.walkingLegs = walkingLegs;

        this.pressure = weight * 1.0 / walkingLegs;

        populateGroundPreferences();

        this.displayString = StringUtils.makeReadableStringFromEnum(this.name());
    }

    public int getWeight() {
        return weight;
    }

    public int getHeight() {
        return height;
    }

    public int getTicksPerStep() {
        return ticksPerStep;
    }

    public Diet getDiet() {
        return diet;
    }

    public int getStrength() {
        return strength;
    }

    public int getDefence() {
        return defence;
    }

    public Habitat getHabitat() {
        return habitat;
    }

    public int getEggTime() {
        return eggTime;
    }

    public long getTicksAsChild() {
        return ticksAsChild;
    }

    public long getLifeTime() {
        return lifeTime;
    }

    public Map<GroundType, Integer> getGroundPreferences() {
        return groundPreferences;
    }

    private void populateGroundPreferences() {
        groundPreferences = habitat.getGroundTypeBonus();

        // Set the preferred water depth for sea creatures based on their weight.
        if (habitat == SEA) {
            double preferredDepth = Math.min(Math.max(((Math.log(weight + 1)) / 3), 0), GroundSubType.WATER_DEPTHS.size());
            for (GroundSubType groundSubType : GroundSubType.WATER_DEPTHS) {
                double power = 4 * (4 - Math.abs(groundSubType.getDepth() - preferredDepth));
                waterDepthBonus.put(groundSubType, (int) Math.pow(3, power));
            }
        }
    }

    public int getWaterDepthBonus(GroundSubType waterType) {
        if (!waterDepthBonus.containsKey(waterType)) {
            return 0;
        }
        return waterDepthBonus.get(waterType);
    }

    public int getWalkingLegs() {
        return walkingLegs;
    }

    public double getPressure() {
        return pressure;
    }

    @Override
    public ObjectType getForeground() {
        return null;
    }

    @Override
    public Integer getPrice() {
        return price;
    }

    @Override
    public String getDisplayString() {
        return displayString;
    }
}
