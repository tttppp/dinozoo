package com.gitlab.tttppp.dinozoo.gameworld;

import com.gitlab.tttppp.dinozoo.utils.StringUtils;

public enum StaffType implements ObjectType {
    CLEANER(75);

    /** Monthly wage. */
    private Integer wage;
    private String displayString;

    StaffType(int wage) {
        this.wage = wage;
        this.displayString = StringUtils.makeReadableStringFromEnum(this.name());
    }

    @Override
    public ObjectType getForeground() {
        return null;
    }

    @Override
    public Integer getPrice() {
        return wage;
    }

    public Integer getWage() {
        return wage;
    }

    @Override
    public String getDisplayString() {
        return displayString;
    }
}
