package com.gitlab.tttppp.dinozoo.gameworld;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Screen;
import com.gitlab.tttppp.dinozoo.gameobject.Building;
import com.gitlab.tttppp.dinozoo.gameobject.GameObject;
import com.gitlab.tttppp.dinozoo.gameobject.Land;
import com.gitlab.tttppp.dinozoo.gamerenderer.GameRenderer;
import com.gitlab.tttppp.dinozoo.screens.GameScreen;
import com.gitlab.tttppp.dinozoo.screens.SubMenuType;
import com.gitlab.tttppp.dinozoo.utils.Log;
import com.gitlab.tttppp.dinozoo.utils.Pair;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static com.gitlab.tttppp.dinozoo.gamerenderer.GameRenderer.SCALE;
import static com.gitlab.tttppp.dinozoo.gamerenderer.GameRenderer.VISIBLE_TILES_HEIGHT;
import static com.gitlab.tttppp.dinozoo.gamerenderer.GameRenderer.VISIBLE_TILES_WIDTH;
import static com.gitlab.tttppp.dinozoo.gameworld.BuildingType.ENTRY_NS;
import static com.gitlab.tttppp.dinozoo.gameworld.BuildingType.SOLID_WALL;
import static com.gitlab.tttppp.dinozoo.gameworld.GroundSubType.EMPTY;
import static com.gitlab.tttppp.dinozoo.gameworld.GroundSubType.PATH;
import static com.gitlab.tttppp.dinozoo.gameworld.ItemType.NEST;

public class GameWorld {
    public static final int GAME_VERSION = 16;
    public static final int MAP_WIDTH = 100;
    public static final int MAP_HEIGHT = 100;
    public static final int VISIBLE_MAP_WIDTH = GameRenderer.VIEWPORT_WIDTH / SCALE;
    public static final int VISIBLE_MAP_HEIGHT = GameRenderer.VIEWPORT_HEIGHT / SCALE;
    public static final int MAX_VIEW_X = MAP_WIDTH - VISIBLE_MAP_WIDTH;
    public static final int MAX_VIEW_Y = MAP_HEIGHT - VISIBLE_MAP_HEIGHT;
    /** The tick length in seconds. */
    public static final double TICK_LENGTH = 0.04;
    /** The number of seconds in a day. */
    public static final double DAY_LENGTH_IN_SECONDS = 0.6;
    /** Each day is a second. */
    public static final int TICKS_IN_DAY = (int) Math.round(DAY_LENGTH_IN_SECONDS / TICK_LENGTH);
    /** There are no leap years. */
    public static final int TICKS_IN_YEAR = (int) Math.round(365.0 * DAY_LENGTH_IN_SECONDS / TICK_LENGTH);
    /** Number of idle ticks before the game will pause itself. */
    private static final int INITIAL_SCREEN_SAVER_TIME = GameWorld.TICKS_IN_YEAR;
    private static GameWorld instance;

    private transient Game game;
    private Integer gameVersion = GAME_VERSION;
    private ZooStats zooStats = new ZooStats();
    private Research research = new Research();
    private Population population = new Population();
    private Map<Position, Land> ground = new HashMap<>();
    private List<Pair<Position, Land>> groundUpdates = new ArrayList<>();
    /** There can only be one fixed object per tile (e.g. buildings). */
    private Map<Position, GameObject> fixedObjects = new HashMap<>();
    /** There can be many stackable objects per tile (e.g. guests). */
    private Multimap<Position, GameObject> stackableObjects = HashMultimap.create();
    private boolean removeGameObjectThisTick;
    private List<Pair<Position, GameObject>> gameObjectAdditions = new ArrayList<>();
    /** TODO Requires equals to be implemented on all GameObjects. */
    private List<Pair<Position, GameObject>> gameObjectRemovals = new ArrayList<>();
    private List<Building> exits = new ArrayList<>();
    private double runTime = 0;
    private long tick;
    /** Number of ticks before the game will pause itself. */
    private int screenSaverTimer = INITIAL_SCREEN_SAVER_TIME;
    private int viewX = 38;
    private int viewY = 84;

    /** Private constructor for singleton. */
    private GameWorld() {
        // Shared wall object (hopefully null position causes no issues).
        Building wall = new Building(SOLID_WALL, null);
        for (int x = 0; x < MAP_WIDTH; x++) {
            fixedObjects.put(Position.of(x, 0), wall);
            Position position = Position.of(x, MAP_HEIGHT - 1);
            if (x == MAP_WIDTH / 2) {
                // Initial entry/exit point for guests.
                Building entry = new Building(ENTRY_NS, position);
                exits.add(entry);
                fixedObjects.put(position, entry);
            } else {
                fixedObjects.put(position, wall);
            }
        }
        for (int y = 1; y < MAP_HEIGHT - 1; y++) {
            fixedObjects.put(Position.of(0, y), wall);
            fixedObjects.put(Position.of(MAP_WIDTH - 1, y), wall);
        }
        // Initial bit of path.
        for (int y = 1; y <= 4; y++) {
            Position position = Position.of(MAP_WIDTH / 2, MAP_HEIGHT - y);
            Land path = new Land(PATH, position);
            ground.put(position, path);
        }
    }

    public static synchronized GameWorld getInstance() {
        if (instance == null) {
            instance = new GameWorld();
        }
        return instance;
    }

    public void update(float delta) {
        while ((runTime % TICK_LENGTH) + delta > TICK_LENGTH) {
            delta--;
            runTime++;
            tick = (long) (runTime / TICK_LENGTH);

            updateGround();
            updateObjects(tick);
            research.tick();
            population.tick();
            screenSaverTimer--;
        }
        processUserUpdates();

        // Potentially pause the game if there has been no user interaction for a long time.
        if (screenSaverTimer <= 0) {
            Screen screen = game.getScreen();
            if (screen instanceof GameScreen) {
                GameScreen gameScreen = (GameScreen) screen;
                if (!gameScreen.isPaused()) {
                    gameScreen.pause();
                    gameScreen.getGameRenderer().getSubMenu().setSelected(SubMenuType.PREFERENCES, 0);
                }
            }
        }

        sleep(TICK_LENGTH - delta);
        runTime += delta;
    }

    private void sleep(double amount) {
        try {
            // Try to reduce CPU usage to avoid overheating on the pi.
            Thread.sleep((long) amount * 100);
        } catch (InterruptedException e) {
        }
    }

    private void updateGround() {
        List<Position> toRemove = new ArrayList<>();
        for (Map.Entry<Position, Land> entry : ground.entrySet()) {
            Position position = entry.getKey();
            Land land = entry.getValue();
            boolean remove = land.tick();
            if (remove) {
                toRemove.add(position);
            }
        }
        for (Position position : toRemove) {
            ground.remove(position);
        }
    }

    private void updateObjects(long tick) {
        for (Position position : getAllPositions()) {
            Iterator<GameObject> iterator = getGameObjectsAt(position).iterator();
            while (iterator.hasNext()) {
                GameObject gameObject = iterator.next();
                gameObject.tick(tick);
                if (removeGameObjectThisTick) {
                    gameObjectRemovals.add(new Pair<>(position, gameObject));
                    removeGameObjectThisTick = false;
                }
            }
        }
    }

    private void processUserUpdates() {
        Iterator<Pair<Position, Land>> iterator = groundUpdates.iterator();
        while (iterator.hasNext()) {
            Pair<Position, Land> next = iterator.next();
            if (next.getRight().getGroundType() == EMPTY) {
                ground.remove(next.getLeft());
            } else {
                ground.put(next.getLeft(), next.getRight());
            }
            iterator.remove();
        }
        Iterator<Pair<Position, GameObject>> removalIterator = gameObjectRemovals.iterator();
        while (removalIterator.hasNext()) {
            Pair<Position, GameObject> next = removalIterator.next();
            stackableObjects.remove(next.getLeft(), next.getRight());
            removalIterator.remove();
        }
        Iterator<Pair<Position, GameObject>> additionIterator = gameObjectAdditions.iterator();
        while (additionIterator.hasNext()) {
            Pair<Position, GameObject> next = additionIterator.next();
            stackableObjects.put(next.getLeft(), next.getRight());
            additionIterator.remove();
        }
    }

    public Map<Position, Land> getGround() {
        return ground;
    }

    public Map<Position, GameObject> getFixedObjects() {
        return fixedObjects;
    }

    public Multimap<Position, GameObject> getStackableObjects() {
        return stackableObjects;
    }

    public GameObject getFixedObjectAt(Position position) {
        return fixedObjects.get(position);
    }

    public List<GameObject> getGameObjectsAt(Position position) {
        List<GameObject> gameObjects = new ArrayList<>(stackableObjects.get(position));
        GameObject fixedObject = fixedObjects.get(position);
        if (fixedObject != null) {
            gameObjects.add(fixedObject);
        }
        return gameObjects;
    }

    public List<Position> getAllPositions() {
        List<Position> allPositions = new ArrayList<>();
        for (int y = 0; y < MAP_HEIGHT; y++) {
            for (int x = 0; x < MAP_WIDTH; x++) {
                allPositions.add(Position.of(x, y));
            }
        }
        return allPositions;
    }

    public List<Position> getAllVisiblePositions() {
        List<Position> allPositions = new ArrayList<>();
        for (int y = viewY; y < viewY + VISIBLE_TILES_HEIGHT; y++) {
            for (int x = viewX; x < viewX + VISIBLE_TILES_WIDTH; x++) {
                allPositions.add(Position.of(x, y));
            }
        }
        return allPositions;
    }

    public int getViewX() {
        return viewX;
    }

    public void setViewX(int viewX) {
        if (viewX < 0) {
            viewX = 0;
        } else if (viewX > MAX_VIEW_X) {
            viewX = MAX_VIEW_X;
        }
        this.viewX = viewX;
    }

    public int getViewY() {
        return viewY;
    }

    public void setViewY(int viewY) {
        if (viewY < 0) {
            viewY = 0;
        } else if (viewY > MAX_VIEW_Y) {
            viewY = MAX_VIEW_Y;
        }
        this.viewY = viewY;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public Game getGame() {
        return game;
    }

    public void moveObject(GameObject gameObject, Position newPosition) {
        removeCurrentObject();
        gameObject.setPosition(newPosition);
        stackableObjects.put(newPosition, gameObject);
    }

    public void removeCurrentObject() {
        removeGameObjectThisTick = true;
    }

    /**
     * Try to place the land in the given location.
     *
     * @param position The position to put the land.
     * @param land The new land object.
     * @return true if the land was placed successfully.
     */
    public boolean putLand(Position position, Land land) {
        if (getGroundTypeAt(position) == land.getGroundType()) {
            return false;
        }
        groundUpdates.add(new Pair<>(position, land));
        return true;
    }

    public ZooStats getZooStats() {
        return zooStats;
    }

    public long getTick() {
        return tick;
    }

    public List<Building> getExits() {
        return exits;
    }

    public GroundSubType getGroundTypeAt(Position destination) {
        Land land = ground.get(destination);
        if (land == null) {
            return EMPTY;
        }
        return land.getGroundType();
    }

    /** Get the type of ground that the position will become if left alone for long enough. */
    public GroundSubType getEventualGroundTypeAt(Position destination) {
        Land land = ground.get(destination);
        if (land == null) {
            return EMPTY;
        }
        return land.getEventualGroundType();
    }

    /**
     * Try to place an object in the world.
     *
     * @param position The position to place the object.
     * @param gameObject The object to place.
     * @return true if the placement was successful.
     */
    public boolean putObject(Position position, GameObject gameObject) {
        gameObject.setPosition(position);
        if (gameObject.isStackable()) {
            stackableObjects.put(position, gameObject);
        } else {
            boolean needsItemMenuUpdate = (gameObject.getObjectType() == NEST
                    || (fixedObjects.containsKey(position) && fixedObjects.get(position).getObjectType() == NEST));
            GameObject existingObject = getFixedObjectAt(position);
            if (existingObject != null && existingObject.getObjectType() == gameObject.getObjectType()) {
                // Don't place the item.
                Log.log("Not placing " + gameObject + " since the given location already contains " + existingObject);
                return false;
            }
            fixedObjects.put(position, gameObject);
            // We might need to update a submenu.
            if (needsItemMenuUpdate) {
                ((GameScreen) game.getScreen()).getGameRenderer().getSubMenu().createSubMenu(SubMenuType.ITEM);
            }
        }
        return true;
    }

    public void removeObject(GameObject gameObject) {
        Position position = gameObject.getPosition();
        if (gameObject.isStackable()) {
            stackableObjects.remove(position, gameObject);
        } else {
            removeObject(position);
        }
    }

    /** Remove any fixed object at the given location. */
    public void removeObject(Position position) {
        boolean needsItemMenuUpdate = fixedObjects.containsKey(position) && fixedObjects.get(position).getObjectType() == NEST;
        fixedObjects.remove(position);
        // We might need to update a submenu.
        if (needsItemMenuUpdate) {
            ((GameScreen) game.getScreen()).getGameRenderer().getSubMenu().createSubMenu(SubMenuType.ITEM);
        }
    }

    public Population getPopulation() {
        return population;
    }

    public Research getResearch() {
        return research;
    }

    public void setZooStats(ZooStats zooStats) {
        this.zooStats = zooStats;
    }

    public void setResearch(Research research) {
        this.research = research;
    }

    public void setPopulation(Population population) {
        this.population = population;
    }

    public void setGround(Map<Position, Land> ground) {
        this.ground = ground;
    }

    public void setFixedObjects(Map<Position, GameObject> fixedObjects) {
        this.fixedObjects = fixedObjects;
    }

    public void setStackableObjects(Multimap<Position, GameObject> stackableObjects) {
        this.stackableObjects = stackableObjects;
    }

    public Integer getGameVersion() {
        return gameVersion;
    }

    public boolean isPaused() {
        Screen screen = game.getScreen();
        return screen instanceof GameScreen && ((GameScreen) screen).isPaused();
    }

    public void resetScreenSaverTimer() {
        screenSaverTimer = INITIAL_SCREEN_SAVER_TIME;
    }
}
