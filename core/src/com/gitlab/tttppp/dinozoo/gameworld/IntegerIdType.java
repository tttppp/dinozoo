package com.gitlab.tttppp.dinozoo.gameworld;

public class IntegerIdType implements ObjectType {
    private int id;

    public IntegerIdType(int id) {
        this.id = id;
    }

    /** No arg constructor for game serialization. */
    public IntegerIdType() {
    }

    public int getId() {
        return id;
    }

    @Override
    public ObjectType getForeground() {
        return null;
    }

    @Override
    public Integer getPrice() {
        return null;
    }

    @Override
    public String getDisplayString() {
        // Never displayed
        return "";
    }
}
