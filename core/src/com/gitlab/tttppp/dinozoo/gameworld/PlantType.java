package com.gitlab.tttppp.dinozoo.gameworld;

import com.gitlab.tttppp.dinozoo.utils.StringUtils;

import java.util.ArrayList;
import java.util.List;

public enum PlantType implements ObjectType {
    TREE(100, 1200), TREE_BARE(TREE), BUSH(80, 400), BUSH_LEAVES(BUSH), BUSH_BARE(BUSH_LEAVES),
    BEECH(90, 4000), BEECH_BARE(BEECH), OAK(120, 1500), OAK_GROWING(OAK), OAK_BARE(OAK_GROWING),
    HORSES_TAIL(140, 50), HORSES_TAIL_BARE(HORSES_TAIL), BULL_RUSH(60, 200), BULL_RUSH_BARE(BULL_RUSH),
    BULL_RUSH_SHORT(BULL_RUSH_BARE), SILVER_BIRCH(210, 3000), SILVER_BIRCH_BARE(SILVER_BIRCH),
    PALM(320, 800), PALM_BARE(PALM), CACTUS(820, 1200), CACTUS_EATEN(CACTUS), ACACIA(470, 3000), ACACIA_BARE(ACACIA),
    FAN_PALM(370, 1500), FAN_PALM_EATEN(FAN_PALM), GIANT_DRACAENA(770, 1200), GIANT_DRACAENA_GROWING(GIANT_DRACAENA),
    GIANT_DRACAENA_BARE(GIANT_DRACAENA_GROWING);

    private PlantType nextStage = null;
    private PlantType previousStage = null;
    private Integer price = null;
    /** The height of the fully grown plant in cm. */
    private Integer height = null;
    private String displayString;

    PlantType(PlantType nextStage) {
        this.nextStage = nextStage;
        nextStage.previousStage = this;
        this.displayString = StringUtils.makeReadableStringFromEnum(this.name());
        // Set the height to be the same as the next stage (if possible).
        PlantType next = nextStage;
        while (next != null) {
            if (next.height != null) {
                this.height = next.height;
                break;
            }
            next = next.nextStage;
        }
    }

    PlantType(int price, int height) {
        this.price = price;
        this.height = height;
        this.displayString = StringUtils.makeReadableStringFromEnum(this.name());
    }

    public static PlantType[] allPurchasable() {
        List<PlantType> purchasable = new ArrayList<>();
        for (PlantType plantType : values()) {
            if (plantType.price != null) {
                purchasable.add(plantType);
            }
        }
        return purchasable.toArray(new PlantType[purchasable.size()]);
    }

    @Override
    public ObjectType getForeground() {
        return null;
    }

    @Override
    public Integer getPrice() {
        return price;
    }

    public Integer getHeight() {
        return height;
    }

    public PlantType getNextStage() {
        return nextStage;
    }

    @Override
    public String getDisplayString() {
        return displayString;
    }

    public PlantType eatenType() {
        PlantType returnValue = this;
        PlantType previous = previousStage;
        while (previous != null) {
            returnValue = previous;
            previous = previous.previousStage;
        }
        return returnValue;
    }
}
