package com.gitlab.tttppp.dinozoo.gameworld;

import com.gitlab.tttppp.dinozoo.utils.StringUtils;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Sets;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.gitlab.tttppp.dinozoo.gameworld.Direction.DOWN;
import static com.gitlab.tttppp.dinozoo.gameworld.Direction.LEFT;
import static com.gitlab.tttppp.dinozoo.gameworld.Direction.RIGHT;
import static com.gitlab.tttppp.dinozoo.gameworld.Direction.UP;
import static java.util.Arrays.asList;

/** By convention values in this enum must be a style followed by an underscore and then a list of compass directions (or "POST"). */
public enum WallType implements ObjectType {
    BRICK_POST, BRICK_W, BRICK_E, BRICK_N, BRICK_S, BRICK_SW, BRICK_NW, BRICK_EN, BRICK_ES, BRICK_EW, BRICK_NS,
    BRICK_ESW, BRICK_NSW, BRICK_ENW, BRICK_ENS, BRICK_ENSW,
    CHAINLINK_POST, CHAINLINK_W, CHAINLINK_E, CHAINLINK_N, CHAINLINK_S, CHAINLINK_NW, CHAINLINK_EN,
    CHAINLINK_ES, CHAINLINK_SW, CHAINLINK_EW, CHAINLINK_NS, CHAINLINK_ENW, CHAINLINK_ENS, CHAINLINK_ESW, CHAINLINK_NSW,
    CHAINLINK_ENSW,
    GLASS_POST, GLASS_W, GLASS_E, GLASS_N, GLASS_S, GLASS_SW, GLASS_NW, GLASS_EN, GLASS_ES, GLASS_EW, GLASS_NS,
    GLASS_ESW, GLASS_NSW, GLASS_ENW, GLASS_ENS, GLASS_ENSW,
    CONCRETE_POST, CONCRETE_W, CONCRETE_E, CONCRETE_N, CONCRETE_S, CONCRETE_SW, CONCRETE_NW, CONCRETE_EN,
    CONCRETE_ES, CONCRETE_EW, CONCRETE_NS, CONCRETE_ESW, CONCRETE_NSW, CONCRETE_ENW, CONCRETE_ENS, CONCRETE_ENSW,
    ELECTRIC_POST, ELECTRIC_W, ELECTRIC_E, ELECTRIC_N, ELECTRIC_S, ELECTRIC_SW, ELECTRIC_NW, ELECTRIC_EN,
    ELECTRIC_ES, ELECTRIC_EW, ELECTRIC_NS, ELECTRIC_ESW, ELECTRIC_NSW, ELECTRIC_ENW, ELECTRIC_ENS, ELECTRIC_ENSW,
    WOODEN_POST, WOODEN_W, WOODEN_E, WOODEN_N, WOODEN_S, WOODEN_SW, WOODEN_NW, WOODEN_EN,
    WOODEN_ES, WOODEN_EW, WOODEN_NS, WOODEN_ESW, WOODEN_NSW, WOODEN_ENW, WOODEN_ENS, WOODEN_ENSW;

    private static final Map<String, Integer> PRICES = ImmutableMap.<String, Integer>builder().put("BRICK", 1)
            .put("CHAINLINK", 1).put("GLASS", 4).put("CONCRETE", 2).put("ELECTRIC", 10).put("WOODEN", 3).build();
    private String wallStyle;
    private final String displayString;
    private Set<Direction> joinedTo;

    WallType() {
        String[] words = this.name().split("_", 2);
        wallStyle = words[0];
        displayString = StringUtils.makeReadableStringFromEnum(words[0]);

        String directions = words[words.length - 1];
        joinedTo = new HashSet<>();
        if (!directions.equals("POST")) {
            for (char letter : directions.toCharArray()) {
                switch (letter) {
                    case 'N':
                        joinedTo.add(UP);
                        break;
                    case 'E':
                        joinedTo.add(RIGHT);
                        break;
                    case 'S':
                        joinedTo.add(DOWN);
                        break;
                    case 'W':
                        joinedTo.add(LEFT);
                        break;
                    default:
                        throw new IllegalStateException("Unrecognised last word: " + directions);
                }
            }
        }
    }

    public static WallType getWallType(String wallStyle, Set<Direction> joinedTo) {
        for (WallType wallType : WallType.values()) {
            if (wallType.wallStyle.equals(wallStyle) && wallType.joinedTo.equals(joinedTo)) {
                return wallType;
            }
        }
        throw new IllegalStateException("Unrecognised wall type: " + wallStyle + ", " + joinedTo);
    }

    public static WallType[] allPurchasable() {
        List<WallType> ewWallTypes = new ArrayList<>();
        for (WallType wallType : values()) {
            if (wallType.joinedTo.equals(Sets.newHashSet(LEFT, RIGHT))) {
                ewWallTypes.add(wallType);
            }
        }
        return ewWallTypes.toArray(new WallType[ewWallTypes.size()]);
    }

    @Override
    public ObjectType getForeground() {
        return null;
    }

    @Override
    public Integer getPrice() {
        return PRICES.get(wallStyle);
    }

    public String getWallStyle() {
        return wallStyle;
    }

    @Override
    public String getDisplayString() {
        return displayString;
    }
}
