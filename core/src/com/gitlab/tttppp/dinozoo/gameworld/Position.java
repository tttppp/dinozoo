package com.gitlab.tttppp.dinozoo.gameworld;

/**
 * An immutable object for storing position.
 */
public class Position {
    private int x;
    private int y;

    public static Position of(int x, int y) {
        Position position = new Position();
        position.x = x;
        position.y = y;
        return position;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Position position = (Position) o;
        return x == position.x &&
                y == position.y;
    }

    @Override
    public int hashCode() {
        return com.google.common.base.Objects.hashCode(x, y);
    }

    @Override
    public String toString() {
        return "Position(" + x + ", " + y + ")";
    }

    public static Position valueOf(String positionString) {
        String[] coordinates = positionString.replace("Position(", "")
                .replace(")", "").split(", ");
        return Position.of(Integer.valueOf(coordinates[0]), Integer.valueOf(coordinates[1]));
    }
}
