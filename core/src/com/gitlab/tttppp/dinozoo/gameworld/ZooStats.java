package com.gitlab.tttppp.dinozoo.gameworld;

import com.gitlab.tttppp.dinozoo.utils.Log;

public class ZooStats {
    private static final int POPULARITY_DAMPENING = 99;
    private static final int EXPERIENCE_DAMPENING = 49;
    private static final long INITIAL_MONEY = 10000;
    private static final int INITIAL_TICKET_PRICE = 100;

    private double popularity = 0.1;
    private double recentExperience = 1;
    private long money = INITIAL_MONEY;
    private int ticketPrice = INITIAL_TICKET_PRICE;

    public double getPopularity() {
        return popularity;
    }

    public void setPopularity(double popularity) {
        this.popularity = popularity;
    }

    public double getRecentExperience() {
        return recentExperience;
    }

    public void addRecentExperience(double experience, double expectation) {
        // Update the recent experience score.
        recentExperience = (recentExperience * EXPERIENCE_DAMPENING + experience) / (EXPERIENCE_DAMPENING + 1);

        // Update the zoo popularity based on this guest's experience and expectation.
        double guestSatisfaction = experience / (experience + expectation);
        double newPopularity = (popularity * POPULARITY_DAMPENING + guestSatisfaction) / (POPULARITY_DAMPENING + 1);
        this.setPopularity(newPopularity);
        Log.log("Update popularity to " + newPopularity + " and recentExp to " + recentExperience + "(experience: " + experience + ", expectation: " + expectation + ")");
    }

    public long getMoney() {
        return money;
    }

    public void setMoney(long money) {
        this.money = money;
    }

    public void addMoney(int amount) {
        money += amount;
    }

    /**
     * Try to spend some money.
     *
     * @param amount The amount to try to spend.
     * @return false if the zoo didn't have enough money to spend.
     */
    public boolean spendMoney(int amount) {
        if (amount > money) {
            return false;
        }
        money -= amount;
        return true;
    }

    public int getTicketPrice() {
        return ticketPrice;
    }

    public void setTicketPrice(int ticketPrice) {
        this.ticketPrice = ticketPrice;
    }
}
