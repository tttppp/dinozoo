package com.gitlab.tttppp.dinozoo.utils;

import java.util.Map;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class RandomUtils {
    /** Private constructor for utility class. */
    private RandomUtils() {
    }

    /**
     * Pick an option at random according to the weights supplied.
     *
     * @param weightedOptions The map from the options to the weights.
     * @param <T> The type of the options.
     * @return The chosen option, or null if no option had any weight.
     */
    public static <T> T pickWeightedOption(Map<T, Long> weightedOptions) {
        long sum = 0;
        for (long weight : weightedOptions.values()) {
            sum += weight;
        }
        if (sum == 0) {
            return null;
        }
        long value = ThreadLocalRandom.current().nextLong(sum);
        for (Map.Entry<T, Long> entry : weightedOptions.entrySet()) {
            value -= entry.getValue();
            if (value < 0) {
                return entry.getKey();
            }
        }
        throw new IllegalStateException("Failed to pick option from " + weightedOptions);
    }

    public static boolean flipCoin(int chanceSuccess, int chanceFailure) {
        return (ThreadLocalRandom.current().nextInt(chanceSuccess + chanceFailure) >= chanceFailure);
    }

    public static int getRandomInt() {
        return ThreadLocalRandom.current().nextInt();
    }

    public static int getRandomInt(int range) {
        return ((getRandomInt() % range) + range) % range;
    }
}
