package com.gitlab.tttppp.dinozoo.utils;

import com.google.common.base.Objects;

public class Triple<T, U, V> {
    private final T left;
    private final U middle;
    private final V right;

    public Triple(T left, U middle, V right) {
        this.left = left;
        this.middle = middle;
        this.right = right;
    }

    public T getLeft() {
        return left;
    }

    public U getMiddle() {
        return middle;
    }

    public V getRight() {
        return right;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Triple<?, ?, ?> pair = (Triple<?, ?, ?>) o;
        return Objects.equal(left, pair.left) &&
                Objects.equal(middle, pair.middle) &&
                Objects.equal(right, pair.right);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(left, middle, right);
    }

    @Override
    public String toString() {
        return "Pair: " + left.toString() + "," + middle.toString() + "," + right.toString();
    }
}
