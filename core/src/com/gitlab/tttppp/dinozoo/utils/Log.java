package com.gitlab.tttppp.dinozoo.utils;

import com.badlogic.gdx.Gdx;

public class Log {
    public static void log(String message) {
        Gdx.app.log(Thread.currentThread().getStackTrace()[1].getClassName(), message);
    }
}
