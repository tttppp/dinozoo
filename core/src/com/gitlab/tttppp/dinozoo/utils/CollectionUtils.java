package com.gitlab.tttppp.dinozoo.utils;

import java.util.Collection;

public class CollectionUtils {
    /** Private constructor for helper. */
    private CollectionUtils() {
    }

    /**
     * Check if a collection is null or empty.
     *
     * @param collection The collection.
     * @param <T> The type of objects in the collection.
     * @return true if the collection is null or empty.
     */
    public static <T> boolean isEmpty(Collection<T> collection) {
        return collection == null || collection.isEmpty();
    }
}
