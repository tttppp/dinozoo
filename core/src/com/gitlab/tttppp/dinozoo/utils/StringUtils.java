package com.gitlab.tttppp.dinozoo.utils;

public class StringUtils {
    /** Private constructor for utility class. */
    private StringUtils() {
    }

    public static String makeReadableStringFromEnum(String enumName) {
        StringBuilder readableString = new StringBuilder();
        boolean capital = true;
        for (char c : enumName.toCharArray()) {
            if (c == '_') {
                readableString.append(" ");
                capital = true;
            } else if (!capital && c >= 'A' && c <= 'Z') {
                readableString.append((char) (c - 'A' + 'a'));
            } else {
                readableString.append(c);
                capital = false;
            }
        }
        return readableString.toString();
    }
}
