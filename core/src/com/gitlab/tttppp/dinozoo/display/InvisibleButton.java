package com.gitlab.tttppp.dinozoo.display;

import com.gitlab.tttppp.dinozoo.input.Action;

public class InvisibleButton extends AbstractMenuButton {
    private Action action;
    private boolean clickActivated;
    private boolean dragActivated;

    public InvisibleButton(Action action, float x, float y, float width, float height, boolean dragActivated) {
        super(x, y, width, height);
        this.action = action;
        this.clickActivated = !dragActivated;
        this.dragActivated = dragActivated;
    }

    @Override
    public Action getAction() {
        return action;
    }

    @Override
    public boolean handleTouchUp(float screenX, float screenY) {
        if (super.handleTouchUp(screenX, screenY)) {
            if (clickActivated) {
                action.execute();
            }
            return true;
        }
        return false;
    }

    @Override
    public boolean handleDrag(float screenX, float screenY) {
        if (dragActivated && super.handleDrag(screenX, screenY)) {
            action.execute();
            return true;
        }
        return false;
    }
}
