package com.gitlab.tttppp.dinozoo.display;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;

public class GraphicDisplay implements DisplayElement {
    protected float x;
    protected float y;
    private TextureRegion textureRegion;
    protected float width;
    protected float height;
    protected Rectangle bounds;

    /** Graphic display without a graphic set. */
    public GraphicDisplay(float x, float y, float width, float height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        bounds = new Rectangle(x, y, width, height);
    }

    public GraphicDisplay(float x, float y, TextureRegion textureRegion) {
        this(x, y, textureRegion.getRegionWidth(), textureRegion.getRegionHeight());
        this.textureRegion = textureRegion;
    }

    @Override
    public void draw(SpriteBatch batcher) {
        TextureRegion textureRegion = getTextureRegion();
        if (textureRegion != null) {
            batcher.draw(textureRegion, x, y, width, height);
        }
    }

    public TextureRegion getTextureRegion() {
        return textureRegion;
    }
}
