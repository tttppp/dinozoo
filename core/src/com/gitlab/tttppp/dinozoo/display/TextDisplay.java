package com.gitlab.tttppp.dinozoo.display;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;

import static com.gitlab.tttppp.dinozoo.gamerenderer.GameRenderer.SCREEN_HEIGHT;
import static com.gitlab.tttppp.dinozoo.gamerenderer.GameRenderer.SCREEN_WIDTH;

public class TextDisplay implements DisplayElement {

	private float textWidth, textHeight;
	private String text;
	private GlyphLayout glyphLayout = new GlyphLayout();
	private BitmapFont bitmapFont;
	private Color textColor;
	private float x, y;
	private float width, height;

	public TextDisplay(float x, float y, float width, float height, String text, BitmapFont bitmapFont,
                       Color textColor) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.bitmapFont = bitmapFont;
		this.textColor = textColor;
		setText(text);
	}

	@Override
	public void draw(SpriteBatch batcher) {
		float textX = x + (width - textWidth) / 2;
		float textY = y + (height + textHeight) / 2;
		bitmapFont.setColor(textColor);
		bitmapFont.draw(batcher, text, textX, textY);
	}

	public void setTextColor(Color textColor) {
		this.textColor = textColor;
	}

	/**
	 * Set the text string and reevaluate the bounding box (and therefore the
	 * centre of the text).
	 * 
	 * @param text The new text to display.
	 */
	public void setText(String text) {
		this.text = text;
		glyphLayout.setText(bitmapFont, text);
		textWidth = glyphLayout.width;
		textHeight = glyphLayout.height;
	}

	/**
	 * Set the selected font and reevaluate the bounding box.
	 * 
	 * @param selectedFont The new font.
	 */
	public void setBitmapFont(BitmapFont selectedFont) {
		this.bitmapFont = selectedFont;
		glyphLayout.setText(bitmapFont, text);
		textWidth = glyphLayout.width;
		textHeight = glyphLayout.height;
	}

	public String getText() {
		return text;
	}

	public float getX() {
		return x;
	}

	public float getY() {
		return y;
	}

	public float getWidth() {
		return width;
	}

	public float getHeight() {
		return height;
	}

	public Color getTextColor() {
		return textColor;
	}

	public BitmapFont getBitmapFont() {
		return bitmapFont;
	}

}
