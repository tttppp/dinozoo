package com.gitlab.tttppp.dinozoo.display;

import com.gitlab.tttppp.dinozoo.input.Action;

public interface MenuButton extends DisplayElement {
    Action getAction();

    boolean handleTouchDown(float screenX, float screenY);

    boolean handleTouchUp(float screenX, float screenY);

    boolean handleDrag(float screenX, float screenY);

    void setActive(boolean isActive);
}
