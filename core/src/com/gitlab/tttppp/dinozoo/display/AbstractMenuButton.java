package com.gitlab.tttppp.dinozoo.display;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;

import static com.gitlab.tttppp.dinozoo.gamerenderer.GameRenderer.SCREEN_HEIGHT;
import static com.gitlab.tttppp.dinozoo.gamerenderer.GameRenderer.SCREEN_WIDTH;
import static java.lang.Math.max;

public abstract class AbstractMenuButton extends GraphicDisplay implements MenuButton {
    protected TextureRegion buttonUp;
    protected TextureRegion buttonDown;

    private boolean isActive = true;
    private boolean isPressed = false;
    private boolean darkenWhenPressed = false;
    private boolean isDraggedOnto = false;

    /**
     * A button with texture for pressed and de-pressed.
     *
     * @param x
     * @param y
     * @param buttonUp
     * @param buttonDown
     */
    public AbstractMenuButton(float x, float y, TextureRegion buttonUp, TextureRegion buttonDown) {
        super(x, y, max(buttonDown.getRegionWidth(), buttonUp.getRegionWidth()), max(buttonDown.getRegionHeight(), buttonUp.getRegionHeight()));
        this.buttonUp = buttonUp;
        this.buttonDown = buttonDown;
    }

    /**
     * A button with a single texture that is darkened when pressed.
     */
    public AbstractMenuButton(float x, float y, TextureRegion buttonUp) {
        this(x, y, buttonUp, buttonUp);
        this.darkenWhenPressed = true;
    }

    public AbstractMenuButton(float x, float y, float width, float height) {
        super(x, y, width, height);
    }

    @Override
    public void draw(SpriteBatch batcher) {
        TextureRegion button = getTextureRegion();
        boolean darken = isPressed && darkenWhenPressed;
        if (button != null) {
            if (darken) {
                batcher.setColor(0.5F, 0.5F, 0.5F, 1F);
            }
            batcher.draw(button, x, y, width, height);
            if (darken) {
                batcher.setColor(1F, 1F, 1F, 1F);
            }
        }
    }

    @Override
    public boolean handleTouchDown(float screenX, float screenY) {
        if (isActive && containsPosition(screenX, screenY)) {
            isPressed = true;
            return true;
        }

        return false;
    }

    @Override
    public boolean handleTouchUp(float screenX, float screenY) {
        // It only counts as a touchUp if the button is in a pressed state.
        if (isActive && containsPosition(screenX, screenY) && isPressed) {
            isPressed = false;
            return true;
        }
        if (isDraggedOnto) {
            isDraggedOnto = false;
            return true;
        }

        // Whenever a finger is released, we will cancel any presses.
        isPressed = false;
        return false;
    }

    @Override
    public boolean handleDrag(float screenX, float screenY) {
        // We only care when the drag enters the button for the first time.
        if (isActive && containsPosition(screenX, screenY) && !isDraggedOnto) {
            isDraggedOnto = true;
            return true;
        }
        return false;
    }

    /**
     * Check if the position on the screen is within the button.
     *
     * @param screenX
     * @param screenY
     * @return
     */
    protected boolean containsPosition(float screenX, float screenY) {
        return bounds.contains(screenX, screenY);
    }

    @Override
    public TextureRegion getTextureRegion() {
        if (isPressed) {
            return buttonDown;
        }
        return buttonUp;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public float getWidth() {
        return width;
    }

    public float getHeight() {
        return height;
    }

    public void setActive(boolean isActive) {
        this.isActive = isActive;
    }

    public boolean isActive() {
        return isActive;
    }
}
