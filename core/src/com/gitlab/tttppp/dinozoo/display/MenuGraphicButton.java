package com.gitlab.tttppp.dinozoo.display;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.gitlab.tttppp.dinozoo.input.Action;

public class MenuGraphicButton extends AbstractMenuButton {
	private final Action action;

	/**
	 * Constructor.
	 *
	 * @param action The action to happen when the button is pressed.
	 * @param x The x coordinate of the top left of the button.
	 * @param y The y coordinate of the top left of the button.
	 * @param buttonUp The texture to display for the button when de-pressed).
	 * @param buttonDown The texture to display for the button when pressed).
	 */
	public MenuGraphicButton(Action action, float x, float y, TextureRegion buttonUp, TextureRegion buttonDown) {
		super(x, y, buttonUp, buttonDown);
		this.action = action;
	}

	/**
	 * Constructor that uses a single texture and darkens it when pressed.
	 */
    public MenuGraphicButton(Action action, int x, int y, TextureRegion buttonUp) {
        super(x, y, buttonUp);
        this.action = action;
    }

    @Override
	public Action getAction() {
		return action;
	}

	@Override
	public boolean handleTouchUp(float screenX, float screenY) {
		if (super.handleTouchUp(screenX, screenY)) {
			action.execute();
			return true;
		}
		return false;
	}
}
