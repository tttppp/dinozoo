package com.gitlab.tttppp.dinozoo.display;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public abstract class AbstractTextButton extends AbstractMenuButton {
	private float textWidth, textHeight;
	private String text;
	private GlyphLayout glyphLayout = new GlyphLayout();
	private BitmapFont bitmapFont;
	private Color textColor;

	public AbstractTextButton(float x, float y, TextureRegion buttonUp, TextureRegion buttonDown,
                              String text, BitmapFont bitmapFont, Color textColor) {
		super(x, y, buttonUp, buttonDown);
		this.text = text;
		this.bitmapFont = bitmapFont;
		this.textColor = textColor;
		glyphLayout.setText(bitmapFont, text);
		textWidth = glyphLayout.width;
		textHeight = glyphLayout.height;
	}

	/**
	 * A text button with no texture associated.
	 * 
	 * @param x The left of the button.
	 * @param y The top of the button.
	 * @param width The width of the button.
	 * @param height The height of the button.
	 * @param text The text to display.
	 * @param bitmapFont The font to use.
	 * @param textColor The colour to make the text.
	 */
	public AbstractTextButton(float x, float y, float width, float height, String text,
                              BitmapFont bitmapFont, Color textColor) {
		super(x, y, width, height);
		this.text = text;
		this.bitmapFont = bitmapFont;
		this.textColor = textColor;
		glyphLayout.setText(bitmapFont, text);
		textWidth = glyphLayout.width;
		textHeight = glyphLayout.height;
	}

	@Override
	public void draw(SpriteBatch batcher) {
		super.draw(batcher);
		float textX = x + (width - textWidth) / 2;
		float textY = y + (height + textHeight) / 2 - 1;
		bitmapFont.setColor(textColor);
		bitmapFont.draw(batcher, text, textX, textY);
	}

	public void setTextColor(Color textColor) {
		this.textColor = textColor;
	}
}
