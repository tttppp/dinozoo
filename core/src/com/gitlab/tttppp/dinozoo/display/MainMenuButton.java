package com.gitlab.tttppp.dinozoo.display;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.gitlab.tttppp.dinozoo.input.Action;
import com.gitlab.tttppp.dinozoo.input.GameMode;
import com.gitlab.tttppp.dinozoo.input.MainMenuAction;

public class MainMenuButton extends AbstractTextButton {
    private GameMode gameMode;
    private Action action;

    public MainMenuButton(GameMode gameMode, float x, float y, TextureRegion buttonUp,
                          TextureRegion buttonDown, String text, BitmapFont bitmapFont, Color textColor, Game game) {
        super(x, y, buttonUp, buttonDown, text, bitmapFont, textColor);
        this.gameMode = gameMode;
        this.action = new MainMenuAction(this.gameMode, game);
    }

    @Override
    public Action getAction() {
        return action;
    }
}
