package com.gitlab.tttppp.dinozoo.display;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.gitlab.tttppp.dinozoo.input.Action;

public class TextButton extends AbstractTextButton {
    private Action action;

    /**
     * A text button with no texture associated.
     *
     * @param x The left of the button.
     * @param y The top of the button.
     * @param width The width of the button.
     * @param height The height of the button.
     * @param text The text to display.
     * @param bitmapFont The font to use.
     * @param textColor The colour to make the text.
     * @param action The action to execute when the button is pressed.
     */
    public TextButton(float x, float y, float width, float height, String text,
                      BitmapFont bitmapFont, Color textColor, Action action) {
        super(x, y, width, height, text, bitmapFont, textColor);
        this.action = action;
    }

    @Override
    public Action getAction() {
        return null;
    }

    @Override
    public boolean handleTouchUp(float screenX, float screenY) {
        if (super.handleTouchUp(screenX, screenY)) {
            action.execute();
            return true;
        }
        return false;
    }
}
