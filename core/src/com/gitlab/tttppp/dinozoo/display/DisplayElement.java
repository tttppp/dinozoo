package com.gitlab.tttppp.dinozoo.display;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;

public interface DisplayElement {
	void draw(SpriteBatch batcher);
}
