package com.gitlab.tttppp.dinozoo.input;

import com.gitlab.tttppp.dinozoo.gameworld.GameWorld;
import com.gitlab.tttppp.dinozoo.screens.SubMenu;
import com.gitlab.tttppp.dinozoo.utils.Log;

import static com.gitlab.tttppp.dinozoo.screens.SubMenuType.FINANCE;

public class UpdateTicketPriceAction implements Action {
    private final int delta;
    private final SubMenu subMenu;

    public UpdateTicketPriceAction(SubMenu subMenu, int delta) {
        this.subMenu = subMenu;
        this.delta = delta;
    }

    @Override
    public void execute() {
        if (subMenu.getSelected() == FINANCE) {
            int ticketPrice = GameWorld.getInstance().getZooStats().getTicketPrice();
            if (ticketPrice + delta > 0) {
                GameWorld.getInstance().getZooStats().setTicketPrice(ticketPrice + delta);
                Log.log("Ticket price now " + GameWorld.getInstance().getZooStats().getTicketPrice());
            }
        }
    }
}
