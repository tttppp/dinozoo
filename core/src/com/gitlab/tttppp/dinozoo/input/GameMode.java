package com.gitlab.tttppp.dinozoo.input;

public enum GameMode {
	PLAY, LOAD_1, LOAD_2, LOAD_3, ABOUT, MAIN_MENU
}
