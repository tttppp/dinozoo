package com.gitlab.tttppp.dinozoo.input;

import com.gitlab.tttppp.dinozoo.gameworld.GameWorld;
import com.gitlab.tttppp.dinozoo.screens.SubMenu;
import com.gitlab.tttppp.dinozoo.utils.Log;

import static com.gitlab.tttppp.dinozoo.screens.SubMenuType.FINANCE;

public class UpdateResearchAction implements Action {
    private final int delta;
    private final SubMenu subMenu;

    public UpdateResearchAction(SubMenu subMenu, int delta) {
        this.subMenu = subMenu;
        this.delta = delta;
    }

    @Override
    public void execute() {
        if (subMenu.getSelected() == FINANCE) {
            int researchAmount = GameWorld.getInstance().getResearch().getResearchAmount();
            if (researchAmount + delta >= 0) {
                GameWorld.getInstance().getResearch().setResearchAmount(researchAmount + delta);
                Log.log("Research amount now " + (researchAmount + delta));
            }
        }
    }
}
