package com.gitlab.tttppp.dinozoo.input;

import com.badlogic.gdx.Game;
import com.gitlab.tttppp.dinozoo.save.GameSaver;
import com.gitlab.tttppp.dinozoo.screens.AboutScreen;
import com.gitlab.tttppp.dinozoo.screens.GameScreen;
import com.gitlab.tttppp.dinozoo.screens.MainMenuScreen;
import com.gitlab.tttppp.dinozoo.utils.Log;

public class MainMenuAction implements Action {
    private Game game;
    private GameMode gameMode;

    public MainMenuAction(GameMode gameMode, Game game) {
        this.gameMode = gameMode;
        this.game = game;
    }

    public GameMode getGameMode() {
        return gameMode;
    }

    @Override
    public void execute() {
        Log.log("Pressed " + gameMode);
        GameSaver gameSaver = new GameSaver();
        switch (gameMode) {
            case PLAY:
                game.setScreen(new GameScreen(game));
                break;
            case LOAD_1:
                // Load the game into the GameWorld singleton.
                gameSaver.loadGame("1");
                game.setScreen(new GameScreen(game));
                break;
            case LOAD_2:
                // Load the game into the GameWorld singleton.
                gameSaver.loadGame("2");
                game.setScreen(new GameScreen(game));
                break;
            case LOAD_3:
                // Load the game into the GameWorld singleton.
                gameSaver.loadGame("3");
                game.setScreen(new GameScreen(game));
                break;
            case ABOUT:
                // Display the About screen.
                game.setScreen(new AboutScreen(game));
                break;
            case MAIN_MENU:
                // Display the Main Menu screen.
                game.setScreen(new MainMenuScreen(game));
                break;
        }
    }
}
