package com.gitlab.tttppp.dinozoo.input;

import com.gitlab.tttppp.dinozoo.gameobject.Building;
import com.gitlab.tttppp.dinozoo.gameobject.Egg;
import com.gitlab.tttppp.dinozoo.gameobject.GameObject;
import com.gitlab.tttppp.dinozoo.gameobject.Item;
import com.gitlab.tttppp.dinozoo.gameobject.Land;
import com.gitlab.tttppp.dinozoo.gameobject.Plant;
import com.gitlab.tttppp.dinozoo.gameobject.Wall;
import com.gitlab.tttppp.dinozoo.gameworld.BuildingType;
import com.gitlab.tttppp.dinozoo.gameworld.DinosaurType;
import com.gitlab.tttppp.dinozoo.gameworld.GameWorld;
import com.gitlab.tttppp.dinozoo.gameworld.GroundSubType;
import com.gitlab.tttppp.dinozoo.gameworld.ItemType;
import com.gitlab.tttppp.dinozoo.gameworld.ObjectType;
import com.gitlab.tttppp.dinozoo.gameworld.PlantType;
import com.gitlab.tttppp.dinozoo.gameworld.Position;
import com.gitlab.tttppp.dinozoo.gameworld.WallType;
import com.gitlab.tttppp.dinozoo.screens.SubMenu;
import com.gitlab.tttppp.dinozoo.screens.SubMenuType;
import com.gitlab.tttppp.dinozoo.utils.Log;

import java.util.List;

import static com.gitlab.tttppp.dinozoo.gameworld.ItemType.BULLDOZER;

public class PlaceObjectAction implements Action {
    private final SubMenu subMenu;
    private int x, y;

    public PlaceObjectAction(SubMenu subMenu, int x, int y) {
        this.subMenu = subMenu;
        this.x = x;
        this.y = y;
    }

    @Override
    public void execute() {
        GameWorld gameWorld = GameWorld.getInstance();
        if (gameWorld.isPaused()) {
            return;
        }
        int mapX = gameWorld.getViewX() + x;
        int mapY = gameWorld.getViewY() + y;
        Position position = Position.of(mapX, mapY);
        SubMenuType selected = subMenu.getSelected();
        ObjectType objectType = GameInputHandler.INSTANCE.getMapPlacementObjectType();
        if (objectType == null) {
            return;
        }
        if (gameWorld.getZooStats().spendMoney(objectType.getPrice())) {
            Log.log("PLACING OBJECT AT " + mapX + ", " + mapY);
            boolean placedSuccessfully = false;
            switch (selected) {
                case ITEM:
                    if (objectType == BULLDOZER) {
                        gameWorld.removeObject(position);
                    } else if (objectType instanceof ItemType) {
                        placedSuccessfully = gameWorld.putObject(position, new Item((ItemType) objectType, position));
                    }
                    break;
                case GROUND:
                    if (objectType instanceof GroundSubType) {
                        placedSuccessfully = gameWorld.putLand(position, new Land((GroundSubType) objectType, position));
                    }
                    break;
                case WALL:
                    if (objectType instanceof WallType) {
                        placedSuccessfully = gameWorld.putObject(position, new Wall((WallType) objectType, position));
                    }
                    break;
                case PLANT:
                    if (objectType instanceof PlantType) {
                        placedSuccessfully = gameWorld.putObject(position, new Plant((PlantType) objectType, position));
                    }
                    break;
                case DINOSAUR:
                    if (objectType instanceof DinosaurType) {
                        // Can only place on an empty nest.
                        boolean canPlace = false;
                        List<GameObject> objectsPresent = gameWorld.getGameObjectsAt(position);
                        for (GameObject objectPresent : objectsPresent) {
                            if (objectPresent.getObjectType() == ItemType.NEST) {
                                canPlace = true;
                            } else if (objectPresent instanceof Egg) {
                                canPlace = false;
                                break;
                            }
                        }
                        if (canPlace) {
                            placedSuccessfully = gameWorld.putObject(position, new Egg((DinosaurType) objectType, position));
                        } else {
                            // Refund the player.
                            placedSuccessfully = false;
                        }
                    }
                    break;
                case BUILDING:
                    if (objectType instanceof BuildingType) {
                        placedSuccessfully = gameWorld.putObject(position, new Building((BuildingType) objectType, position));
                    }
                    break;
            }
            if (!placedSuccessfully) {
                gameWorld.getZooStats().addMoney(objectType.getPrice());
            }
        }
    }
}
