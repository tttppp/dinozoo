package com.gitlab.tttppp.dinozoo.input;

import com.gitlab.tttppp.dinozoo.gameworld.GameWorld;
import com.gitlab.tttppp.dinozoo.gameworld.ObjectType;
import com.gitlab.tttppp.dinozoo.gameworld.Research;
import com.gitlab.tttppp.dinozoo.screens.SubMenu;
import com.gitlab.tttppp.dinozoo.screens.SubMenuType;

public class SetMapClickAction implements Action {
    private final SubMenu subMenu;
    private final SubMenuType subMenuType;
    private final ObjectType objectType;
    private MapInputMode mapInputMode;

    public SetMapClickAction(SubMenu subMenu, SubMenuType subMenuType, MapInputMode mapInputMode, ObjectType objectType) {
        this.subMenu = subMenu;
        this.subMenuType = subMenuType;
        this.mapInputMode = mapInputMode;
        this.objectType = objectType;
    }

    @Override
    public void execute() {
        if (subMenu.getSelected() == subMenuType) {
            GameInputHandler gameInputHandler = GameInputHandler.INSTANCE;
            gameInputHandler.setMapInputMode(mapInputMode);
            gameInputHandler.setMapPlacementObjectType(objectType);
            // New research.
            Research research = GameWorld.getInstance().getResearch();
            if (research.isNewResearch(objectType)) {
                research.seenResearch(objectType);
            }
        }
    }
}
