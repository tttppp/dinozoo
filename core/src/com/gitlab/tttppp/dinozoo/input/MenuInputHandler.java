package com.gitlab.tttppp.dinozoo.input;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputProcessor;
import com.gitlab.tttppp.dinozoo.display.MenuButton;
import com.gitlab.tttppp.dinozoo.screens.MenuScreen;

import java.util.List;

import static com.gitlab.tttppp.dinozoo.gamerenderer.GameRenderer.SCREEN_WIDTH;

public class MenuInputHandler implements InputProcessor {

	private MenuScreen menuScreen;
	private List<? extends MenuButton> buttons;
	protected float xRatio;
	protected float yRatio;

	public MenuInputHandler(MenuScreen menuScreen, List<? extends MenuButton> buttons) {
		this.menuScreen = menuScreen;
		this.buttons = buttons;
		resize();
	}

	@Override
	public boolean keyDown(int keycode) {
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		if (keycode == Keys.BACK) {
			menuScreen.back();
			return true;
		}
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		for (MenuButton menuButton : buttons) {
			if (menuButton.handleTouchDown(screenX * xRatio, screenY * yRatio)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		for (MenuButton menuButton : buttons) {
			if (menuButton.handleTouchUp(screenX * xRatio, screenY * yRatio)) {
				menuButton.getAction().execute();
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean touchCancelled(int screenX, int screenY, int pointer, int button)
	{
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}

	@Override
	public boolean scrolled(float amountX, float amountY)
	{
		return false;
	}

	public void resize() {
		float screenWidth = Gdx.graphics.getWidth();
		float screenHeight = Gdx.graphics.getHeight();
		float gameScreenWidth = SCREEN_WIDTH;
		float gameScreenHeight = gameScreenWidth * screenHeight / screenWidth;
		xRatio = gameScreenWidth / screenWidth;
		yRatio = gameScreenHeight / screenHeight;
	}

	public void setButtons(List<MenuButton> buttons) {
		this.buttons = buttons;
	}
}
