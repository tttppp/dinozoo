package com.gitlab.tttppp.dinozoo.input;

import com.gitlab.tttppp.dinozoo.screens.SubMenu;
import com.gitlab.tttppp.dinozoo.screens.SubMenuType;
import com.gitlab.tttppp.dinozoo.utils.Log;

public class PickSubMenuAction implements Action {
    private final SubMenuType subMenuType;
    private SubMenu subMenu;
    private int subMenuPage;

    public PickSubMenuAction(SubMenu subMenu, SubMenuType subMenuType) {
        this(subMenu, subMenuType, 0);
    }

    public PickSubMenuAction(SubMenu subMenu, SubMenuType subMenuType, int subMenuPage) {
        this.subMenu = subMenu;
        this.subMenuType = subMenuType;
        this.subMenuPage = subMenuPage;
    }

    @Override
    public void execute() {
        Log.log("Firing Action " + subMenuType);
        subMenu.setSelected(subMenuType, subMenuPage);
    }
}
