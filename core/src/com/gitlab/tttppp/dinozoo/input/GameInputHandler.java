package com.gitlab.tttppp.dinozoo.input;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.gitlab.tttppp.dinozoo.display.InvisibleButton;
import com.gitlab.tttppp.dinozoo.display.MenuButton;
import com.gitlab.tttppp.dinozoo.gameworld.GameWorld;
import com.gitlab.tttppp.dinozoo.gameworld.ObjectType;
import com.gitlab.tttppp.dinozoo.save.CrashHandler;
import com.gitlab.tttppp.dinozoo.screens.GameScreen;
import com.gitlab.tttppp.dinozoo.screens.MainMenuScreen;
import com.gitlab.tttppp.dinozoo.screens.SubMenu;
import com.gitlab.tttppp.dinozoo.utils.Log;

import java.util.ArrayList;
import java.util.List;

import static com.gitlab.tttppp.dinozoo.gamerenderer.GameRenderer.SCALE;
import static com.gitlab.tttppp.dinozoo.gamerenderer.GameRenderer.SCREEN_HEIGHT;
import static com.gitlab.tttppp.dinozoo.gamerenderer.GameRenderer.SCREEN_WIDTH;
import static com.gitlab.tttppp.dinozoo.gameworld.GameWorld.VISIBLE_MAP_HEIGHT;
import static com.gitlab.tttppp.dinozoo.gameworld.GameWorld.VISIBLE_MAP_WIDTH;
import static com.gitlab.tttppp.dinozoo.input.MapInputMode.DRAG_PLACEMENT_MODE;

public class GameInputHandler implements InputProcessor {
    public static final GameInputHandler INSTANCE = new GameInputHandler();
    private Game game;
    private List<MenuButton> buttons;
    private MapInputMode mapInputMode;
    private List<MenuButton> clickPlacementButtons = new ArrayList<>();
    private List<MenuButton> dragPlacementButtons = new ArrayList<>();
    private ObjectType mapPlacementObjectType;

    private GameInputHandler() {
    }

    public void init(Game game, List<MenuButton> buttons, SubMenu subMenu) {
        this.game = game;
        this.buttons = buttons;
        // Click and drag placement probably shouldn't happen under scroll buttons.
        for (int x = 1; x < VISIBLE_MAP_WIDTH - 1; x++) {
            for (int y = 1; y < VISIBLE_MAP_HEIGHT - 1; y++) {
                Action action = new PlaceObjectAction(subMenu, x, y);
                MenuButton dragPlacementButton = new InvisibleButton(action, x * SCALE, y * SCALE, SCALE - 1, SCALE - 1, true);
                dragPlacementButtons.add(dragPlacementButton);
                MenuButton clickPlacementButton = new InvisibleButton(action, x * SCALE, y * SCALE, SCALE - 1, SCALE - 1, false);
                clickPlacementButtons.add(clickPlacementButton);
            }
        }
    }

    @Override
    public boolean keyDown(int keycode) {
        try {
            return false;
        } catch (RuntimeException e) {
            CrashHandler crashHandler = new CrashHandler();
            throw crashHandler.handleCrashAndReturnException(e);
        }
    }

    @Override
    public boolean keyUp(int keycode) {
        try {
            if (keycode == Keys.BACK) {
                game.setScreen(new MainMenuScreen(game));
                return true;
            }
            return false;
        } catch (RuntimeException e) {
            CrashHandler crashHandler = new CrashHandler();
            throw crashHandler.handleCrashAndReturnException(e);
        }
    }

    @Override
    public boolean keyTyped(char character) {
        try {
            return false;
        } catch (RuntimeException e) {
            CrashHandler crashHandler = new CrashHandler();
            throw crashHandler.handleCrashAndReturnException(e);
        }
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int buttonId) {
        try {
            float x = getXPixels(screenX);
            float y = getYPixels(screenY);

            resetScreenSaverTimer();

            Log.log("TOUCHED " + x + " " + y);

            for (MenuButton button : buttons) {
                button.handleTouchDown(x, y);
            }
            if (mapInputMode == DRAG_PLACEMENT_MODE) {
                for (MenuButton dragPlacementButton : dragPlacementButtons) {
                    dragPlacementButton.handleDrag(x, y);
                }
            } else {
                for (MenuButton clickPlacementButton : clickPlacementButtons) {
                    clickPlacementButton.handleTouchDown(x, y);
                }
            }
            return true;
        } catch (RuntimeException e) {
            CrashHandler crashHandler = new CrashHandler();
            throw crashHandler.handleCrashAndReturnException(e);
        }
    }

    private void resetScreenSaverTimer() {
        Screen screen = GameWorld.getInstance().getGame().getScreen();
        if (screen instanceof GameScreen) {
            GameScreen gameScreen = (GameScreen) screen;
            if (!gameScreen.isPaused()) {
                GameWorld.getInstance().resetScreenSaverTimer();
            }
        }
    }

    private int getYPixels(int screenY) {
        return screenY * SCREEN_HEIGHT / Gdx.graphics.getHeight();
    }

    private int getXPixels(int screenX) {
        return screenX * SCREEN_WIDTH / Gdx.graphics.getWidth();
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int buttonId) {
        try {
            float x = getXPixels(screenX);
            float y = getYPixels(screenY);

            for (MenuButton button : buttons) {
                button.handleTouchUp(x, y);
            }
            if (mapInputMode == DRAG_PLACEMENT_MODE) {
                for (MenuButton dragPlacementButton : dragPlacementButtons) {
                    dragPlacementButton.handleTouchUp(x, y);
                }
            } else {
                for (MenuButton clickPlacementButton : clickPlacementButtons) {
                    clickPlacementButton.handleTouchUp(x, y);
                }
            }

            return true;
        } catch (RuntimeException e) {
            CrashHandler crashHandler = new CrashHandler();
            throw crashHandler.handleCrashAndReturnException(e);
        }
    }

    @Override
    public boolean touchCancelled(int screenX, int screenY, int pointer, int button)
    {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        try {
            if (pointer != -1 && mapInputMode == DRAG_PLACEMENT_MODE) {
                float x = getXPixels(screenX);
                float y = getYPixels(screenY);

                for (MenuButton dragPlacementButton : dragPlacementButtons) {
                    dragPlacementButton.handleDrag(x, y);
                }
            }
            resetScreenSaverTimer();
            return true;
        } catch (RuntimeException e) {
            CrashHandler crashHandler = new CrashHandler();
            throw crashHandler.handleCrashAndReturnException(e);
        }
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        try {
            return false;
        } catch (RuntimeException e) {
            CrashHandler crashHandler = new CrashHandler();
            throw crashHandler.handleCrashAndReturnException(e);
        }
    }

    @Override
    public boolean scrolled(float amountX, float amountY) {
        try {
            return false;
        } catch (RuntimeException e) {
            CrashHandler crashHandler = new CrashHandler();
            throw crashHandler.handleCrashAndReturnException(e);
        }
    }

    public void setMapInputMode(MapInputMode mapInputMode) {
        this.mapInputMode = mapInputMode;
        Log.log("Map input mode now: " + mapInputMode);
    }

    public void setMapPlacementObjectType(ObjectType objectType) {
        this.mapPlacementObjectType = objectType;
    }

    public ObjectType getMapPlacementObjectType() {
        return mapPlacementObjectType;
    }
}
