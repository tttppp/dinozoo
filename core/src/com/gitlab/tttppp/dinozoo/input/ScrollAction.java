package com.gitlab.tttppp.dinozoo.input;

import com.badlogic.gdx.Gdx;
import com.gitlab.tttppp.dinozoo.gameworld.GameWorld;

public class ScrollAction implements Action {
    private final GameWorld gameWorld;
    private final int deltaX;
    private final int deltaY;

    public ScrollAction(GameWorld gameWorld, int deltaX, int deltaY) {
        this.gameWorld = gameWorld;
        this.deltaX = deltaX;
        this.deltaY = deltaY;
    }

    @Override
    public void execute() {
        gameWorld.setViewX(gameWorld.getViewX() + deltaX);
        gameWorld.setViewY(gameWorld.getViewY() + deltaY);
    }
}
