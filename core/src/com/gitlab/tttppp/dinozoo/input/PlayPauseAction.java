package com.gitlab.tttppp.dinozoo.input;

import com.badlogic.gdx.Game;
import com.gitlab.tttppp.dinozoo.gameworld.GameWorld;
import com.gitlab.tttppp.dinozoo.screens.SubMenu;
import com.gitlab.tttppp.dinozoo.utils.Log;

import static com.gitlab.tttppp.dinozoo.screens.SubMenuType.PREFERENCES;

/** Pause or resume the game. */
public class PlayPauseAction implements Action {
    private SubMenu subMenu;

    public PlayPauseAction(SubMenu subMenu) {
        this.subMenu = subMenu;
    }

    @Override
    public void execute() {
        Log.log("Executing play/pause.");
        if (subMenu.getSelected() == PREFERENCES) {
            GameWorld gameWorld = GameWorld.getInstance();
            Game game = gameWorld.getGame();
            if (gameWorld.isPaused()) {
                gameWorld.resetScreenSaverTimer();
                game.resume();
            } else {
                game.pause();
            }
            subMenu.createSubMenu(PREFERENCES);
        }
    }
}
