package com.gitlab.tttppp.dinozoo.input;

public interface Action {
    void execute();
}
