package com.gitlab.tttppp.dinozoo.input;

public enum MapInputMode {
    QUERY_MODE, DRAG_PLACEMENT_MODE, CLICK_PLACEMENT_MODE
}
