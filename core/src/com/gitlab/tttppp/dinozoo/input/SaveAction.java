package com.gitlab.tttppp.dinozoo.input;

import com.gitlab.tttppp.dinozoo.save.GameSaver;
import com.gitlab.tttppp.dinozoo.screens.SubMenu;

import static com.gitlab.tttppp.dinozoo.screens.SubMenuType.PREFERENCES;

public class SaveAction implements Action {
    private SubMenu subMenu;
    private String key;

    public SaveAction(SubMenu subMenu, String key) {
        this.subMenu = subMenu;
        this.key = key;
    }

    @Override
    public void execute() {
        if (subMenu.getSelected() == PREFERENCES) {
            GameSaver gameSaver = new GameSaver();
            gameSaver.saveGame(key);
        }
    }
}
