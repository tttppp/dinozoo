# Dino Zoo

A dinosaur zoo simulation game written using LibGDX.

![Screenshot of Dino Zoo game](docs/screenshot.png)

# Building

## For Desktop

From the root of the project execute the following:

```gradle desktop:jar```

The jar file will be produced in `desktop/build/libs` and it can be started with:

```java -jar desktop/build/libs/desktop-[version].jar```

## For Raspberry Pi

Open `desktop-[version].jar` as a zip archive and copy the four *.so files from
the `artifacts` directory into the root. Save the archive and transfer to the Pi.
